<?php

class DashboardController extends Component
{

    public $acessoSessao;
    public $tipoSessao;

    function __construct()
    {
        $this->init();
        $this->isLogged();
        $this->acessoSessao = $this->sessaoLogin()['objeto'];
    }

    public function index()
    {
        switch($this->sessaoLogin()['tipo']){
            case "paciente":
                $this->tipoSessao = "paciente";
                $this->sendview("dashboard/paciente");
                break;
            case "fornecedor":
                $this->tipoSessao = "fornecedor";
                $this->sendview("dashboard/fornecedor");
                break;
            case "medico":
                $this->tipoSessao = "medico";
                $this->sendview("dashboard/medico");
                break;
            case "administrador":
                $this->tipoSessao = "administrador";
                $this->sendview("dashboard/administrador");
                break;
            default:
                die("USUARIO NÃO ENCONTRADO");
        }
    }
}