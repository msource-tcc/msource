<?php

class AdministradorController extends Component
{

    public $acessoSessao;
    public $tipoSessao;
    public $administrador;

    function __construct()
    {
        $this->init();
        $this->acessoSessao = $this->sessaoLogin()['objeto'];
        $this->tipoSessao = $this->sessaoLogin()['tipo'];
    }

    public function inserir()
    {
        if ($this->request()) {

            /** data atual **/
            $dataHoje = date('Y-m-d');

            /** instancia **/
            $administrador = new Administrador();
            $administrador->setNome($this->post('nome'));
            $administrador->setEndereco($this->post('endereco'));
            $administrador->setNumero(intval($this->post('numero')));
            $administrador->setComplemento($this->post('complemento'));
            $administrador->setBairro($this->post('bairro'));
            $administrador->setCidade($this->post('cidade'));
            $administrador->setUf($this->post('uf'));
            $administrador->setCep($this->post('cep'));
            $administrador->setPeso(floatval($this->post('peso')));
            $administrador->setNacionalidade($this->post('nacionalidade'));
            $administrador->setDataNascimento($this->dataAmerica($this->post('data_nascimento')));
            $administrador->setSexo($this->post('sexo'));
            $administrador->setObservacao($this->post('observacao'));
            $administrador->setEmail($this->post('email'));
            $administrador->setSenha($this->post('senha'));
            $administrador->setDataCriacao($dataHoje);
            $administrador->setDataAtualizacao($dataHoje);

            /** dao **/
            $administradorDAO = new administradorDAO();
            $administradorDAO->inserir($administrador);

            $this->mensagem = "Inserido com sucesso!";
        }

        /** front **/
        $this->sendview("administrador/inserir");
    }

    public function visualizar()
    {
        /** instancia **/
        $this->administrador = new Administrador();
        $this->administrador->setId($this->acessoSessao->getId());

        /** dao **/
        $administradorDAO = new administradorDAO();
        $administradorDAO->visualizar($this->administrador);

        /** front **/
        $this->sendview("administrador/visualizar");
    }

    public function atualizar()
    {
        if ($this->request()) {

            /** data atual **/
            $dataHoje = date('Y-m-d');

            /** instancia **/
            $administrador = new Administrador();
            $administrador->setId($this->acessoSessao->getId());
            $administrador->setNome($this->post('nome'));
            $administrador->setEndereco($this->post('endereco'));
            $administrador->setNumero(intval($this->post('numero')));
            $administrador->setComplemento($this->post('complemento'));
            $administrador->setBairro($this->post('bairro'));
            $administrador->setCidade($this->post('cidade'));
            $administrador->setUf($this->post('uf'));
            $administrador->setCep($this->post('cep'));
            $administrador->setPeso(floatval($this->post('peso')));
            $administrador->setNacionalidade($this->post('nacionalidade'));
            $administrador->setDataNascimento($this->dataAmerica($this->post('data_nascimento')));
            $administrador->setSexo($this->post('sexo'));
            $administrador->setObservacao($this->post('observacao'));
            $administrador->setEmail($this->post('email'));
            $administrador->setSenha($this->post('senha'));
            $administrador->setDataAtualizacao($dataHoje);

            /** dao **/
            $administradorDAO = new administradorDAO();
            $administradorDAO->atualizar($administrador);

            $this->mensagem = "Atualizado com sucesso!";
        }

        /** instancia **/
        $this->administrador = new Administrador();
        $this->administrador->setId($this->acessoSessao->getId());
        /** dao **/
        $administradorDAO = new administradorDAO();
        $administradorDAO->visualizar($this->administrador);
        /** front **/
        $this->sendview("administrador/atualizar");
    }

    public function excluir()
    {
        /** instancia **/
        $administrador = new Administrador();
        $administrador->setId($this->acessoSessao->getId());
        /** dao **/
        $administradorDAO = new administradorDAO();
        $administradorDAO->excluir($administrador);

        $this->redirect('logoff');
    }

}
