<?php

class RemedioSinteticoController extends Component
{

    public $acessoSessao;
    public $tipoSessao;
    public $remedioSintetico;

    function __construct()
    {
        $this->init();
        $this->acessoSessao = $this->sessaoLogin()['objeto'];
        $this->tipoSessao = $this->sessaoLogin()['tipo'];
    }

    public function listar(){

        /** dao **/
        $remedioSinteticoDAO = new remedioSinteticoDAO();
        $this->remedioSinteticoArray = $remedioSinteticoDAO->listar();

        /** front **/
        $this->sendview("remedio-sintetico/listar");
    }

    public function inserir()
    {
        if ($this->request()) {

            /** data atual **/
            $dataHoje = date('Y-m-d');

            /** instancia **/
            $remedioSintetico = new RemedioSintetico();
            $remedioSintetico->setNomePopular($this->post('nome_popular'));
            $remedioSintetico->setNomeCientifico($this->post('nome_cientifico'));
            $remedioSintetico->setQuilograma($this->post('quilograma'));
            $remedioSintetico->setCalorias($this->post('calorias'));
            $remedioSintetico->setCorNatural($this->post('cor_natural'));
            $remedioSintetico->setDataDescoberta($this->dataAmerica($this->post('data_descoberta')));
            $remedioSintetico->setExcrecao($this->dataAmerica($this->post('excrecao')));
            $remedioSintetico->setAdministracao($this->dataAmerica($this->post('administracao')));
            $remedioSintetico->setCas($this->dataAmerica($this->post('cas')));
            $remedioSintetico->setMetabolismo($this->dataAmerica($this->post('metabolismo')));
            $remedioSintetico->setFinsMedicinais($this->post('fins_medicinais'));
            $remedioSintetico->setDataCriacao($dataHoje);
            $remedioSintetico->setDataAtualizacao($dataHoje);

            /** dao **/
            $remedioSinteticoDAO = new RemedioSinteticoDAO();
            $remedioSinteticoDAO->inserir($remedioSintetico);

            $this->mensagem = "Inserido com sucesso!";
        }

        /** front **/
        $this->sendview("remedio-sintetico/inserir");
    }

    public function visualizar($id)
    {
        /** instancia **/
        $this->remedioSintetico = new RemedioSintetico();
        $this->remedioSintetico->setId($id);

        /** dao **/
        $remedioSinteticoDAO = new RemedioSinteticoDAO();
        $remedioSinteticoDAO->visualizar($this->remedioSintetico);
        $remedioSinteticoDAO->listarAssocDoenca($this->remedioSintetico);
        $remedioSinteticoDAO->listarAssocFornecedor($this->remedioSintetico);

        /** AGREGANDO MEDICO AO REMEDIO **/
        $remedioSinteticoDAO->listarMedicoEspecialista($this->remedioSintetico);

        /** front **/
        $this->sendview("remedio-sintetico/visualizar");
    }

    public function atualizar($id)
    {
        if ($this->request()) {

            /** data atual **/
            $dataHoje = date('Y-m-d');

            /** instancia **/
            $remedioSintetico = new RemedioSintetico();
            $remedioSintetico->setId($id);
            $remedioSintetico->setNomePopular($this->post('nome_popular'));
            $remedioSintetico->setNomeCientifico($this->post('nome_cientifico'));
            $remedioSintetico->setQuilograma($this->post('quilograma'));
            $remedioSintetico->setCalorias($this->post('calorias'));
            $remedioSintetico->setCorNatural($this->post('cor_natural'));
            $remedioSintetico->setDataDescoberta($this->dataAmerica($this->post('data_descoberta')));
            $remedioSintetico->setExcrecao($this->dataAmerica($this->post('excrecao')));
            $remedioSintetico->setAdministracao($this->dataAmerica($this->post('administracao')));
            $remedioSintetico->setCas($this->dataAmerica($this->post('cas')));
            $remedioSintetico->setMetabolismo($this->dataAmerica($this->post('metabolismo')));
            $remedioSintetico->setFinsMedicinais($this->post('fins_medicinais'));
            $remedioSintetico->setDataAtualizacao($dataHoje);

            /** dao **/
            $remedioSinteticoDAO = new RemedioSinteticoDAO();
            $remedioSinteticoDAO->atualizar($remedioSintetico);

            $this->mensagem = "Atualizado com sucesso!";
        }

        /** instancia **/
        $this->remedioSintetico = new RemedioSintetico();
        $this->remedioSintetico->setId($id);

        /** dao **/
        $remedioSinteticoDAO = new RemedioSinteticoDAO();
        $remedioSinteticoDAO->visualizar($this->remedioSintetico);

        /** front **/
        $this->sendview("remedio-sintetico/atualizar");
    }

    public function excluir($id)
    {
        /** instancia **/
        $remedioSintetico = new RemedioSintetico();
        $remedioSintetico->setId($id);

        /** dao **/
        $remedioSinteticoDAO = new RemedioSinteticoDAO();
        $remedioSinteticoDAO->excluirAssocFornecedor($remedioSintetico);
        $remedioSinteticoDAO->excluirAssocDoenca($remedioSintetico);
        $remedioSinteticoDAO->excluir($remedioSintetico);

        $this->redirect('remedio-sintetico/listar');
    }

}
