<?php

class PacienteController extends Component
{

    public $acessoSessao;
    public $tipoSessao;
    public $paciente;
    public $contatoCheckBox = false;

    function __construct()
    {
        $this->init();
        $this->acessoSessao = $this->sessaoLogin()['objeto'];
        $this->tipoSessao = $this->sessaoLogin()['tipo'];
    }

    public function inserir()
    {
        if ($this->request()) {

            /** data atual **/
            $dataHoje = date('Y-m-d');

            /** instancia paciente**/
            $paciente = new Paciente();
            $paciente->setNome($this->post('nome'));
            $paciente->setEndereco($this->post('endereco'));
            $paciente->setNumero(intval($this->post('numero')));
            $paciente->setComplemento($this->post('complemento'));
            $paciente->setBairro($this->post('bairro'));
            $paciente->setCidade($this->post('cidade'));
            $paciente->setUf($this->post('uf'));
            $paciente->setCep($this->post('cep'));
            $paciente->setPeso(floatval($this->post('peso')));
            $paciente->setAltura(floatval($this->post('altura')));
            $paciente->setNacionalidade($this->post('nacionalidade'));
            $paciente->setDataNascimento($this->dataAmerica($this->post('data_nascimento')));
            $paciente->setSexo($this->post('sexo'));
            $paciente->setNomePai($this->post('nome_pai'));
            $paciente->setNomeMae($this->post('nome_mae'));
            $paciente->setObservacao($this->post('observacao'));
            $paciente->setEmail($this->post('email'));
            $paciente->setSenha($this->post('senha'));
            $paciente->setDataCriacao($dataHoje);
            $paciente->setDataAtualizacao($dataHoje);

            /** dao paciente **/
            $pacienteDAO = new PacienteDAO();
            $pacienteDAO->inserir($paciente);

            /** instancia de contato paciente **/
            if(!empty($paciente->getId())){

                $arrayContatos = $this->post('contato');
                foreach ($arrayContatos as $arrayContato) {

                    if (isset($arrayContato['contato-checkbox'])) {

                        $paciente->setContatoPaciente(
                            null,
                            $arrayContato['celular'],
                            intval($arrayContato['ddi']),
                            $arrayContato['whatsapp'],
                            $arrayContato['telegram'],
                            $arrayContato['contato_primeiro_grau'],
                            $arrayContato['contato_segundo_grau'],
                            $arrayContato['titular'],
                            floatval($arrayContato['frequencia_uso']),
                            $this->dataAmerica($arrayContato['pertence_desde']),
                            $arrayContato['observacao'],
                            $dataHoje,
                            $dataHoje
                        );
                    }
                }

                $contatoPacienteDAO = new ContatoPacienteDAO();
                $contatoPacienteDAO->inserir($paciente);
            }

            $this->redirect('login/paciente');
        }

        /** front **/
        $this->sendview("paciente/inserir");
    }

    public function visualizar()
    {
        /** instancia paciente**/
        $this->paciente = new Paciente();
        $this->paciente->setId($this->acessoSessao->getId());

        /** dao paciente**/
        $pacienteDAO = new PacienteDAO();
        $pacienteDAO->visualizar($this->paciente);

        /** dao contato paciente**/
        $contatoPacienteDAO = new ContatoPacienteDAO();
        $contatoPacienteDAO->visualizar($this->paciente);

        /** front **/
        $this->sendview("paciente/visualizar");
    }

    public function atualizar()
    {
        if ($this->request()){

            /** data atual **/
            $dataHoje = date('Y-m-d');

            /** instancia paciente **/
            $paciente = new Paciente();
            $paciente->setId($this->acessoSessao->getId());
            $paciente->setNome($this->post('nome'));
            $paciente->setEndereco($this->post('endereco'));
            $paciente->setNumero(intval($this->post('numero')));
            $paciente->setComplemento($this->post('complemento'));
            $paciente->setBairro($this->post('bairro'));
            $paciente->setCidade($this->post('cidade'));
            $paciente->setUf($this->post('uf'));
            $paciente->setCep($this->post('cep'));
            $paciente->setPeso(floatval($this->post('peso')));
            $paciente->setAltura(floatval($this->post('altura')));
            $paciente->setNacionalidade($this->post('nacionalidade'));
            $paciente->setDataNascimento($this->dataAmerica($this->post('data_nascimento')));
            $paciente->setSexo($this->post('sexo'));
            $paciente->setNomePai($this->post('nome_pai'));
            $paciente->setNomeMae($this->post('nome_mae'));
            $paciente->setObservacao($this->post('observacao'));
            $paciente->setEmail($this->post('email'));
            $paciente->setSenha($this->post('senha'));
            $paciente->setDataAtualizacao($dataHoje);

            /** dao contato paciente **/
            $contatoPacienteDAO = new ContatoPacienteDAO();

            $arrayContatos = $this->post('contato');
            foreach ($arrayContatos as $arrayContato) {

                if (isset($arrayContato['contato-checkbox']) && isset($arrayContato['id'])) {
                    $paciente->setContatoPaciente(
                        $arrayContato['id'],
                        $arrayContato['celular'],
                        intval($arrayContato['ddi']),
                        $arrayContato['whatsapp'],
                        $arrayContato['telegram'],
                        $arrayContato['contato_primeiro_grau'],
                        $arrayContato['contato_segundo_grau'],
                        $arrayContato['titular'],
                        floatval($arrayContato['frequencia_uso']),
                        $this->dataAmerica($arrayContato['pertence_desde']),
                        $arrayContato['observacao'],
                        $dataHoje,
                        $dataHoje
                    );
                    $this->contatoCheckBox = true;
                }else if(isset($arrayContato['contato-checkbox'])){
                    $paciente->setContatoPaciente(
                        null,
                        $arrayContato['celular'],
                        intval($arrayContato['ddi']),
                        $arrayContato['whatsapp'],
                        $arrayContato['telegram'],
                        $arrayContato['contato_primeiro_grau'],
                        $arrayContato['contato_segundo_grau'],
                        $arrayContato['titular'],
                        floatval($arrayContato['frequencia_uso']),
                        $this->dataAmerica($arrayContato['pertence_desde']),
                        $arrayContato['observacao'],
                        $dataHoje,
                        $dataHoje
                    );
                    $this->contatoCheckBox = true;
                }
            }

            /** dao paciente **/
            $pacienteDAO = new PacienteDAO();
            $pacienteDAO->atualizar($paciente);

            /** dao contato paciente **/
            $contatoPacienteDAO = new ContatoPacienteDAO();
            if($paciente->getContatoPaciente()){
                $contatoPacienteDAO->atualizar($paciente);
            }
            if(!$this->contatoCheckBox){
                $contatoPacienteDAO->excluir($paciente);
            }

            $this->mensagem = "Atualizado com sucesso!";
        }

        /** instancia paciente **/
        $this->paciente = new Paciente();
        $this->paciente->setId($this->acessoSessao->getId());

        /** dao paciente **/
        $pacienteDAO = new PacienteDAO();
        $pacienteDAO->visualizar($this->paciente);

        /** dao contato paciente **/
        $contatoPacienteDAO = new ContatoPacienteDAO();
        $contatoPacienteDAO->visualizar($this->paciente);

        /** front **/
        $this->sendview("paciente/atualizar");
    }

    public function excluir()
    {
        /** instancia **/
        $paciente = new Paciente();
        $paciente->setId($this->acessoSessao->getId());

        /** dao paciente **/
        $contatoPacienteDAO = new ContatoPacienteDAO();
        $contatoPacienteDAO->excluir($paciente);

        /** dao contato paciente **/
        $pacienteDAO = new PacienteDAO();
        $pacienteDAO->excluir($paciente);

        $this->redirect('logoff');
    }


}
