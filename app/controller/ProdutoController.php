<?php

class ProdutoController extends Component
{

    public $medico;
    public $medicoSessao;
    public $produto;

    function __construct()
    {
        $this->init();
        $this->acessoSessao = $this->sessaoLogin()['objeto'];
        $this->tipoSessao = $this->sessaoLogin()['tipo'];
    }

    public function listar()
    {
//INSTANCIA
        $this->medico = new Medico();
        $produto = new Produto();
        $produtoDAO = new ProdutoDAO();

//SET
        $this->medico->setId($this->acessoSessao->getId());

//CHAMADA
        $produtoDAO->listar($this->medico, $produto);

        $this->sendview("produto/listar");
    }

    public function inserir()
    {
        if ($this->request()) {

//INSTANCIA
            $produto = new Produto();
            $produtoDAO = new ProdutoDAO();

//DATA ATUAL
            $dataHoje = date('Y-m-d');

//SET
            $produto->setMedicoId(intval($this->acessoSessao->getId()));
            $produto->setNomePopular($this->post('nome_popular'));
            $produto->setNomeCientifico($this->post('nome_cientifico'));
            $produto->setClasse($this->post('classe'));
            $produto->setFamilia($this->post('familia'));
            $produto->setGenero($this->post('genero'));
            $produto->setReino($this->post('reino'));
            $produto->setQuilograma(floatval($this->post('quilograma')));
            $produto->setCalorias(intval($this->post('calorias')));
            $produto->setCorNatural($this->post('cor_natural'));
            $produto->setDataDescoberta($this->dataAmerica($this->post('data_descoberta')));
            $produto->setFinsMedicinais($this->post('fins_medicinais'));
            $produto->setDataCriacao($dataHoje);
            $produto->setDataAtualizacao($dataHoje);

//PERSISTENCIA
            $produtoDAO->inserir($produto);

            $this->mensagem = "Cadastrado com sucesso!";
        }
//FRONT
        $this->sendview("produto/inserir");
    }


    public function visualizar($id)
    {

//INSTANCIA
        $this->produto = new Produto();
        $produtoDAO = new ProdutoDAO();

//SET
        $this->produto->setId($id);
        $this->produto->setMedicoId($this->acessoSessao->getId());

//CHAMADA
        $produtoDAO->visualizar($this->produto);

        $this->sendview("produto/visualizar");
    }

    public function atualizar($id)
    {

//INSTANCIA
        $this->produto = new Produto();
        $produtoDAO = new ProdutoDAO();

//SET
        $this->produto->setId($id);
        $this->produto->setMedicoId($this->acessoSessao->getId());

        if ($this->request()) {

//DATA ATUAL
            $dataHoje = date('Y-m-d');

//SET
            $this->produto->setNomePopular($this->post('nome_popular'));
            $this->produto->setNomeCientifico($this->post('nome_cientifico'));
            $this->produto->setClasse($this->post('classe'));
            $this->produto->setFamilia($this->post('familia'));
            $this->produto->setGenero($this->post('genero'));
            $this->produto->setReino($this->post('reino'));
            $this->produto->setQuilograma(floatval($this->post('quilograma')));
            $this->produto->setCalorias(intval($this->post('calorias')));
            $this->produto->setCorNatural($this->post('cor_natural'));
            $this->produto->setDataDescoberta($this->dataAmerica($this->post('data_descoberta')));
            $this->produto->setFinsMedicinais($this->post('fins_medicinais'));
            $this->produto->setDataAtualizacao($dataHoje);

//PERSISTENCIA
            $produtoDAO->atualizar($this->produto);

            $this->mensagem = "Atualizado com sucesso!";
        }

//CHAMADA
        $produtoDAO->visualizar($this->produto);
        $this->sendview("produto/atualizar");
    }

    public function excluir($id)
    {

//INSTANCIA
        $produto = new Produto();
        $produtoDAO = new ProdutoDAO();

//SET
        $produto->setId($id);
        $produto->setMedicoId($this->acessoSessao->getId());

//CHAMADA
        $produtoDAO->excluir($produto);
        $this->redirect('produto/listar');
    }

}
