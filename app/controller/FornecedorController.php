<?php

class FornecedorController extends Component
{

    public $fornecedor;
    public $acessoSessao;

    function __construct()
    {
        $this->init();
        $this->acessoSessao = $this->sessaoLogin()['objeto'];
        $this->tipoSessao = $this->sessaoLogin()['tipo'];
    }

    public function inserir()
    {

        if ($this->request()) {

            /** data atual **/
            $dataHoje = date('Y-m-d');

            /** instancia **/
            $fornecedor = new Fornecedor();
            $fornecedor->setNome($this->post('nome'));
            $fornecedor->setEndereco($this->post('endereco'));
            $fornecedor->setNumero($this->post('numero'));
            $fornecedor->setComplemento($this->post('complemento'));
            $fornecedor->setBairro($this->post('bairro'));
            $fornecedor->setCidade($this->post('cidade'));
            $fornecedor->setUf($this->post('uf'));
            $fornecedor->setCep($this->post('cep'));
            $fornecedor->setNomeFantasia($this->post('nome_fantasia'));
            $fornecedor->setDesde($this->dataAmerica($this->post('desde')));
            $fornecedor->setPercentualRamo($this->post('percentual_ramo'));
            $fornecedor->setObservacao($this->post('observacao'));
            $fornecedor->setEmail($this->post('email'));
            $fornecedor->setSenha($this->post('senha'));
            $fornecedor->setDataCriacao($dataHoje);
            $fornecedor->setDataAtualizacao($dataHoje);
            $fornecedor->setDataAtualizacao($dataHoje);

            /** dao **/
            $fornecedorDAO = new FornecedorDAO();
            $fornecedorDAO->inserir($fornecedor);

            $this->redirect('login/fornecedor');
        }

        /** front **/
        $this->sendview("fornecedor/inserir");
    }

    public function visualizar()
    {

        /** instancia **/
        $this->fornecedor = new Fornecedor();
        $this->fornecedor->setId($this->acessoSessao->getId());

        /** dao **/
        $fornecedorDAO = new FornecedorDAO();
        $fornecedorDAO->visualizar($this->fornecedor);
        $fornecedorDAO->listarARN($this->fornecedor);
        $fornecedorDAO->listarARS($this->fornecedor);

        /** front **/
        $this->sendview("fornecedor/visualizar");
    }

    public function atualizar()
    {
        if ($this->request()) {

            /** data atual **/
            $dataHoje = date('Y-m-d');

            /** instancia **/
            $fornecedor = new Fornecedor();
            $fornecedor->setId($this->acessoSessao->getId());
            $fornecedor->setNome($this->post('nome'));
            $fornecedor->setEndereco($this->post('endereco'));
            $fornecedor->setNumero(intval($this->post('numero')));
            $fornecedor->setComplemento($this->post('complemento'));
            $fornecedor->setBairro($this->post('bairro'));
            $fornecedor->setCidade($this->post('cidade'));
            $fornecedor->setUf($this->post('uf'));
            $fornecedor->setCep($this->post('cep'));
            $fornecedor->setNomeFantasia($this->post('nome_fantasia'));
            $fornecedor->setDesde($this->dataAmerica($this->post('desde')));
            $fornecedor->setPercentualRamo(floatval($this->post('percentual_ramo')));
            $fornecedor->setObservacao($this->post('observacao'));
            $fornecedor->setEmail($this->post('email'));
            $fornecedor->setSenha($this->post('senha'));
            $fornecedor->setDataAtualizacao($dataHoje);

            /** dao **/
            $fornecedorDAO = new FornecedorDAO();
            $fornecedorDAO->atualizar($fornecedor);

            $this->mensagem = "Atualizado com sucesso!";
        }

        /** instancia **/
        $this->fornecedor = new Fornecedor();
        $this->fornecedor->setId($this->acessoSessao->getId());

        /** dao **/
        $fornecedorDAO = new FornecedorDAO();
        $fornecedorDAO->visualizar($this->fornecedor);

        $this->sendview("fornecedor/atualizar");
    }

    public function excluir()
    {

        /** instancia **/
        $fornecedor = new Fornecedor();
        $fornecedor->setId($this->acessoSessao->getId());

        /** dao **/
        $fornecedorDAO = new FornecedorDAO();
        $fornecedorDAO->excluirARN($fornecedor);
        $fornecedorDAO->excluirARS($fornecedor);
        $fornecedorDAO->excluir($fornecedor);

        $this->redirect('logoff');
    }

    public function assocRemedioNatural()
    {
        if ($this->request()) {

            /** instancia fornecedor **/
            $fornecedor = new Fornecedor();
            $fornecedor->setId($this->acessoSessao->getId());

            /** dao fornecedor**/
            $fornecedorDAO = new FornecedorDAO();

            /** instancia remedio natural **/
            $arrayRemedioId = $this->post('remedio');
            if($arrayRemedioId){
                foreach ($arrayRemedioId as $remedioId) {
                    $remedioNatural = new RemedioNatural();
                    $remedioNatural->setId($remedioId);
                    $fornecedor->setRemedioNatural($remedioNatural);
                }

                $fornecedorDAO->desassocRN($fornecedor);
                $fornecedorDAO->assocRN($fornecedor);
                $this->mensagem = "Associado com sucesso!";
            }else{
                $fornecedorDAO->desassocRN($fornecedor);
                $this->mensagem = "Removido com sucesso!";
            }
        }

        /** instancia fornecedor **/
        $this->fornecedor = new Fornecedor();

        /** dao fornecedor **/
        $fornecedorDAO = new FornecedorDAO();
        $this->fornecedor->setId($this->acessoSessao->getId());
        $fornecedorDAO->visualizar($this->fornecedor);
        $fornecedorDAO->visualizarARN($this->fornecedor);

        /** dao remedio sintetico **/
        $remedioNaturalDAO = new RemedioNaturalDAO();
        $this->arrayObjRemedioNatural = $remedioNaturalDAO->listar();

        /** front **/
        $this->sendview("fornecedor/assoc-remedio-natural");
    }



    public function assocRemedioSintetico()
    {
        if ($this->request()) {

            /** instancia fornecedor **/
            $fornecedor = new Fornecedor();
            $fornecedor->setId($this->acessoSessao->getId());

            /** dao  fornecedor**/
            $fornecedorDAO = new FornecedorDAO();

            /** instancia remedio natural **/
            $arrayRemedioId = $this->post('remedio');
            if($arrayRemedioId){
                foreach ($arrayRemedioId as $remedioId) {
                    $remedioSintetico = new RemedioSintetico();
                    $remedioSintetico->setId($remedioId);
                    $fornecedor->setRemedioSintetico($remedioSintetico);
                }

                $fornecedorDAO->desassocRS($fornecedor);
                $fornecedorDAO->assocRS($fornecedor);
                $this->mensagem = "Associado com sucesso!";
            }else{
                $fornecedorDAO->desassocRS($fornecedor);
                $this->mensagem = "Removido com sucesso!";
            }
        }

        /** instancia fornecedor **/
        $this->fornecedor = new Fornecedor();

        /** dao fornecedor **/
        $fornecedorDAO = new FornecedorDAO();
        $this->fornecedor->setId($this->acessoSessao->getId());
        $fornecedorDAO->visualizar($this->fornecedor);
        $fornecedorDAO->visualizarARS($this->fornecedor);

        /** dao remedio sintetico **/
        $remedioSinteticoDAO = new RemedioSinteticoDAO();
        $this->arrayObjRemedioSintetico = $remedioSinteticoDAO->listar();

        /** front **/
        $this->sendview("fornecedor/assoc-remedio-sintetico");
    }


}
