<?php

class DoencaController extends Component
{

    public $medico;
    public $acessoSessao;
    public $doenca;
    public $arrayObjDoencas;
    public $arrayObjRemedioNatural;
    public $arrayObjRemedioSintetico;

    function __construct()
    {
        $this->init();
        $this->acessoSessao = $this->sessaoLogin()['objeto'];
        $this->tipoSessao = $this->sessaoLogin()['tipo'];
    }

    public function listar()
    {
        /** dao **/
        $doencaDAO = new doencaDAO();
        $this->arrayObjDoencas = $doencaDAO->listar();

        /** front **/
        $this->sendview("doenca/listar");
    }

    public function inserir()
    {
        if ($this->request()) {

            /** data atual **/
            $dataHoje = date('Y-m-d');

            /** instancia **/
            $doenca = new Doenca();
            $doenca->setNomePopular($this->post('nome_popular'));
            $doenca->setNomeCientifico($this->post('nome_cientifico'));
            $doenca->setSintomas($this->post('sintomas'));
            $doenca->setPercentualRisco(floatval($this->post('percentual_risco')));
            $doenca->setEspecialidade($this->post('especialidade'));
            $doenca->setMetodoDiagnostico($this->post('metodo_diagnostico'));
            $doenca->setPeriodo($this->post('periodo'));
            $doenca->setDiasDuracao(intval($this->post('dias_duracao')));
            $doenca->setInicio($this->post('inicio'));
            $doenca->setDataDescoberta($this->dataAmerica($this->post('data_descoberta')));
            $doenca->setObservacao($this->post('observacao'));
            $doenca->setDataCriacao($dataHoje);
            $doenca->setDataAtualizacao($dataHoje);

            /** dao **/
            $doencaDAO = new DoencaDAO();
            $doencaDAO->inserir($doenca);

            $this->mensagem = "Cadastrado com sucesso!";
        }

        /** front **/
        $this->sendview("doenca/inserir");
    }


    public function visualizar($id)
    {
        /** instancia doenca **/
        $this->doenca = new Doenca();

        /** dao doenca **/
        $doencaDAO = new DoencaDAO();
        $this->doenca->setId($id);
        $doencaDAO->visualizar($this->doenca);
        $doencaDAO->listarARN($this->doenca);
        $doencaDAO->listarARS($this->doenca);

        /** AGREGANDO MEDICO A DOENÇA **/
        /** dao medico **/
        $medicoDAO = new MedicoDAO();
        $medicoDAO->listarPorEspecializacaoMaior($this->doenca);

        /** front **/
        $this->sendview("doenca/visualizar");
    }


    public function atualizar($id)
    {
        if ($this->request()) {

            /** data atual **/
            $dataHoje = date('Y-m-d');

            /** instancia **/
            $doenca = new Doenca();
            $doenca->setId($id);
            $doenca->setNomePopular($this->post('nome_popular'));
            $doenca->setNomeCientifico($this->post('nome_cientifico'));
            $doenca->setSintomas($this->post('sintomas'));
            $doenca->setPercentualRisco(floatval($this->post('percentual_risco')));
            $doenca->setEspecialidade($this->post('especialidade'));
            $doenca->setMetodoDiagnostico($this->post('metodo_diagnostico'));
            $doenca->setPeriodo($this->post('periodo'));
            $doenca->setDiasDuracao(intval($this->post('dias_duracao')));
            $doenca->setInicio($this->post('inicio'));
            $doenca->setDataDescoberta($this->dataAmerica($this->post('data_descoberta')));
            $doenca->setObservacao($this->post('observacao'));
            $doenca->setDataAtualizacao($dataHoje);

            /** dao **/
            $doencaDAO = new DoencaDAO();
            $doencaDAO->atualizar($doenca);

            $this->mensagem = "Atualizado com sucesso!";
        }

        /** instancia doenca **/
        $this->doenca = new Doenca();

        /** dao doenca **/
        $doencaDAO = new DoencaDAO();
        $this->doenca->setId($id);
        $doencaDAO->visualizar($this->doenca);

        /** front **/
        $this->sendview("doenca/atualizar");
    }

    public function excluir($id)
    {
        /** instancia **/
        $doenca = new Doenca();
        $doenca->setId($id);

        /** dao **/
        $doencaDAO = new DoencaDAO();
        $doencaDAO->excluirARN($doenca);
        $doencaDAO->excluirARS($doenca);
        $doencaDAO->excluir($doenca);

        $this->redirect('doenca/listar');
    }

    public function assocRemedioNatural($id)
    {
        if ($this->request()) {

            /** instancia doenca **/
            $doenca = new Doenca();
            $doenca->setId($id);

            /** dao  doenca**/
            $doencaDAO = new DoencaDAO();

            /** instancia remedio natural **/
            $arrayRemedioId = $this->post('remedio');
            if($arrayRemedioId){
                foreach ($arrayRemedioId as $remedioId) {
                    $remedioNatural = new RemedioNatural();
                    $remedioNatural->setId($remedioId);
                    $doenca->setRemedioNatural($remedioNatural);
                }

                $doencaDAO->desassocRN($doenca);
                $doencaDAO->assocRN($doenca);
                $this->mensagem = "Associado com sucesso!";
            }else{
                $doencaDAO->desassocRN($doenca);
                $this->mensagem = "Removido com sucesso!";
            }
        }

        /** instancia doenca **/
        $this->doenca = new Doenca();

        /** dao doenca **/
        $doencaDAO = new DoencaDAO();
        $this->doenca->setId($id);
        $doencaDAO->visualizar($this->doenca);
        $doencaDAO->visualizarARN($this->doenca);

        /** dao remedio sintetico **/
        $remedioNaturalDAO = new RemedioNaturalDAO();
        $this->arrayObjRemedioNatural = $remedioNaturalDAO->listar();

        /** front **/
        $this->sendview("doenca/assoc-remedio-natural");
    }



    public function assocRemedioSintetico($id)
    {
        if ($this->request()) {

            /** instancia doenca **/
            $doenca = new Doenca();
            $doenca->setId($id);

            /** dao  doenca**/
            $doencaDAO = new DoencaDAO();

            /** instancia remedio natural **/
            $arrayRemedioId = $this->post('remedio');
            if($arrayRemedioId){
                foreach ($arrayRemedioId as $remedioId) {
                    $remedioSintetico = new RemedioSintetico();
                    $remedioSintetico->setId($remedioId);
                    $doenca->setRemedioSintetico($remedioSintetico);
                }

                $doencaDAO->desassocRS($doenca);
                $doencaDAO->assocRS($doenca);
                $this->mensagem = "Associado com sucesso!";
            }else{
                $doencaDAO->desassocRS($doenca);
                $this->mensagem = "Removido com sucesso!";
            }
        }

        /** instancia doenca **/
        $this->doenca = new Doenca();

        /** dao doenca **/
        $doencaDAO = new DoencaDAO();
        $this->doenca->setId($id);
        $doencaDAO->visualizar($this->doenca);
        $doencaDAO->visualizarARS($this->doenca);

        /** dao remedio sintetico **/
        $remedioSinteticoDAO = new RemedioSinteticoDAO();
        $this->arrayObjRemedioSintetico = $remedioSinteticoDAO->listar();

        /** front **/
        $this->sendview("doenca/assoc-remedio-sintetico");
    }


}