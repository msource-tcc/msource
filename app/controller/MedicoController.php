<?php

class MedicoController extends Component
{

    public $medico;
    public $acessoSessao;

    function __construct()
    {
        $this->init();
        $this->acessoSessao = $this->sessaoLogin()['objeto'];
        $this->tipoSessao = $this->sessaoLogin()['tipo'];
    }

    public function listar(){

        /** dao **/
        $medicoDAO = new MedicoDAO();
        $this->medicoArray = $medicoDAO->listar();

        /** front **/
        $this->tipoSessao = "administrador";
        $this->sendview("medico/listar");
    }


    public function inserir()
    {
        if ($this->request()) {
            /** data atual **/
            $dataHoje = date('Y-m-d');

            /** instancia **/
            $medico = new Medico();
            $medico->setNome($this->post('nome'));
            $medico->setEndereco($this->post('endereco'));
            $medico->setNumero(intval($this->post('numero')));
            $medico->setComplemento($this->post('complemento'));
            $medico->setBairro($this->post('bairro'));
            $medico->setCidade($this->post('cidade'));
            $medico->setUf($this->post('uf'));
            $medico->setCep($this->post('cep'));
            $medico->setSexo($this->post('sexo'));
            $medico->setDataNascimento($this->dataAmerica($this->post('data_nascimento')));
            $medico->setNacionalidade($this->post('nacionalidade'));
            $medico->setEspRemedioNat(floatval($this->post('esp_remedio_nat')));
            $medico->setEspRemedioSint(floatval($this->post('esp_remedio_sint')));
            $medico->setCrm($this->post('crm'));
            $medico->setObservacao($this->post('observacao'));
            $medico->setEmail($this->post('email'));
            $medico->setSenha($this->post('senha'));
            $medico->setDataCriacao($dataHoje);
            $medico->setDataAtualizacao($dataHoje);

            /** instancia **/
            $medicoDAO = new MedicoDAO();
            $medicoDAO->inserir($medico);

            $this->mensagem = "Cadastrado com sucesso";
        }

        /** front **/
        $this->tipoSessao = "administrador";
        $this->sendview("medico/inserir");
    }

    public function visualizar($id)
    {
        /** instancia **/
        $this->medico = new Medico();
        $this->medico->setId($id);

        /** dao **/
        $medicoDAO = new MedicoDAO();
        $medicoDAO->visualizar($this->medico);

        /** front **/
        $this->sendview("medico/visualizar");
    }

    public function atualizar($id)
    {
        if ($this->request()) {

            /** data atual **/
            $dataHoje = date('Y-m-d');

            /** medico **/
            $medico = new Medico();
            $medico->setId($id);
            $medico->setNome($this->post('nome'));
            $medico->setEndereco($this->post('endereco'));
            $medico->setNumero(intval($this->post('numero')));
            $medico->setComplemento($this->post('complemento'));
            $medico->setBairro($this->post('bairro'));
            $medico->setCidade($this->post('cidade'));
            $medico->setUf($this->post('uf'));
            $medico->setCep($this->post('cep'));
            $medico->setSexo($this->post('sexo'));
            $medico->setDataNascimento($this->dataAmerica($this->post('data_nascimento')));
            $medico->setNacionalidade($this->post('nacionalidade'));
            $medico->setEspRemedioNat(floatval($this->post('esp_remedio_nat')));
            $medico->setEspRemedioSint(floatval($this->post('esp_remedio_sint')));
            $medico->setCrm($this->post('crm'));
            $medico->setObservacao($this->post('observacao'));
            $medico->setEmail($this->post('email'));
            $medico->setSenha($this->post('senha'));
            $medico->setDataAtualizacao($dataHoje);

            /** dao **/
            $medicoDAO = new MedicoDAO();
            $medicoDAO->atualizar($medico);
            $this->mensagem = "Atualizado com sucesso!";
        }

        /** instancia **/
        $this->medico = new Medico();
        $this->medico->setId($id);

        /** dao **/
        $medicoDAO = new MedicoDAO();
        $medicoDAO->visualizar($this->medico);

        /** front **/
        $this->tipoSessao = "administrador";
        $this->sendview("medico/atualizar");
    }

    public function excluir($id)
    {
        /** instancia **/
        $medico = new Medico();
        $medico->setId($id);

        /** dao **/
        $medicoDAO = new MedicoDAO();
        $medicoDAO->excluir($medico);

        $this->redirect('medico/listar');
    }


}
