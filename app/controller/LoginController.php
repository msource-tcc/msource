<?php

class LoginController extends Component
{

    public function index()
    {
        $this->isLogin();
        $this->sendview("login/index");
    }

    /** administrador **/
    public function administrador()
    {
        $this->isLogin();
        if ($this->request()){

            /** instancia **/
            $administrador = new Administrador();
            $administrador->setEmail($this->post('email'));
            $administrador->setSenha($this->post('senha'));

            /** dao **/
            $AdministradorDAO = new AdministradorDAO();
            $AdministradorDAO->validarAdministrador($administrador);

            if ($administrador->getId()) {
                $_SESSION['login']['logged'] = true;
                $_SESSION['login']['tipo'] = "administrador";
                $_SESSION['login']['objeto'] = $administrador;
                $this->redirect('dashboard');
            } else {
                $this->mensagem = "e-mail ou senha inválidos!";
            }
        }

        /** front **/
        $this->sendview("login/administrador");
    }


    /** paciente **/
    public function paciente()
    {
        $this->isLogin();
        if ($this->request()) {

            /** instancia **/
            $paciente = new Paciente();
            $paciente->setEmail($this->post('email'));
            $paciente->setSenha($this->post('senha'));

            /** dao **/
            $pacienteDAO = new PacienteDAO();
            $arrayPaciente = $pacienteDAO->validarPaciente($paciente);

            if (!empty($arrayPaciente)) {
                $paciente->setId($arrayPaciente['id']);
                $paciente->setNome($arrayPaciente['nome']);

                session_start();
                $_SESSION['login']['logged'] = true;
                $_SESSION['login']['tipo'] = "paciente";
                $_SESSION['login']['objeto'] = $paciente;
                $this->redirect('dashboard');
            } else {
                $this->mensagem = "e-mail ou senha inválidos!";
            }
        }

        /** front **/
        $this->sendview("login/paciente");
    }


    public function fornecedor()
    {
        $this->isLogin();

        if ($this->request()) {

            /** instancia **/
            $fornecedor = new Fornecedor();
            $fornecedor->setEmail($this->post('email'));
            $fornecedor->setSenha($this->post('senha'));

            /** dao **/
           $fornecedorDAO = new FornecedorDAO();
           $arrayFornecedor = $fornecedorDAO->validarFornecedor($fornecedor);

            if (!empty($arrayFornecedor)) {
                $fornecedor->setId($arrayFornecedor['id']);
                $fornecedor->setNome($arrayFornecedor['nome']);

                session_start();
                $_SESSION['login']['logged'] = true;
                $_SESSION['login']['tipo'] = "fornecedor";
                $_SESSION['login']['objeto'] = $fornecedor;
                $this->redirect('dashboard');
            } else {
                $this->mensagem = "e-mail ou senha inválidos!";
            }
        }
        $this->sendview("login/fornecedor");
    }

//AUTENTICANDO MEDICO
    public function medico()
    {
        $this->isLogin();

        if ($this->request()) {

//INSTANCIA USUARIO
            $medico = new Medico();
            $medico->setEmail($this->post('email'));
            $medico->setSenha($this->post('senha'));

//INSTANCIA PERSISTENCIA
            $medicoDAO = new MedicoDAO();
            $arrayMedico = $medicoDAO->validarMedico($medico);

            if (!empty($arrayMedico)) {
                $medico->setId($arrayMedico['id']);
                $medico->setNome($arrayMedico['nome']);

//INICIANDO SESSÃO
                session_start();
                $_SESSION['login']['logged'] = true;
                $_SESSION['login']['tipo'] = "medico";
                $_SESSION['login']['objeto'] = $medico;
                $this->redirect('dashboard');
            } else {
                $this->mensagem = "e-mail ou senha inválidos!";
            }
        }
        $this->sendview("login/medico");
    }

//VALIDANDO SESSAO
    public function isLogin()
    {
        session_start();
        if (isset($_SESSION['login']['logged']) && ($_SESSION['login']['tipo'] == "paciente" || $_SESSION['login']['tipo'] == "fornecedor" || $_SESSION['login']['tipo'] == "medico")) {
            $this->redirect("dashboard");
        }
    }

//DESTRUINDO SESSAO
    public function logoff()
    {
        session_start();
        session_unset();
        $this->redirect("");
    }
}
