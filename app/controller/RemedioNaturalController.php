<?php

class RemedioNaturalController extends Component
{

    public $acessoSessao;
    public $tipoSessao;
    public $remedioNatural;

    function __construct()
    {
        $this->init();
        $this->acessoSessao = $this->sessaoLogin()['objeto'];
        $this->tipoSessao = $this->sessaoLogin()['tipo'];
    }

    public function listar(){

        /** dao **/
        $remedioNaturalDAO = new RemedioNaturalDAO();
        $this->remedioNaturalArray = $remedioNaturalDAO->listar();

        /** front **/
        $this->sendview("remedio-natural/listar");
    }

    public function inserir()
    {
        if ($this->request()) {

            /** data atual **/
            $dataHoje = date('Y-m-d');

            /** instancia **/
            $remedioNatural = new RemedioNatural();
            $remedioNatural->setNomePopular($this->post('nome_popular'));
            $remedioNatural->setNomeCientifico($this->post('nome_cientifico'));
            $remedioNatural->setClasse($this->post('classe'));
            $remedioNatural->setFamilia($this->post('familia'));
            $remedioNatural->setGenero($this->post('genero'));
            $remedioNatural->setReino($this->post('reino'));
            $remedioNatural->setQuilograma($this->post('quilograma'));
            $remedioNatural->setCalorias($this->post('calorias'));
            $remedioNatural->setCorNatural($this->post('cor_natural'));
            $remedioNatural->setDataDescoberta($this->dataAmerica($this->post('data_descoberta')));
            $remedioNatural->setFinsMedicinais($this->post('fins_medicinais'));
            $remedioNatural->setDataCriacao($dataHoje);
            $remedioNatural->setDataAtualizacao($dataHoje);

            /** dao **/
            $remedioNaturalDAO = new RemedioNaturalDAO();
            $remedioNaturalDAO->inserir($remedioNatural);

            $this->mensagem = "Inserido com sucesso!";
        }

        /** front **/
        $this->sendview("remedio-natural/inserir");
    }

    public function visualizar($id)
    {
        /** instancia **/
        $this->remedioNatural = new RemedioNatural();
        $this->remedioNatural->setId($id);

        /** dao **/
        $remedioNaturalDAO = new remedioNaturalDAO();
        $remedioNaturalDAO->visualizar($this->remedioNatural);
        $remedioNaturalDAO->listarAssocDoenca($this->remedioNatural);
        $remedioNaturalDAO->listarAssocFornecedor($this->remedioNatural);

        /** AGREGANDO MEDICO AO REMEDIO **/
        $remedioNaturalDAO->listarMedicoEspecialista($this->remedioNatural);

        /** front **/
        $this->sendview("remedio-natural/visualizar");
    }

    public function atualizar($id)
    {
        if ($this->request()) {

            /** data atual **/
            $dataHoje = date('Y-m-d');

            /** instancia **/
            $remedioNatural = new RemedioNatural();
            $remedioNatural->setId($id);
            $remedioNatural->setNomePopular($this->post('nome_popular'));
            $remedioNatural->setNomeCientifico($this->post('nome_cientifico'));
            $remedioNatural->setClasse($this->post('classe'));
            $remedioNatural->setFamilia($this->post('familia'));
            $remedioNatural->setGenero($this->post('genero'));
            $remedioNatural->setReino($this->post('reino'));
            $remedioNatural->setQuilograma($this->post('quilograma'));
            $remedioNatural->setCalorias($this->post('calorias'));
            $remedioNatural->setCorNatural($this->post('cor_natural'));
            $remedioNatural->setDataDescoberta($this->dataAmerica($this->post('data_descoberta')));
            $remedioNatural->setFinsMedicinais($this->post('fins_medicinais'));
            $remedioNatural->setDataAtualizacao($dataHoje);

            /** dao **/
            $remedioNaturalDAO = new RemedioNaturalDAO();
            $remedioNaturalDAO->atualizar($remedioNatural);

            $this->mensagem = "Atualizado com sucesso!";
        }

        /** instancia **/
        $this->remedioNatural = new RemedioNatural();
        $this->remedioNatural->setId($id);

        /** dao **/
        $remedioNaturalDAO = new RemedioNaturalDAO();
        $remedioNaturalDAO->visualizar($this->remedioNatural);

        /** front **/
        $this->sendview("remedio-natural/atualizar");
    }

    public function excluir($id)
    {
        /** instancia **/
        $remedioNatural = new RemedioNatural();
        $remedioNatural->setId($id);

        /** dao **/
        $remedioNaturalDAO = new remedioNaturalDAO();
        $remedioNaturalDAO->excluirAssocFornecedor($remedioNatural);
        $remedioNaturalDAO->excluirAssocDoenca($remedioNatural);
        $remedioNaturalDAO->excluir($remedioNatural);

        $this->redirect('remedio-natural/listar');
    }

}
