<?php

class HospitalController extends Component
{

    public $acessoSessao;
    public $hospital;
    public $hospitalArray;

    function __construct()
    {
        $this->init();
        $this->acessoSessao = $this->sessaoLogin()['objeto'];
        $this->tipoSessao = $this->sessaoLogin()['tipo'];
    }

    public function listar(){

        /** dao **/
        $hospitalDAO = new hospitalDAO();
        $this->hospitalArray = $hospitalDAO->listar();

        /** front **/
        $this->sendview("hospital/listar");
    }

    public function inserir()
    {
        if ($this->request()) {

            /** data atual **/
            $dataHoje = date('Y-m-d');

            /** instancia **/
            $hospital = new Hospital();
            $hospital->setNome($this->post('nome'));
            $hospital->setEndereco($this->post('endereco'));
            $hospital->setNumero(intval($this->post('numero')));
            $hospital->setComplemento($this->post('complemento'));
            $hospital->setBairro($this->post('bairro'));
            $hospital->setCidade($this->post('cidade'));
            $hospital->setUf($this->post('uf'));
            $hospital->setCep($this->post('cep'));
            $hospital->setDesde($this->dataAmerica($this->post('desde')));
            $hospital->setAvaliacao(floatval($this->post('avaliacao')));
            $hospital->setObservacao($this->post('observacao'));
            $hospital->setDataCriacao($dataHoje);
            $hospital->setDataAtualizacao($dataHoje);

            /** dao **/
            $hospitalDAO = new hospitalDAO();
            $hospitalDAO->inserir($hospital);

            $this->mensagem = "Inserido com sucesso!";
        }

        /** front **/
        $this->sendview("hospital/inserir");
    }

    public function visualizar($id)
    {
        /** instancia **/
        $this->hospital = new Hospital();
        $this->hospital->setId($id);

        /** dao **/
        $hospitalDAO = new hospitalDAO();
        $hospitalDAO->visualizar($this->hospital);

        /** front **/
        $this->sendview("hospital/visualizar");
    }

    public function atualizar($id)
    {
        if ($this->request()){

            /** data atual **/
            $dataHoje = date('Y-m-d');

            /** instancia **/
            $hospital = new Hospital();
            $hospital->setId($id);
            $hospital->setNome($this->post('nome'));
            $hospital->setEndereco($this->post('endereco'));
            $hospital->setNumero(intval($this->post('numero')));
            $hospital->setComplemento($this->post('complemento'));
            $hospital->setBairro($this->post('bairro'));
            $hospital->setCidade($this->post('cidade'));
            $hospital->setUf($this->post('uf'));
            $hospital->setCep($this->post('cep'));
            $hospital->setDesde($this->dataAmerica($this->post('desde')));
            $hospital->setAvaliacao(floatval($this->post('avaliacao')));
            $hospital->setObservacao($this->post('observacao'));
            $hospital->setDataAtualizacao($dataHoje);

            /** dao **/
            $hospitalDAO = new hospitalDAO();
            $hospitalDAO->atualizar($hospital);

            $this->mensagem = "Atualizado com sucesso!";
        }

        /** instancia **/
        $this->hospital = new Hospital();
        $this->hospital->setId($id);

        /** dao **/
        $hospitalDAO = new hospitalDAO();
        $hospitalDAO->visualizar($this->hospital);

        /** front **/
        $this->sendview("hospital/atualizar");
    }

    public function excluir($id)
    {
        /** instancia **/
        $this->hospital = new Hospital();
        $this->hospital->setId($id);

        /** dao **/
        $hospitalDAO = new hospitalDAO();
        $hospitalDAO->excluir($this->hospital);

        $this->redirect('hospital/listar');
    }

}
