<?php
/**
 * Created by PhpStorm.
 * User: Paulo Teixeira
 * Date: 06/05/2018
 * Time: 00:45
 */


require_once ('app/model/remedio/Remedio.php');

class RemedioNatural extends Remedio
{
    private $classe;
    private $familia;
    private $genero;
    private $reino;
    private $doenca;
    private $fornecedor;
    private $medico;

    /**
     * @return mixed
     */
    public function getClasse()
    {
        return $this->classe;
    }

    /**
     * @param mixed $classe
     */
    public function setClasse($classe)
    {
        $this->classe = $classe;
    }

    /**
     * @return mixed
     */
    public function getFamilia()
    {
        return $this->familia;
    }

    /**
     * @param mixed $familia
     */
    public function setFamilia($familia)
    {
        $this->familia = $familia;
    }

    /**
     * @return mixed
     */
    public function getGenero()
    {
        return $this->genero;
    }

    /**
     * @param mixed $genero
     */
    public function setGenero($genero)
    {
        $this->genero = $genero;
    }

    /**
     * @return mixed
     */
    public function getReino()
    {
        return $this->reino;
    }

    /**
     * @param mixed $reino
     */
    public function setReino($reino)
    {
        $this->reino = $reino;
    }

    /**
     * @return mixed
     */
    public function getDoenca()
    {
        return $this->doenca;
    }

    /**
     * @param mixed $doenca
     */
    public function setDoenca(Doenca $doenca)
    {
        $this->doenca[] = $doenca;
    }

    /**
     * @return mixed
     */
    public function getFornecedor()
    {
        return $this->fornecedor;
    }

    /**
     * @param mixed $fornecedor
     */
    public function setFornecedor(Fornecedor $fornecedor)
    {
        $this->fornecedor[] = $fornecedor;
    }

    /**
     * @return mixed
     */
    public function getMedico()
    {
        return $this->medico;
    }

    /**
     * @param mixed $medico
     */
    public function setMedico(Medico $medico)
    {
        $this->medico[] = $medico;
    }


}