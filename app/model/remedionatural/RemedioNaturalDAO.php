<?php

class RemedioNaturalDAO extends EntityModel{

    public $remedio = array();

    public function listar(){
        try{
            $sql = "SELECT * FROM remedio_natural";

            $this->save = $this->start->prepare($sql);
            $this->save->execute();
            $arrayObj = $this->save->fetchAll(PDO::FETCH_OBJ);

            if(!empty($arrayObj)){
                foreach ($arrayObj as $obj){

                    $remedioNatural = new RemedioNatural();
                    $remedioNatural->setId($obj->id);
                    $remedioNatural->setNomePopular($obj->nome_popular);
                    $remedioNatural->setNomeCientifico($obj->nome_cientifico);
                    $remedioNatural->setClasse($obj->classe);
                    $remedioNatural->setFamilia($obj->familia);
                    $remedioNatural->setGenero($obj->genero);
                    $remedioNatural->setReino($obj->reino);
                    $remedioNatural->setQuilograma($obj->quilograma);
                    $remedioNatural->setCalorias($obj->calorias);
                    $remedioNatural->setCorNatural($obj->cor_natural);
                    $remedioNatural->setDataDescoberta($this->dataBrasil($obj->data_descoberta));
                    $remedioNatural->setFinsMedicinais($obj->fins_medicinais);
                    $remedioNatural->setDataCriacao($this->dataBrasil($obj->data_criacao));
                    $remedioNatural->setDataAtualizacao($this->dataBrasil($obj->data_atualizacao));

                    $arrayRemedioNatural[] = $remedioNatural;
                    }
                    return $arrayRemedioNatural;
                }else{
                    return [];
            }

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function inserir($remedioNatural){
        try{
            $sql = "INSERT INTO remedio_natural
                    (
                    nome_popular,
                    nome_cientifico,
                    classe, 
                    familia, 
                    genero, 
                    reino,
                    quilograma, 
                    calorias, 
                    cor_natural, 
                    data_descoberta, 
                    fins_medicinais,
                    data_criacao, 
                    data_atualizacao
                    )
                    VALUE (?,?,?,?,?,?,?,?,?,?,?,?,?)";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1,$remedioNatural->getNomePopular());
            $this->save->bindValue(2,$remedioNatural->getNomeCientifico());
            $this->save->bindValue(3,$remedioNatural->getClasse());
            $this->save->bindValue(4,$remedioNatural->getFamilia());
            $this->save->bindValue(5,$remedioNatural->getGenero());
            $this->save->bindValue(6,$remedioNatural->getReino());
            $this->save->bindValue(7,$remedioNatural->getQuilograma());
            $this->save->bindValue(8,$remedioNatural->getCalorias());
            $this->save->bindValue(9,$remedioNatural->getCorNatural());
            $this->save->bindValue(10,$remedioNatural->getDataDescoberta());
            $this->save->bindValue(11,$remedioNatural->getFinsMedicinais());
            $this->save->bindValue(12,$remedioNatural->getDataCriacao());
            $this->save->bindValue(13,$remedioNatural->getDataAtualizacao());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function visualizar($remedioNatural){
        try{

            $sql = "SELECT * FROM remedio_natural WHERE id = ?";

            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $remedioNatural->getId());
            $this->save->execute();

            $obj = $this->save->fetch(PDO::FETCH_OBJ);

            if($obj){
                $remedioNatural->setId($obj->id);
                $remedioNatural->setNomePopular($obj->nome_popular);
                $remedioNatural->setNomeCientifico($obj->nome_cientifico);
                $remedioNatural->setClasse($obj->classe);
                $remedioNatural->setFamilia($obj->familia);
                $remedioNatural->setGenero($obj->genero);
                $remedioNatural->setReino($obj->reino);
                $remedioNatural->setQuilograma($obj->quilograma);
                $remedioNatural->setCalorias($obj->calorias);
                $remedioNatural->setCorNatural($obj->cor_natural);
                $remedioNatural->setDataDescoberta($this->dataBrasil($obj->data_descoberta));
                $remedioNatural->setFinsMedicinais($obj->fins_medicinais);
                $remedioNatural->setDataCriacao($this->dataBrasil($obj->data_criacao));
                $remedioNatural->setDataAtualizacao($this->dataBrasil($obj->data_atualizacao));
            }

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function atualizar($remedioNatural){
        try{
            $sql = "UPDATE remedio_natural 
                    SET 
                    nome_popular = ?,
                    nome_cientifico = ?,
                    classe = ?, 
                    familia = ?, 
                    genero = ?, 
                    reino = ?,
                    quilograma = ?, 
                    calorias = ?, 
                    cor_natural = ?, 
                    data_descoberta = ?, 
                    fins_medicinais = ?,
                    data_atualizacao = ?
                    WHERE id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1,$remedioNatural->getNomePopular());
            $this->save->bindValue(2,$remedioNatural->getNomeCientifico());
            $this->save->bindValue(3,$remedioNatural->getClasse());
            $this->save->bindValue(4,$remedioNatural->getFamilia());
            $this->save->bindValue(5,$remedioNatural->getGenero());
            $this->save->bindValue(6,$remedioNatural->getReino());
            $this->save->bindValue(7,$remedioNatural->getQuilograma());
            $this->save->bindValue(8,$remedioNatural->getCalorias());
            $this->save->bindValue(9,$remedioNatural->getCorNatural());
            $this->save->bindValue(10,$remedioNatural->getDataDescoberta());
            $this->save->bindValue(11,$remedioNatural->getFinsMedicinais());
            $this->save->bindValue(12,$remedioNatural->getDataAtualizacao());
            $this->save->bindValue(13,$remedioNatural->getId());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }


    public function excluir($remedioNatural){
        try{
            $sql = "DELETE FROM remedio_natural WHERE id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $remedioNatural->getId());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function excluirAssocFornecedor($remedioNatural){
        try{
            $sql = "DELETE FROM fornecedor_has_remedio_natural WHERE remedio_id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $remedioNatural->getId());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function excluirAssocDoenca($remedioNatural){
        try{
            $sql = "DELETE FROM doenca_has_remedio_natural WHERE remedio_id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $remedioNatural->getId());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }


    public function validarMedico($medico){
        try{
            $sql = "SELECT id, nome FROM medico WHERE email = ? AND senha = ? limit 1";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);

            $this->save->bindValue(1, $medico->getEmail());
            $this->save->bindValue(2, $medico->getSenha());

            $this->save->execute();
            $this->start->commit();
            return $this->save->fetch(PDO::FETCH_ASSOC);
        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function listarAssocDoenca($remedioNatural){
        try{

            $sql = "SELECT doenca.* FROM doenca_has_remedio_natural 
                    INNER JOIN doenca on doenca.id = doenca_has_remedio_natural.doenca_id
                    WHERE doenca_has_remedio_natural.remedio_id = ?";

            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $remedioNatural->getId());
            $this->save->execute();
            $arrayObj = $this->save->fetchAll(PDO::FETCH_OBJ);

            if($arrayObj){
                foreach ($arrayObj as $obj) {
                    $doenca = new Doenca();
                    $doenca->setId($obj->id);
                    $doenca->setNomePopular($obj->nome_popular);
                    $doenca->setNomeCientifico($obj->nome_cientifico);
                    $doenca->setSintomas($obj->sintomas);
                    $doenca->setPercentualRisco($obj->percentual_risco);
                    $doenca->setEspecialidade($obj->especialidade);
                    $doenca->setMetodoDiagnostico($obj->metodo_diagnostico);
                    $doenca->setPeriodo($obj->periodo);
                    $doenca->setDiasDuracao($obj->dias_duracao);
                    $doenca->setInicio($obj->inicio);
                    $doenca->setDataDescoberta($this->dataBrasil($obj->data_descoberta));
                    $doenca->setObservacao($obj->observacao);
                    $doenca->setDataCriacao($this->dataBrasil($obj->data_criacao));
                    $doenca->setDataAtualizacao($this->dataBrasil($obj->data_atualizacao));

                    $remedioNatural->setDoenca($doenca);
                }
            }

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }


    public function listarAssocFornecedor($remedioNatural){
        try{

            $sql = "SELECT fornecedor.* FROM fornecedor_has_remedio_natural 
                    INNER JOIN fornecedor on fornecedor.id = fornecedor_has_remedio_natural.fornecedor_id
                    WHERE fornecedor_has_remedio_natural.remedio_id = ?";

            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $remedioNatural->getId());
            $this->save->execute();
            $arrayObj = $this->save->fetchAll(PDO::FETCH_OBJ);

            if($arrayObj){
                foreach ($arrayObj as $obj) {
                    $fornecedor = new Fornecedor();
                    $fornecedor->setNome($obj->nome);
                    $fornecedor->setEndereco($obj->endereco);
                    $fornecedor->setNumero($obj->numero);
                    $fornecedor->setComplemento($obj->complemento);
                    $fornecedor->setBairro($obj->bairro);
                    $fornecedor->setCidade($obj->cidade);
                    $fornecedor->setUf($obj->uf);
                    $fornecedor->setCep($obj->cep);
                    $fornecedor->setEmail($obj->email);
                    $fornecedor->setSenha($obj->senha);
                    $fornecedor->setDataCriacao($this->dataBrasil($obj->data_criacao));
                    $fornecedor->setDataAtualizacao($this->dataBrasil($obj->data_atualizacao));
                    $fornecedor->setNomeFantasia($obj->nome_fantasia);
                    $fornecedor->setPercentualRamo($obj->percentual_ramo);
                    $fornecedor->setObservacao($obj->observacao);

                    $remedioNatural->setFornecedor($fornecedor);
                }
            }

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function listarMedicoEspecialista($remedioNatural){
        try{

            $sql = "SELECT * FROM medico WHERE esp_remedio_nat > 0.0";

            $this->save = $this->start->prepare($sql);
            $this->save->execute();
            $arrayObj = $this->save->fetchAll(PDO::FETCH_OBJ);

            if($arrayObj){
                foreach ($arrayObj as $obj) {
                    $medico = new Medico();
                    $medico->setId($obj->id);
                    $medico->setNome($obj->nome);
                    $medico->setEndereco($obj->endereco);
                    $medico->setNumero($obj->numero);
                    $medico->setComplemento($obj->complemento);
                    $medico->setBairro($obj->bairro);
                    $medico->setCidade($obj->cidade);
                    $medico->setUf($obj->uf);
                    $medico->setCep($obj->cep);
                    $medico->setEmail($obj->email);
                    $medico->setSenha($obj->senha);
                    $medico->setDataCriacao($this->dataBrasil($obj->data_criacao));
                    $medico->setDataAtualizacao($this->dataBrasil($obj->data_atualizacao));
                    $medico->setSexo($obj->sexo);
                    $medico->setDataNascimento($this->dataBrasil($obj->data_nascimento));
                    $medico->setNacionalidade($obj->nacionalidade);
                    $medico->setCrm($obj->crm);
                    $medico->setEspRemedioNat($obj->esp_remedio_nat);
                    $medico->setEspRemedioSint($obj->esp_remedio_sint);
                    $medico->setObservacao($obj->observacao);
                    $remedioNatural->setMedico($medico);
               }
            }

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }
}
 ?>
