<?php

class PacienteDAO extends EntityModel{

    public function inserir($paciente){
        try{
            $sql = "INSERT INTO paciente (nome, endereco, numero, complemento, bairro,
                    cidade, uf, cep, peso, altura, nacionalidade, data_nascimento, sexo,
                    nome_pai, nome_mae, observacao, email, senha, data_criacao, data_atualizacao)
                    VALUE (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $paciente->getNome());
            $this->save->bindValue(2, $paciente->getEndereco());
            $this->save->bindValue(3, $paciente->getNumero());
            $this->save->bindValue(4, $paciente->getComplemento());
            $this->save->bindValue(5, $paciente->getBairro());
            $this->save->bindValue(6, $paciente->getCidade());
            $this->save->bindValue(7, $paciente->getUf());
            $this->save->bindValue(8, $paciente->getCep());
            $this->save->bindValue(9, $paciente->getPeso());
            $this->save->bindValue(10, $paciente->getAltura());
            $this->save->bindValue(11, $paciente->getNacionalidade());
            $this->save->bindValue(12, $paciente->getDataNascimento());
            $this->save->bindValue(13, $paciente->getSexo());
            $this->save->bindValue(14, $paciente->getNomePai());
            $this->save->bindValue(15, $paciente->getNomeMae());
            $this->save->bindValue(16, $paciente->getObservacao());
            $this->save->bindValue(17, $paciente->getEmail());
            $this->save->bindValue(18, $paciente->getSenha());
            $this->save->bindValue(19, $paciente->getDataCriacao());
            $this->save->bindValue(20, $paciente->getDataAtualizacao());
            $this->save->execute();
            $paciente->setId($this->start->lastInsertId());
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }


    public function visualizar($paciente){
        try{
            $sql = "SELECT * FROM paciente WHERE id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $paciente->getId());
            $this->save->execute();
            $this->start->commit();

            $obj = $this->save->fetch(PDO::FETCH_OBJ);

            if($obj){
                $paciente->setNome($obj->nome);
                $paciente->setEndereco($obj->endereco);
                $paciente->setNumero($obj->numero);
                $paciente->setComplemento($obj->complemento);
                $paciente->setBairro($obj->bairro);
                $paciente->setCidade($obj->cidade);
                $paciente->setUf($obj->uf);
                $paciente->setCep($obj->cep);
                $paciente->setPeso($obj->peso);
                $paciente->setAltura($obj->altura);
                $paciente->setNacionalidade($obj->nacionalidade);
                $paciente->setDataNascimento($this->dataBrasil($obj->data_nascimento));
                $paciente->setSexo($obj->sexo);
                $paciente->setNomePai($obj->nome_pai);
                $paciente->setNomeMae($obj->nome_mae);
                $paciente->setObservacao($obj->observacao);
                $paciente->setEmail($obj->email);
                $paciente->setSenha($obj->senha);
                $paciente->setDataCriacao($this->dataBrasil($obj->data_criacao));
                $paciente->setDataAtualizacao($this->dataBrasil($obj->data_atualizacao));
            }

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }


    public function atualizar($paciente){
        try{
            $sql = "UPDATE paciente
                    SET nome = ?, endereco = ?, numero = ?, complemento = ?, bairro = ?,
                    cidade = ?, uf = ?, cep = ?, peso = ?, altura = ?, nacionalidade = ?, data_nascimento = ?, sexo = ?,
                    nome_pai = ?, nome_mae = ?, observacao = ?, email = ?, senha = ?, data_atualizacao = ?
                    WHERE id = ?";


            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $paciente->getNome());
            $this->save->bindValue(2, $paciente->getEndereco());
            $this->save->bindValue(3, $paciente->getNumero());
            $this->save->bindValue(4, $paciente->getComplemento());
            $this->save->bindValue(5, $paciente->getBairro());
            $this->save->bindValue(6, $paciente->getCidade());
            $this->save->bindValue(7, $paciente->getUf());
            $this->save->bindValue(8, $paciente->getCep());
            $this->save->bindValue(9, $paciente->getPeso());
            $this->save->bindValue(10, $paciente->getAltura());
            $this->save->bindValue(11, $paciente->getNacionalidade());
            $this->save->bindValue(12, $paciente->getDataNascimento());
            $this->save->bindValue(13, $paciente->getSexo());
            $this->save->bindValue(14, $paciente->getNomePai());
            $this->save->bindValue(15, $paciente->getNomeMae());
            $this->save->bindValue(16, $paciente->getObservacao());
            $this->save->bindValue(17, $paciente->getEmail());
            $this->save->bindValue(18, $paciente->getSenha());
            $this->save->bindValue(19, $paciente->getDataAtualizacao());
            $this->save->bindValue(20, $paciente->getId());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function excluir($paciente){
        try{
            $sql = "DELETE FROM paciente WHERE id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $paciente->getId());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function validarPaciente($paciente){
        try{
            $sql = "SELECT id, nome FROM paciente WHERE email = ? AND senha = ? limit 1";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $paciente->getEmail());
            $this->save->bindValue(2, $paciente->getSenha());
            $this->save->execute();
            $this->start->commit();

            return $this->save->fetch(PDO::FETCH_ASSOC);
        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }


}
 ?>
