<?php
/**
 * Created by PhpStorm.
 * User: Paulo Teixeira
 * Date: 06/05/2018
 * Time: 00:45
 */
require_once ('app/model/abstract/Pessoa.php');

class Paciente extends pessoa
{
    private $id;
    private $nome;
    private $endereco;
    private $numero;
    private $complemento;
    private $bairro;
    private $cidade;
    private $uf;
    private $cep;
    private $email;
    private $senha;
    private $peso;
    private $altura;
    private $nacionalidade;
    private $data_nascimento;
    private $sexo;
    private $nome_pai;
    private $nome_mae;
    private $observacao;
    private $contato_paciente = [];

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * @param mixed $endereco
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
    }

    /**
     * @return mixed
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param mixed $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    /**
     * @return mixed
     */
    public function getComplemento()
    {
        return $this->complemento;
    }

    /**
     * @param mixed $complemento
     */
    public function setComplemento($complemento)
    {
        $this->complemento = $complemento;
    }

    /**
     * @return mixed
     */
    public function getBairro()
    {
        return $this->bairro;
    }

    /**
     * @param mixed $bairro
     */
    public function setBairro($bairro)
    {
        $this->bairro = $bairro;
    }

    /**
     * @return mixed
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * @param mixed $cidade
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;
    }

    /**
     * @return mixed
     */
    public function getUf()
    {
        return $this->uf;
    }

    /**
     * @param mixed $uf
     */
    public function setUf($uf)
    {
        $this->uf = $uf;
    }

    /**
     * @return mixed
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * @param mixed $cep
     */
    public function setCep($cep)
    {
        $this->cep = $cep;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getSenha()
    {
        return $this->senha;
    }

    /**
     * @param mixed $senha
     */
    public function setSenha($senha)
    {
        $this->senha = $senha;
    }

    /**
     * @return mixed
     */
    public function getPeso()
    {
        return $this->peso;
    }

    /**
     * @param mixed $peso
     */
    public function setPeso($peso)
    {
        $this->peso = $peso;
    }

    /**
     * @return mixed
     */
    public function getAltura()
    {
        return $this->altura;
    }

    /**
     * @param mixed $altura
     */
    public function setAltura($altura)
    {
        $this->altura = $altura;
    }

    /**
     * @return mixed
     */
    public function getNacionalidade()
    {
        return $this->nacionalidade;
    }

    /**
     * @param mixed $nacionalidade
     */
    public function setNacionalidade($nacionalidade)
    {
        $this->nacionalidade = $nacionalidade;
    }

    /**
     * @return mixed
     */
    public function getDataNascimento()
    {
        return $this->data_nascimento;
    }

    /**
     * @param mixed $data_nascimento
     */
    public function setDataNascimento($data_nascimento)
    {
        $this->data_nascimento = $data_nascimento;
    }

    /**
     * @return mixed
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * @param mixed $sexo
     */
    public function setSexo($sexo)
    {
        $this->sexo = $sexo;
    }

    /**
     * @return mixed
     */
    public function getNomePai()
    {
        return $this->nome_pai;
    }

    /**
     * @param mixed $nome_pai
     */
    public function setNomePai($nome_pai)
    {
        $this->nome_pai = $nome_pai;
    }

    /**
     * @return mixed
     */
    public function getNomeMae()
    {
        return $this->nome_mae;
    }

    /**
     * @param mixed $nome_mae
     */
    public function setNomeMae($nome_mae)
    {
        $this->nome_mae = $nome_mae;
    }

    /**
     * @return mixed
     */
    public function getObservacao()
    {
        return $this->observacao;
    }

    /**
     * @param mixed $observacao
     */
    public function setObservacao($observacao)
    {
        $this->observacao = $observacao;
    }

    /**
     * @return mixed
     */
    public function getContatoPaciente()
    {
        return $this->contato_paciente;
    }

    /**
     * @param mixed $contato_paciente
     */
    public function setContatoPaciente($id = null, $celular, $ddi, $whatsapp, $telegram, $contatoPrimeiroGrau,
                                       $contatoSegundoGrau, $titular, $frequenciaUso,$pertenceDesde, $observacao, $dataCriacao = null, $dataAtualizacao)
    {
        $contatoPaciente =  new ContatoPaciente();
        $contatoPaciente->setId($id);
        $contatoPaciente->setCelular($celular);
        $contatoPaciente->setDdi($ddi);
        $contatoPaciente->setWhatsapp($whatsapp);
        $contatoPaciente->setTelegram($telegram);
        $contatoPaciente->setContatoPrimeiroGrau($contatoPrimeiroGrau);
        $contatoPaciente->setContatoSegundoGrau($contatoSegundoGrau);
        $contatoPaciente->setTitular($titular);
        $contatoPaciente->setFrequenciaUso(floatval($frequenciaUso));
        $contatoPaciente->setPertenceDesde($pertenceDesde);
        $contatoPaciente->setObservacao($observacao);
        $contatoPaciente->setDataCriacao($dataCriacao);
        $contatoPaciente->setDataAtualizacao($dataAtualizacao);

        $this->contato_paciente[] = $contatoPaciente;
    }




}