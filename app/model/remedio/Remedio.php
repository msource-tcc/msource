<?php
/**
 * Created by PhpStorm.
 * User: Paulo Teixeira
 * Date: 06/05/2018
 * Time: 00:45
 */

class Remedio
{
    private $id;
    private $nome_popular;
    private $nome_cientifico;
    private $quilograma;
    private $calorias;
    private $cor_natural;
    private $data_descoberta;
    private $fins_medicinais;
    private $data_criacao;
    private $data_atualizacao;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNomePopular()
    {
        return $this->nome_popular;
    }

    /**
     * @param mixed $nome_popular
     */
    public function setNomePopular($nome_popular)
    {
        $this->nome_popular = $nome_popular;
    }

    /**
     * @return mixed
     */
    public function getNomeCientifico()
    {
        return $this->nome_cientifico;
    }

    /**
     * @param mixed $nome_cientifico
     */
    public function setNomeCientifico($nome_cientifico)
    {
        $this->nome_cientifico = $nome_cientifico;
    }

    /**
     * @return mixed
     */
    public function getQuilograma()
    {
        return $this->quilograma;
    }

    /**
     * @param mixed $quilograma
     */
    public function setQuilograma($quilograma)
    {
        $this->quilograma = $quilograma;
    }

    /**
     * @return mixed
     */
    public function getCalorias()
    {
        return $this->calorias;
    }

    /**
     * @param mixed $calorias
     */
    public function setCalorias($calorias)
    {
        $this->calorias = $calorias;
    }

    /**
     * @return mixed
     */
    public function getCorNatural()
    {
        return $this->cor_natural;
    }

    /**
     * @param mixed $cor_natural
     */
    public function setCorNatural($cor_natural)
    {
        $this->cor_natural = $cor_natural;
    }

    /**
     * @return mixed
     */
    public function getDataDescoberta()
    {
        return $this->data_descoberta;
    }

    /**
     * @param mixed $data_descoberta
     */
    public function setDataDescoberta($data_descoberta)
    {
        $this->data_descoberta = $data_descoberta;
    }

    /**
     * @return mixed
     */
    public function getFinsMedicinais()
    {
        return $this->fins_medicinais;
    }

    /**
     * @param mixed $fins_medicinais
     */
    public function setFinsMedicinais($fins_medicinais)
    {
        $this->fins_medicinais = $fins_medicinais;
    }

    /**
     * @return mixed
     */
    public function getDataCriacao()
    {
        return $this->data_criacao;
    }

    /**
     * @param mixed $data_criacao
     */
    public function setDataCriacao($data_criacao)
    {
        $this->data_criacao = $data_criacao;
    }

    /**
     * @return mixed
     */
    public function getDataAtualizacao()
    {
        return $this->data_atualizacao;
    }

    /**
     * @param mixed $data_atualizacao
     */
    public function setDataAtualizacao($data_atualizacao)
    {
        $this->data_atualizacao = $data_atualizacao;
    }




}