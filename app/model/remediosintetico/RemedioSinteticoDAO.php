<?php

class RemedioSinteticoDAO extends EntityModel{

    public $remedio = array();

    public function listar(){
        try{
            $sql = "SELECT * FROM remedio_sintetico";

            $this->save = $this->start->prepare($sql);
            $this->save->execute();
            $arrayObj = $this->save->fetchAll(PDO::FETCH_OBJ);

            if(!empty($arrayObj)){
                foreach ($arrayObj as $obj){

                    $remedioSintetico = new RemedioSintetico();
                    $remedioSintetico->setId($obj->id);
                    $remedioSintetico->setNomePopular($obj->nome_popular);
                    $remedioSintetico->setNomeCientifico($obj->nome_cientifico);
                    $remedioSintetico->setQuilograma($obj->quilograma);
                    $remedioSintetico->setCalorias($obj->calorias);
                    $remedioSintetico->setCorNatural($obj->cor_natural);
                    $remedioSintetico->setDataDescoberta($this->dataBrasil($obj->data_descoberta));
                    $remedioSintetico->setExcrecao($obj->excrecao);
                    $remedioSintetico->setAdministracao($obj->administracao);
                    $remedioSintetico->setCas($obj->cas);
                    $remedioSintetico->setMetabolismo($obj->metabolismo);
                    $remedioSintetico->setFinsMedicinais($obj->fins_medicinais);
                    $remedioSintetico->setDataCriacao($this->dataBrasil($obj->data_criacao));
                    $remedioSintetico->setDataAtualizacao($this->dataBrasil($obj->data_atualizacao));
                    $arrayremedioSintetico[] = $remedioSintetico;
                    }
                    return $arrayremedioSintetico;
                }else{
                    return [];
            }

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function inserir($remedioSintetico){
        try{
            $sql = "INSERT INTO remedio_sintetico
                    (
                    nome_popular,
                    nome_cientifico,
                    quilograma, 
                    calorias, 
                    cor_natural, 
                    data_descoberta, 
                    excrecao,
                    administracao,
                    cas,
                    metabolismo,
                    fins_medicinais,
                    data_criacao, 
                    data_atualizacao
                    )
                    VALUE (?,?,?,?,?,?,?,?,?,?,?,?,?)";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $remedioSintetico->getNomePopular());
            $this->save->bindValue(2, $remedioSintetico->getNomeCientifico());
            $this->save->bindValue(3, $remedioSintetico->getQuilograma());
            $this->save->bindValue(4, $remedioSintetico->getCalorias());
            $this->save->bindValue(5, $remedioSintetico->getCorNatural());
            $this->save->bindValue(6, $remedioSintetico->getDataDescoberta());
            $this->save->bindValue(7, $remedioSintetico->getExcrecao());
            $this->save->bindValue(8, $remedioSintetico->getAdministracao());
            $this->save->bindValue(9, $remedioSintetico->getCas());
            $this->save->bindValue(10, $remedioSintetico->getMetabolismo());
            $this->save->bindValue(11, $remedioSintetico->getFinsMedicinais());
            $this->save->bindValue(12, $remedioSintetico->getDataCriacao());
            $this->save->bindValue(13, $remedioSintetico->getDataAtualizacao());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function visualizar($remedioSintetico){
        try{

            $sql = "SELECT * FROM remedio_sintetico WHERE id = ?";

            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $remedioSintetico->getId());
            $this->save->execute();

            $obj = $this->save->fetch(PDO::FETCH_OBJ);

            if($obj){
                $remedioSintetico->setId($obj->id);
                $remedioSintetico->setNomePopular($obj->nome_popular);
                $remedioSintetico->setNomeCientifico($obj->nome_cientifico);
                $remedioSintetico->setQuilograma($obj->quilograma);
                $remedioSintetico->setCalorias($obj->calorias);
                $remedioSintetico->setCorNatural($obj->cor_natural);
                $remedioSintetico->setDataDescoberta($this->dataBrasil($obj->data_descoberta));
                $remedioSintetico->setExcrecao($obj->excrecao);
                $remedioSintetico->setAdministracao($obj->administracao);
                $remedioSintetico->setCas($obj->cas);
                $remedioSintetico->setMetabolismo($obj->metabolismo);
                $remedioSintetico->setFinsMedicinais($obj->fins_medicinais);
                $remedioSintetico->setDataCriacao($this->dataBrasil($obj->data_criacao));
                $remedioSintetico->setDataAtualizacao($this->dataBrasil($obj->data_atualizacao));
            }

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function atualizar($remedioSintetico){
        try{
            $sql = "UPDATE remedio_sintetico 
                    SET 
                    nome_popular = ?,
                    nome_cientifico = ?,
                    quilograma = ?, 
                    calorias = ?, 
                    cor_natural = ?, 
                    data_descoberta = ?, 
                    excrecao = ?,
                    administracao = ?,
                    cas = ?,
                    metabolismo = ?,
                    fins_medicinais = ?, 
                    data_atualizacao = ?
                    WHERE id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1,$remedioSintetico->getNomePopular());
            $this->save->bindValue(2,$remedioSintetico->getNomeCientifico());
            $this->save->bindValue(3,$remedioSintetico->getQuilograma());
            $this->save->bindValue(4,$remedioSintetico->getCalorias());
            $this->save->bindValue(5,$remedioSintetico->getCorNatural());
            $this->save->bindValue(6,$remedioSintetico->getDataDescoberta());
            $this->save->bindValue(7, $remedioSintetico->getExcrecao());
            $this->save->bindValue(8, $remedioSintetico->getAdministracao());
            $this->save->bindValue(9, $remedioSintetico->getCas());
            $this->save->bindValue(10, $remedioSintetico->getMetabolismo());
            $this->save->bindValue(11,$remedioSintetico->getFinsMedicinais());
            $this->save->bindValue(12,$remedioSintetico->getDataAtualizacao());
            $this->save->bindValue(13,$remedioSintetico->getId());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }


    public function excluir($remedioSintetico){
        try{
            $sql = "DELETE FROM remedio_sintetico WHERE id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $remedioSintetico->getId());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }


    public function excluirAssocFornecedor($remedioSintetico){
        try{
            $sql = "DELETE FROM fornecedor_has_remedio_sintetico WHERE remedio_id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $remedioSintetico->getId());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function excluirAssocDoenca($remedioSintetico){
        try{
            $sql = "DELETE FROM doenca_has_remedio_sintetico WHERE remedio_id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $remedioSintetico->getId());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }



    public function validarMedico($medico){
        try{
            $sql = "SELECT id, nome FROM medico WHERE email = ? AND senha = ? limit 1";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);

            $this->save->bindValue(1, $medico->getEmail());
            $this->save->bindValue(2, $medico->getSenha());

            $this->save->execute();
            $this->start->commit();
            return $this->save->fetch(PDO::FETCH_ASSOC);
        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }


    public function listarAssocDoenca($remedioSintetico){
        try{

            $sql = "SELECT doenca.* FROM doenca_has_remedio_sintetico 
                    INNER JOIN doenca on doenca.id = doenca_has_remedio_sintetico.doenca_id
                    WHERE doenca_has_remedio_sintetico.remedio_id = ?";

            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $remedioSintetico->getId());
            $this->save->execute();
            $arrayObj = $this->save->fetchAll(PDO::FETCH_OBJ);

            if($arrayObj){
                foreach ($arrayObj as $obj) {
                    $doenca = new Doenca();
                    $doenca->setId($obj->id);
                    $doenca->setNomePopular($obj->nome_popular);
                    $doenca->setNomeCientifico($obj->nome_cientifico);
                    $doenca->setSintomas($obj->sintomas);
                    $doenca->setPercentualRisco($obj->percentual_risco);
                    $doenca->setEspecialidade($obj->especialidade);
                    $doenca->setMetodoDiagnostico($obj->metodo_diagnostico);
                    $doenca->setPeriodo($obj->periodo);
                    $doenca->setDiasDuracao($obj->dias_duracao);
                    $doenca->setInicio($obj->inicio);
                    $doenca->setDataDescoberta($this->dataBrasil($obj->data_descoberta));
                    $doenca->setObservacao($obj->observacao);
                    $doenca->setDataCriacao($this->dataBrasil($obj->data_criacao));
                    $doenca->setDataAtualizacao($this->dataBrasil($obj->data_atualizacao));

                    $remedioSintetico->setDoenca($doenca);
                }
            }

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }



    public function listarAssocFornecedor($remedioSintetico){
        try{

            $sql = "SELECT fornecedor.* FROM fornecedor_has_remedio_sintetico 
                    INNER JOIN fornecedor on fornecedor.id = fornecedor_has_remedio_sintetico.fornecedor_id
                    WHERE fornecedor_has_remedio_sintetico.remedio_id = ?";

            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $remedioSintetico->getId());
            $this->save->execute();
            $arrayObj = $this->save->fetchAll(PDO::FETCH_OBJ);

            if($arrayObj){
                foreach ($arrayObj as $obj) {
                    $fornecedor = new Fornecedor();
                    $fornecedor->setNome($obj->nome);
                    $fornecedor->setEndereco($obj->endereco);
                    $fornecedor->setNumero($obj->numero);
                    $fornecedor->setComplemento($obj->complemento);
                    $fornecedor->setBairro($obj->bairro);
                    $fornecedor->setCidade($obj->cidade);
                    $fornecedor->setUf($obj->uf);
                    $fornecedor->setCep($obj->cep);
                    $fornecedor->setEmail($obj->email);
                    $fornecedor->setSenha($obj->senha);
                    $fornecedor->setDataCriacao($this->dataBrasil($obj->data_criacao));
                    $fornecedor->setDataAtualizacao($this->dataBrasil($obj->data_atualizacao));
                    $fornecedor->setNomeFantasia($obj->nome_fantasia);
                    $fornecedor->setPercentualRamo($obj->percentual_ramo);
                    $fornecedor->setObservacao($obj->observacao);

                    $remedioSintetico->setFornecedor($fornecedor);
                }
            }

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function listarMedicoEspecialista($remedioSintetico){
        try{

            $sql = "SELECT * FROM medico WHERE esp_remedio_sint > 0.0";

            $this->save = $this->start->prepare($sql);
            $this->save->execute();
            $arrayObj = $this->save->fetchAll(PDO::FETCH_OBJ);

            if($arrayObj){
                foreach ($arrayObj as $obj) {
                    $medico = new Medico();
                    $medico->setId($obj->id);
                    $medico->setNome($obj->nome);
                    $medico->setEndereco($obj->endereco);
                    $medico->setNumero($obj->numero);
                    $medico->setComplemento($obj->complemento);
                    $medico->setBairro($obj->bairro);
                    $medico->setCidade($obj->cidade);
                    $medico->setUf($obj->uf);
                    $medico->setCep($obj->cep);
                    $medico->setEmail($obj->email);
                    $medico->setSenha($obj->senha);
                    $medico->setDataCriacao($this->dataBrasil($obj->data_criacao));
                    $medico->setDataAtualizacao($this->dataBrasil($obj->data_atualizacao));
                    $medico->setSexo($obj->sexo);
                    $medico->setDataNascimento($this->dataBrasil($obj->data_nascimento));
                    $medico->setNacionalidade($obj->nacionalidade);
                    $medico->setCrm($obj->crm);
                    $medico->setEspRemedioNat($obj->esp_remedio_nat);
                    $medico->setEspRemedioSint($obj->esp_remedio_sint);
                    $medico->setObservacao($obj->observacao);
                    $remedioSintetico->setMedico($medico);
                }
            }

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }
}
 ?>
