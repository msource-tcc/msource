<?php
/**
 * Created by PhpStorm.
 * User: Paulo Teixeira
 * Date: 06/05/2018
 * Time: 00:45
 */


require_once ('app/model/remedio/Remedio.php');

class RemedioSintetico extends Remedio
{
    private $excrecao;
    private $administracao;
    private $cas;
    private $metabolismo;
    private $doenca;
    private $fornecedor;
    private $medico;

    /**
     * @return mixed
     */
    public function getExcrecao()
    {
        return $this->excrecao;
    }

    /**
     * @param mixed $excrecao
     */
    public function setExcrecao($excrecao)
    {
        $this->excrecao = $excrecao;
    }

    /**
     * @return mixed
     */
    public function getAdministracao()
    {
        return $this->administracao;
    }

    /**
     * @param mixed $administracao
     */
    public function setAdministracao($administracao)
    {
        $this->administracao = $administracao;
    }

    /**
     * @return mixed
     */
    public function getCas()
    {
        return $this->cas;
    }

    /**
     * @param mixed $cas
     */
    public function setCas($cas)
    {
        $this->cas = $cas;
    }

    /**
     * @return mixed
     */
    public function getMetabolismo()
    {
        return $this->metabolismo;
    }

    /**
     * @param mixed $metabolismo
     */
    public function setMetabolismo($metabolismo)
    {
        $this->metabolismo = $metabolismo;
    }

    /**
     * @return mixed
     */
    public function getDoenca()
    {
        return $this->doenca;
    }

    /**
     * @param mixed $doenca
     */
    public function setDoenca(Doenca $doenca)
    {
        $this->doenca[] = $doenca;
    }

    /**
     * @return mixed
     */
    public function getFornecedor()
    {
        return $this->fornecedor;
    }

    /**
     * @param mixed $fornecedor
     */
    public function setFornecedor($fornecedor)
    {
        $this->fornecedor[] = $fornecedor;
    }

    /**
     * @return mixed
     */
    public function getMedico()
    {
        return $this->medico;
    }

    /**
     * @param mixed $medico
     */
    public function setMedico(Medico $medico)
    {
        $this->medico[] = $medico;
    }

}