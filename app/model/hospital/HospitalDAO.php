<?php

class HospitalDAO extends EntityModel{


    public function listar(){
        try{

            $sql = "SELECT * FROM hospital";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->execute();

            $objArray = $this->save->fetchAll(PDO::FETCH_OBJ);

            if(!empty($objArray)){
                foreach ($objArray as $obj) {

                    /** instancia **/
                    $hospital = new Hospital();
                    $hospital->setId($obj->id);
                    $hospital->setNome($obj->nome);
                    $hospital->setEndereco($obj->endereco);
                    $hospital->setNumero($obj->numero);
                    $hospital->setComplemento($obj->complemento);
                    $hospital->setBairro($obj->bairro);
                    $hospital->setCidade($obj->cidade);
                    $hospital->setUf($obj->uf);
                    $hospital->setCep($obj->cep);
                    $hospital->setDesde($this->dataBrasil($obj->desde));
                    $hospital->setObservacao($obj->observacao);
                    $hospital->setAvaliacao($obj->avaliacao);
                    $hospital->setDataCriacao($this->dataBrasil($obj->data_criacao));
                    $hospital->setDataAtualizacao($this->dataBrasil($obj->data_atualizacao));

                    $hospitalArray[] = $hospital;
                }
                return $hospitalArray;
            }
            return $objArray;

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }


    public function inserir($hospital){
        try{

            $sql = "INSERT INTO hospital 
                    (
                    nome,
                    endereco,
                    numero,
                    complemento,
                    bairro,
                    cidade,
                    uf,
                    cep,
                    desde,
                    observacao,
                    avaliacao,                   
                    data_criacao,
                    data_atualizacao)
                    VALUE (?,?,?,?,?,?,?,?,?,?,?,?,?)";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);

            $this->save->bindValue(1, $hospital->getNome());
            $this->save->bindValue(2, $hospital->getEndereco());
            $this->save->bindValue(3, $hospital->getNumero());
            $this->save->bindValue(4, $hospital->getComplemento());
            $this->save->bindValue(5, $hospital->getBairro());
            $this->save->bindValue(6, $hospital->getCidade());
            $this->save->bindValue(7, $hospital->getUf());
            $this->save->bindValue(8, $hospital->getCep());
            $this->save->bindValue(9, $hospital->getDesde());
            $this->save->bindValue(10, $hospital->getObservacao());
            $this->save->bindValue(11, $hospital->getAvaliacao());
            $this->save->bindValue(12, $hospital->getDataCriacao());
            $this->save->bindValue(13, $hospital->getDataAtualizacao());

            $this->save->execute();
            $this->start->commit();
        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }



    public function visualizar($hospital){
        try{

            $sql = "SELECT * FROM hospital WHERE id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $hospital->getId());
            $this->save->execute();
            $this->start->commit();

            $obj = $this->save->fetch(PDO::FETCH_OBJ);

            if($obj){
                $hospital->setNome($obj->nome);
                $hospital->setEndereco($obj->endereco);
                $hospital->setNumero($obj->numero);
                $hospital->setComplemento($obj->complemento);
                $hospital->setBairro($obj->bairro);
                $hospital->setCidade($obj->cidade);
                $hospital->setUf($obj->uf);
                $hospital->setCep($obj->cep);
                $hospital->setDesde($this->dataBrasil($obj->desde));
                $hospital->setObservacao($obj->observacao);
                $hospital->setAvaliacao($obj->avaliacao);
                $hospital->setDataCriacao($this->dataBrasil($obj->data_criacao));
                $hospital->setDataAtualizacao($this->dataBrasil($obj->data_atualizacao));
            }else{
                die("SEM REGISTRO");
            }

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }


    public function atualizar($hospital){
        try{
            $sql = "UPDATE hospital  
                    SET
                    nome = ?,
                    endereco = ?,
                    numero = ?,
                    complemento = ?,
                    bairro = ?,
                    cidade = ?,
                    uf = ?,
                    cep = ?,
                    desde = ?,
                    observacao = ?,
                    avaliacao = ?, 
                    data_atualizacao = ?
                    WHERE id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $hospital->getNome());
            $this->save->bindValue(2, $hospital->getEndereco());
            $this->save->bindValue(3, $hospital->getNumero());
            $this->save->bindValue(4, $hospital->getComplemento());
            $this->save->bindValue(5, $hospital->getBairro());
            $this->save->bindValue(6, $hospital->getCidade());
            $this->save->bindValue(7, $hospital->getUf());
            $this->save->bindValue(8, $hospital->getCep());
            $this->save->bindValue(9, $hospital->getDesde());
            $this->save->bindValue(10, $hospital->getObservacao());
            $this->save->bindValue(11, $hospital->getAvaliacao());
            $this->save->bindValue(12, $hospital->getDataAtualizacao());
            $this->save->bindValue(13, $hospital->getId());

            $this->save->execute();
            $this->start->commit();
        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }


    public function excluir($hospital){
        try{

            $sql = "DELETE FROM hospital WHERE id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);

            $this->save->bindValue(1, $hospital->getId());

            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

}
 ?>
