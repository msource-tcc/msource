<?php

class FornecedorDAO extends EntityModel{

    public function inserir($fornecedor){
        try{
            $sql = "INSERT INTO 
                    fornecedor 
                    (
                    nome, 
                    endereco, 
                    numero, 
                    complemento, 
                    bairro,
                    cidade, 
                    uf, 
                    cep, 
                    nome_fantasia,
                    desde, 
                    percentual_ramo, 
                    observacao, 
                    email, 
                    senha, 
                    data_criacao, 
                    data_atualizacao
                    )
                    VALUE (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $fornecedor->getNome());
            $this->save->bindValue(2, $fornecedor->getEndereco());
            $this->save->bindValue(3, $fornecedor->getNumero());
            $this->save->bindValue(4, $fornecedor->getComplemento());
            $this->save->bindValue(5, $fornecedor->getBairro());
            $this->save->bindValue(6, $fornecedor->getCidade());
            $this->save->bindValue(7, $fornecedor->getUf());
            $this->save->bindValue(8, $fornecedor->getCep());
            $this->save->bindValue(9, $fornecedor->getNomeFantasia());
            $this->save->bindValue(10, $fornecedor->getDesde());
            $this->save->bindValue(11, $fornecedor->getPercentualRamo());
            $this->save->bindValue(12, $fornecedor->getObservacao());
            $this->save->bindValue(13, $fornecedor->getEmail());
            $this->save->bindValue(14, $fornecedor->getSenha());
            $this->save->bindValue(15, $fornecedor->getDataCriacao());
            $this->save->bindValue(16, $fornecedor->getDataAtualizacao());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }



    public function visualizar($fornecedor){
        try{

            $sql = "SELECT * FROM fornecedor WHERE id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);

            $this->save->bindValue(1, $fornecedor->getId());

            $this->save->execute();
            $this->start->commit();

            $obj = $this->save->fetch(PDO::FETCH_OBJ);

            if($obj){
                $fornecedor->setNome($obj->nome);
                $fornecedor->setEndereco($obj->endereco);
                $fornecedor->setNumero($obj->numero);
                $fornecedor->setComplemento($obj->complemento);
                $fornecedor->setBairro($obj->bairro);
                $fornecedor->setCidade($obj->cidade);
                $fornecedor->setUf($obj->uf);
                $fornecedor->setCep($obj->cep);
                $fornecedor->setEmail($obj->email);
                $fornecedor->setSenha($obj->senha);
                $fornecedor->setDataCriacao($this->dataBrasil($obj->data_criacao));
                $fornecedor->setDataAtualizacao($this->dataBrasil($obj->data_atualizacao));

                $fornecedor->setNomeFantasia($obj->nome_fantasia);
                $fornecedor->setDesde($this->dataBrasil($obj->desde));
                $fornecedor->setPercentualRamo($obj->percentual_ramo);
                $fornecedor->setObservacao($obj->observacao);
            }

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }


    public function atualizar($fornecedor){
        try{
            $sql = "UPDATE fornecedor 
                    SET 
                    nome = ?, 
                    endereco = ?, 
                    numero = ?, 
                    complemento = ?, 
                    bairro = ?,
                    cidade = ?, 
                    uf = ?, 
                    cep = ?, 
                    nome_fantasia = ?,
                    desde = ?, 
                    percentual_ramo = ?, 
                    observacao = ?, 
                    email = ?, 
                    senha = ?,
                    data_atualizacao = ?
                    WHERE id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $fornecedor->getNome());
            $this->save->bindValue(2, $fornecedor->getEndereco());
            $this->save->bindValue(3, $fornecedor->getNumero());
            $this->save->bindValue(4, $fornecedor->getComplemento());
            $this->save->bindValue(5, $fornecedor->getBairro());
            $this->save->bindValue(6, $fornecedor->getCidade());
            $this->save->bindValue(7, $fornecedor->getUf());
            $this->save->bindValue(8, $fornecedor->getCep());
            $this->save->bindValue(9, $fornecedor->getNomeFantasia());
            $this->save->bindValue(10, $fornecedor->getDesde());
            $this->save->bindValue(11, $fornecedor->getPercentualRamo());
            $this->save->bindValue(12, $fornecedor->getObservacao());
            $this->save->bindValue(13, $fornecedor->getEmail());
            $this->save->bindValue(14, $fornecedor->getSenha());
            $this->save->bindValue(15, $fornecedor->getDataAtualizacao());
            $this->save->bindValue(16, $fornecedor->getId());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }


    public function excluir($fornecedor){
        try{

            $sql = "DELETE FROM fornecedor WHERE id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $fornecedor->getId());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }


    public function excluirARN($fornecedor){
        try{
            $sql = "DELETE FROM fornecedor_has_remedio_natural WHERE fornecedor_id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $fornecedor->getId());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function excluirARS($fornecedor){
        try{
            $sql = "DELETE FROM fornecedor_has_remedio_sintetico WHERE fornecedor_id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $fornecedor->getId());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function validarFornecedor($fornecedor){
        try{
            $sql = "SELECT id, nome FROM fornecedor WHERE email = ? AND senha = ? limit 1";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $fornecedor->getEmail());
            $this->save->bindValue(2, $fornecedor->getSenha());
            $this->save->execute();
            $this->start->commit();

            return $this->save->fetch(PDO::FETCH_ASSOC);
        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }








    public function listarARN($fornecedor){
        try{

            $sql = "SELECT remedio_natural.* FROM fornecedor_has_remedio_natural 
                    INNER JOIN remedio_natural on remedio_natural.id = fornecedor_has_remedio_natural.remedio_id
                    WHERE fornecedor_has_remedio_natural.fornecedor_id = ?";

            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $fornecedor->getId());
            $this->save->execute();
            $arrayObj = $this->save->fetchAll(PDO::FETCH_OBJ);

            if($arrayObj){
                foreach ($arrayObj as $obj) {
                    $remedioNatural = new RemedioNatural();
                    $remedioNatural->setId($obj->id);
                    $remedioNatural->setNomePopular($obj->nome_popular);
                    $remedioNatural->setNomeCientifico($obj->nome_cientifico);
                    $remedioNatural->setClasse($obj->classe);
                    $remedioNatural->setFamilia($obj->familia);
                    $remedioNatural->setGenero($obj->genero);
                    $remedioNatural->setReino($obj->reino);
                    $remedioNatural->setQuilograma($obj->quilograma);
                    $remedioNatural->setCalorias($obj->calorias);
                    $remedioNatural->setCorNatural($obj->cor_natural);
                    $remedioNatural->setDataDescoberta($this->dataBrasil($obj->data_descoberta));
                    $remedioNatural->setFinsMedicinais($obj->fins_medicinais);
                    $remedioNatural->setDataCriacao($this->dataBrasil($obj->data_criacao));
                    $remedioNatural->setDataAtualizacao($this->dataBrasil($obj->data_atualizacao));
                    $fornecedor->setRemedioNatural($remedioNatural);
                }
            }

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function listarARS($fornecedor){
        try{

            $sql = "SELECT remedio_sintetico.* FROM fornecedor_has_remedio_sintetico 
                    INNER JOIN remedio_sintetico on remedio_sintetico.id = fornecedor_has_remedio_sintetico.remedio_id
                    WHERE fornecedor_has_remedio_sintetico.fornecedor_id = ?";

            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $fornecedor->getId());
            $this->save->execute();
            $arrayObj = $this->save->fetchAll(PDO::FETCH_OBJ);

            if($arrayObj){
                foreach ($arrayObj as $obj) {
                    $remedioSintetico = new RemedioSintetico();
                    $remedioSintetico->setId($obj->id);
                    $remedioSintetico->setNomePopular($obj->nome_popular);
                    $remedioSintetico->setNomeCientifico($obj->nome_cientifico);
                    $remedioSintetico->setQuilograma($obj->quilograma);
                    $remedioSintetico->setCalorias($obj->calorias);
                    $remedioSintetico->setCorNatural($obj->cor_natural);
                    $remedioSintetico->setDataDescoberta($this->dataBrasil($obj->data_descoberta));
                    $remedioSintetico->setExcrecao($obj->excrecao);
                    $remedioSintetico->setAdministracao($obj->administracao);
                    $remedioSintetico->setCas($obj->cas);
                    $remedioSintetico->setMetabolismo($obj->metabolismo);
                    $remedioSintetico->setFinsMedicinais($obj->fins_medicinais);
                    $remedioSintetico->setDataCriacao($this->dataBrasil($obj->data_criacao));
                    $remedioSintetico->setDataAtualizacao($this->dataBrasil($obj->data_atualizacao));
                    $fornecedor->setRemedioSintetico($remedioSintetico);
                }
            }

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }


    public function visualizarARN($fornecedor){
        try{

            $sql = "SELECT * FROM fornecedor_has_remedio_natural WHERE fornecedor_id = ?";

            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $fornecedor->getId());
            $this->save->execute();
            $arrayObj = $this->save->fetchAll(PDO::FETCH_OBJ);

            if($arrayObj){
                foreach ($arrayObj as $obj) {
                    $remedioNatural = new RemedioNatural();
                    $remedioNatural->setId($obj->remedio_id);
                    $fornecedor->setRemedioNatural($remedioNatural);
                }
            }

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function desassocRN($fornecedor){
        try{
            $sql = "DELETE FROM fornecedor_has_remedio_natural WHERE fornecedor_id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $fornecedor->getId());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function assocRN($fornecedor){
        try{
            $sql = "INSERT INTO fornecedor_has_remedio_natural (fornecedor_id, remedio_id) VALUE (?,?)";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            foreach ($fornecedor->getRemedioNatural() as $remedio) {
                $this->save->bindValue(1, $fornecedor->getId());
                $this->save->bindValue(2, $remedio->getId());
                $this->save->execute();
            }
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }


    public function visualizarARS($fornecedor){
        try{

            $sql = "SELECT * FROM fornecedor_has_remedio_sintetico WHERE fornecedor_id = ?";

            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $fornecedor->getId());
            $this->save->execute();
            $arrayObj = $this->save->fetchAll(PDO::FETCH_OBJ);

            if($arrayObj){
                foreach ($arrayObj as $obj) {
                    $remedioSintetico = new RemedioSintetico();
                    $remedioSintetico->setId($obj->remedio_id);
                    $fornecedor->setRemedioSintetico($remedioSintetico);
                }
            }

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function desassocRS($fornecedor){
        try{
            $sql = "DELETE FROM fornecedor_has_remedio_sintetico WHERE fornecedor_id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $fornecedor->getId());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function assocRS($fornecedor){
        try{
            $sql = "INSERT INTO fornecedor_has_remedio_sintetico (fornecedor_id, remedio_id) VALUE (?,?)";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            foreach ($fornecedor->getRemedioSintetico() as $remedio) {
                $this->save->bindValue(1, $fornecedor->getId());
                $this->save->bindValue(2, $remedio->getId());
                $this->save->execute();
            }
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }



























}
 ?>
