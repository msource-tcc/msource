<?php

class AdministradorDAO extends EntityModel{

    public function validarAdministrador($administrador){
        try{
            $sql = "SELECT id, nome FROM administrador WHERE email = ? AND senha = ? limit 1";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $administrador->getEmail());
            $this->save->bindValue(2, $administrador->getSenha());
            $this->save->execute();
            $this->start->commit();

            $administradorArray = $this->save->fetch(PDO::FETCH_ASSOC);
            if($administradorArray){
                $administrador->setId($administradorArray['id']);
                $administrador->setNome($administradorArray['nome']);
            }
        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function inserir($administrador){
        try{
            $sql = "INSERT INTO administrador 
                    (
                    nome,
                    endereco,
                    numero,
                    complemento,
                    bairro,
                    cidade,
                    uf,
                    cep,
                    peso,                   
                    nacionalidade,
                    data_nascimento,
                    sexo,                    
                    observacao,
                    email,
                    senha,
                    data_criacao,
                    data_atualizacao)
                    VALUE (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $administrador->getNome());
            $this->save->bindValue(2, $administrador->getEndereco());
            $this->save->bindValue(3, $administrador->getNumero());
            $this->save->bindValue(4, $administrador->getComplemento());
            $this->save->bindValue(5, $administrador->getBairro());
            $this->save->bindValue(6, $administrador->getCidade());
            $this->save->bindValue(7, $administrador->getUf());
            $this->save->bindValue(8, $administrador->getCep());
            $this->save->bindValue(9, $administrador->getPeso());
            $this->save->bindValue(10, $administrador->getNacionalidade());
            $this->save->bindValue(11, $administrador->getDataNascimento());
            $this->save->bindValue(12, $administrador->getSexo());
            $this->save->bindValue(13, $administrador->getObservacao());
            $this->save->bindValue(14, $administrador->getEmail());
            $this->save->bindValue(15, $administrador->getSenha());
            $this->save->bindValue(16, $administrador->getDataCriacao());
            $this->save->bindValue(17, $administrador->getDataAtualizacao());
            $this->save->execute();
            $this->start->commit();
        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }



    public function visualizar($administrador){
        try{
            $sql = "SELECT * FROM administrador WHERE id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $administrador->getId());
            $this->save->execute();
            $this->start->commit();

            $obj = $this->save->fetch(PDO::FETCH_OBJ);

            if($obj){
                $administrador->setNome($obj->nome);
                $administrador->setEndereco($obj->endereco);
                $administrador->setNumero($obj->numero);
                $administrador->setComplemento($obj->complemento);
                $administrador->setBairro($obj->bairro);
                $administrador->setCidade($obj->cidade);
                $administrador->setUf($obj->uf);
                $administrador->setCep($obj->cep);
                $administrador->setEmail($obj->email);
                $administrador->setSenha($obj->senha);
                $administrador->setPeso($obj->peso);
                $administrador->setNacionalidade($obj->nacionalidade);
                $administrador->setDataNascimento($this->dataBrasil($obj->data_nascimento));
                $administrador->setSexo($obj->sexo);
                $administrador->setObservacao($obj->observacao);
                $administrador->setDataCriacao($this->dataBrasil($obj->data_criacao));
                $administrador->setDataAtualizacao($this->dataBrasil($obj->data_atualizacao));
            }else{
                die("usuario não encontrado - DAO");
            }

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }



    public function atualizar($administrador){
        try{
            $sql = "UPDATE administrador
                    SET 
                    nome = ?, 
                    endereco = ?, 
                    numero = ?, 
                    complemento = ?, 
                    bairro = ?,
                    cidade = ?, 
                    uf = ?, 
                    cep = ?, 
                    peso = ?,                 
                    nacionalidade = ?, 
                    data_nascimento = ?, 
                    sexo = ?,                   
                    observacao = ?, 
                    email = ?, 
                    senha = ?,                    
                    data_atualizacao = ?
                    WHERE id = ?";


            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $administrador->getNome());
            $this->save->bindValue(2, $administrador->getEndereco());
            $this->save->bindValue(3, $administrador->getNumero());
            $this->save->bindValue(4, $administrador->getComplemento());
            $this->save->bindValue(5, $administrador->getBairro());
            $this->save->bindValue(6, $administrador->getCidade());
            $this->save->bindValue(7, $administrador->getUf());
            $this->save->bindValue(8, $administrador->getCep());
            $this->save->bindValue(9, $administrador->getPeso());
            $this->save->bindValue(10, $administrador->getNacionalidade());
            $this->save->bindValue(11, $administrador->getDataNascimento());
            $this->save->bindValue(12, $administrador->getSexo());
            $this->save->bindValue(13, $administrador->getObservacao());
            $this->save->bindValue(14, $administrador->getEmail());
            $this->save->bindValue(15, $administrador->getSenha());
            $this->save->bindValue(16, $administrador->getDataAtualizacao());
            $this->save->bindValue(17, $administrador->getId());

            $this->save->execute();
            $this->start->commit();
        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }




    public function excluir($administrador){
        try{
            $sql = "DELETE FROM administrador WHERE id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $administrador->getId());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }


}
 ?>
