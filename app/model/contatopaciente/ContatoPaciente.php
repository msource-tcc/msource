<?php
/**
 * Created by PhpStorm.
 * User: Paulo Teixeira
 * Date: 06/05/2018
 * Time: 00:45
 */

class ContatoPaciente
{
    private $id;
    private $ddi;
    private $celular;
    private $whatsapp;
    private $telegram;
    private $contato_primeiro_grau;
    private $contato_segundo_grau;
    private $titular;
    private $frequencia_uso;
    private $pertence_desde;
    private $observacao;
    private $data_criacao;
    private $data_atualizacao;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * @param mixed $celular
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;
    }

    /**
     * @return mixed
     */
    public function getDdi()
    {
        return $this->ddi;
    }

    /**
     * @param mixed $ddi
     */
    public function setDdi($ddi)
    {
        $this->ddi = $ddi;
    }

    /**
     * @return mixed
     */
    public function getWhatsapp()
    {
        return $this->whatsapp;
    }

    /**
     * @param mixed $whatsapp
     */
    public function setWhatsapp($whatsapp)
    {
        $this->whatsapp = $whatsapp;
    }

    /**
     * @return mixed
     */
    public function getTelegram()
    {
        return $this->telegram;
    }

    /**
     * @param mixed $telegram
     */
    public function setTelegram($telegram)
    {
        $this->telegram = $telegram;
    }

    /**
     * @return mixed
     */
    public function getContatoPrimeiroGrau()
    {
        return $this->contato_primeiro_grau;
    }

    /**
     * @param mixed $contato_primeiro_grau
     */
    public function setContatoPrimeiroGrau($contato_primeiro_grau)
    {
        $this->contato_primeiro_grau = $contato_primeiro_grau;
    }

    /**
     * @return mixed
     */
    public function getContatoSegundoGrau()
    {
        return $this->contato_segundo_grau;
    }

    /**
     * @param mixed $contato_segundo_grau
     */
    public function setContatoSegundoGrau($contato_segundo_grau)
    {
        $this->contato_segundo_grau = $contato_segundo_grau;
    }

    /**
     * @return mixed
     */
    public function getTitular()
    {
        return $this->titular;
    }

    /**
     * @param mixed $titular
     */
    public function setTitular($titular)
    {
        $this->titular = $titular;
    }

    /**
     * @return mixed
     */
    public function getFrequenciaUso()
    {
        return $this->frequencia_uso;
    }

    /**
     * @param mixed $frequencia_uso
     */
    public function setFrequenciaUso($frequencia_uso)
    {
        $this->frequencia_uso = $frequencia_uso;
    }

    /**
     * @return mixed
     */
    public function getPertenceDesde()
    {
        return $this->pertence_desde;
    }

    /**
     * @param mixed $pertence_desde
     */
    public function setPertenceDesde($pertence_desde)
    {
        $this->pertence_desde = $pertence_desde;
    }

    /**
     * @return mixed
     */
    public function getObservacao()
    {
        return $this->observacao;
    }

    /**
     * @param mixed $observacao
     */
    public function setObservacao($observacao)
    {
        $this->observacao = $observacao;
    }

    /**
     * @return mixed
     */
    public function getDataCriacao()
    {
        return $this->data_criacao;
    }

    /**
     * @param mixed $data_criacao
     */
    public function setDataCriacao($data_criacao)
    {
        $this->data_criacao = $data_criacao;
    }

    /**
     * @return mixed
     */
    public function getDataAtualizacao()
    {
        return $this->data_atualizacao;
    }

    /**
     * @param mixed $data_atualiacao
     */
    public function setDataAtualizacao($data_atualizacao)
    {
        $this->data_atualizacao = $data_atualizacao;
    }


}