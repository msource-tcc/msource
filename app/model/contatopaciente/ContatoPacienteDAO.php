<?php

class ContatoPacienteDAO extends EntityModel{

    public function inserir($paciente){
        try{
            $sql = "INSERT INTO contato_paciente (paciente_id, ddi, celular, whatsapp, telegram, contato_primeiro_grau,
                    contato_segundo_grau, titular, frequencia_uso, pertence_desde, observacao, data_criacao, data_atualizacao)
                    VALUE (?,?,?,?,?,?,?,?,?,?,?,?,?)";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);

            foreach($paciente->getContatoPaciente() as $contato){
                $this->save->bindValue(1, $paciente->getId());
                $this->save->bindValue(2, $contato->getDdi());
                $this->save->bindValue(3, $contato->getCelular());
                $this->save->bindValue(4, $contato->getWhatsapp());
                $this->save->bindValue(5, $contato->getTelegram());
                $this->save->bindValue(6, $contato->getContatoPrimeiroGrau());
                $this->save->bindValue(7, $contato->getContatoSegundoGrau());
                $this->save->bindValue(8, $contato->getTitular());
                $this->save->bindValue(9, $contato->getFrequenciaUso());
                $this->save->bindValue(10, $contato->getPertenceDesde());
                $this->save->bindValue(11, $contato->getObservacao());
                $this->save->bindValue(12, $contato->getDataCriacao());
                $this->save->bindValue(13, $contato->getDataAtualizacao());
                $this->save->execute();
            }

            $this->start->commit();
        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function inserirPorContato($paciente, $contato){
        try{
            $sql = "INSERT INTO contato_paciente (paciente_id, ddi, celular, whatsapp, telegram, contato_primeiro_grau,
                    contato_segundo_grau, titular, frequencia_uso, pertence_desde, observacao, data_criacao, data_atualizacao)
                    VALUE (?,?,?,?,?,?,?,?,?,?,?,?,?)";

            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $paciente->getId());
            $this->save->bindValue(2, $contato->getDdi());
            $this->save->bindValue(3, $contato->getCelular());
            $this->save->bindValue(4, $contato->getWhatsapp());
            $this->save->bindValue(5, $contato->getTelegram());
            $this->save->bindValue(6, $contato->getContatoPrimeiroGrau());
            $this->save->bindValue(7, $contato->getContatoSegundoGrau());
            $this->save->bindValue(8, $contato->getTitular());
            $this->save->bindValue(9, $contato->getFrequenciaUso());
            $this->save->bindValue(10, $contato->getPertenceDesde());
            $this->save->bindValue(11, $contato->getObservacao());
            $this->save->bindValue(12, $contato->getDataCriacao());
            $this->save->bindValue(13, $contato->getDataAtualizacao());
            $this->save->execute();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function visualizar($paciente){
        try{
            $sql = "SELECT * FROM contato_paciente WHERE paciente_id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $paciente->getId());
            $this->save->execute();
            $this->start->commit();

            $arrayObj = $this->save->fetchAll(PDO::FETCH_OBJ);

            if($arrayObj) {
                foreach ($arrayObj as $obj) {

                    $paciente->setContatoPaciente(
                        $obj->id,
                        $obj->celular,
                        $obj->ddi,
                        $obj->whatsapp,
                        $obj->telegram,
                        $obj->contato_primeiro_grau,
                        $obj->contato_segundo_grau,
                        $obj->titular,
                        $obj->frequencia_uso,
                        $this->dataBrasil($obj->pertence_desde),
                        $obj->observacao,
                        $this->dataBrasil($obj->data_criacao),
                        $this->dataBrasil($obj->data_atualizacao)
                    );
                }
            }

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function atualizar($paciente){
        try{
            $sql = "UPDATE contato_paciente 
                    SET ddi = ?, celular = ?, whatsapp = ?, telegram  = ?, contato_primeiro_grau = ?,
                    contato_segundo_grau = ?, titular = ?, frequencia_uso = ?, pertence_desde = ?, observacao = ?, data_criacao = ?, data_atualizacao = ?
                    WHERE id = ? and paciente_id = ? ";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);

            foreach($paciente->getContatoPaciente() as $contato){
                if($contato->getId()){
                    $this->save->bindValue(1, $contato->getDdi());
                    $this->save->bindValue(2, $contato->getCelular());
                    $this->save->bindValue(3, $contato->getWhatsapp());
                    $this->save->bindValue(4, $contato->getTelegram());
                    $this->save->bindValue(5, $contato->getContatoPrimeiroGrau());
                    $this->save->bindValue(6, $contato->getContatoSegundoGrau());
                    $this->save->bindValue(7, $contato->getTitular());
                    $this->save->bindValue(8, $contato->getFrequenciaUso());
                    $this->save->bindValue(9, $contato->getPertenceDesde());
                    $this->save->bindValue(10, $contato->getObservacao());
                    $this->save->bindValue(11, $contato->getDataCriacao());
                    $this->save->bindValue(12, $contato->getDataAtualizacao());
                    $this->save->bindValue(13, $contato->getId());
                    $this->save->bindValue(14, $paciente->getId());
                    $this->save->execute();
                }else{
                    $this->inserirPorContato($paciente, $contato);
                }
            }

            $this->start->commit();
        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function excluir($paciente){
        try{
            $sql = "DELETE FROM contato_paciente WHERE paciente_id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $paciente->getId());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }
}
 ?>
