<?php
/**
 * Created by PhpStorm.
 * User: Paulo Teixeira
 * Date: 06/05/2018
 * Time: 00:50
 */

abstract class Pessoa
{
    private $data_criacao;
    private $data_atualizacao;

    // Força a classe que estende Pessoa a definir esses métodos
    abstract public function getId();
    abstract public function setId($id);
    abstract public function getNome();
    abstract public function setNome($nome);
    abstract public function getEndereco();
    abstract public function setEndereco($endereco);
    abstract public function getNumero();
    abstract public function setNumero($numero);
    abstract public function getComplemento();
    abstract public function setComplemento($complemento);
    abstract public function getBairro();
    abstract public function setBairro($bairro);
    abstract public function getCidade();
    abstract public function setCidade($cidade);
    abstract public function getUf();
    abstract public function setUf($uf);
    abstract public function getCep();
    abstract public function setCep($cep);
    abstract public function getEmail();
    abstract public function setEmail($email);
    abstract public function getSenha();
    abstract public function setSenha($senha);

    /**
     * @return mixed
     */
    public function getDataCriacao()
    {
        return $this->data_criacao;
    }

    /**
     * @param mixed $data_criacao
     */
    public function setDataCriacao($data_criacao)
    {
        $this->data_criacao = $data_criacao;
    }

    /**
     * @return mixed
     */
    public function getDataAtualizacao()
    {
        return $this->data_atualizacao;
    }

    /**
     * @param mixed $data_atualizacao
     */
    public function setDataAtualizacao($data_atualizacao)
    {
        $this->data_atualizacao = $data_atualizacao;
    }

}