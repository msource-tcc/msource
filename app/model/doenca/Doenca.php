<?php
/**
 * Created by PhpStorm.
 * User: Paulo Teixeira
 * Date: 06/05/2018
 * Time: 00:45
 */

class Doenca
{
    private $id;
    private $nome_popular;
    private $nome_cientifico;
    private $sintomas;
    private $periodo;
    private $percentual_risco;
    private $especialidade;
    private $dias_duracao;
    private $metodo_diagnostico;
    private $inicio;
    private $data_descoberta;
    private $observacao;
    private $data_criacao;
    private $data_atualizacao;
    private $remedio_natural;
    private $remedio_sintetico;
    private $medico = [];

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNomePopular()
    {
        return $this->nome_popular;
    }

    /**
     * @param mixed $nome_popular
     */
    public function setNomePopular($nome_popular)
    {
        $this->nome_popular = $nome_popular;
    }

    /**
     * @return mixed
     */
    public function getNomeCientifico()
    {
        return $this->nome_cientifico;
    }

    /**
     * @param mixed $nome_cientifico
     */
    public function setNomeCientifico($nome_cientifico)
    {
        $this->nome_cientifico = $nome_cientifico;
    }

    /**
     * @return mixed
     */
    public function getSintomas()
    {
        return $this->sintomas;
    }

    /**
     * @param mixed $sintomas
     */
    public function setSintomas($sintomas)
    {
        $this->sintomas = $sintomas;
    }

    /**
     * @return mixed
     */
    public function getPeriodo()
    {
        return $this->periodo;
    }

    /**
     * @param mixed $periodo
     */
    public function setPeriodo($periodo)
    {
        $this->periodo = $periodo;
    }

    /**
     * @return mixed
     */
    public function getPercentualRisco()
    {
        return $this->percentual_risco;
    }

    /**
     * @param mixed $percentual_risco
     */
    public function setPercentualRisco($percentual_risco)
    {
        $this->percentual_risco = $percentual_risco;
    }

    /**
     * @return mixed
     */
    public function getEspecialidade()
    {
        return $this->especialidade;
    }

    /**
     * @param mixed $especialidade
     */
    public function setEspecialidade($especialidade)
    {
        $this->especialidade = $especialidade;
    }

    /**
     * @return mixed
     */
    public function getDiasDuracao()
    {
        return $this->dias_duracao;
    }

    /**
     * @param mixed $dias_duracao
     */
    public function setDiasDuracao($dias_duracao)
    {
        $this->dias_duracao = $dias_duracao;
    }

    /**
     * @return mixed
     */
    public function getMetodoDiagnostico()
    {
        return $this->metodo_diagnostico;
    }

    /**
     * @param mixed $metodo_diagnostico
     */
    public function setMetodoDiagnostico($metodo_diagnostico)
    {
        $this->metodo_diagnostico = $metodo_diagnostico;
    }

    /**
     * @return mixed
     */
    public function getInicio()
    {
        return $this->inicio;
    }

    /**
     * @param mixed $inicio
     */
    public function setInicio($inicio)
    {
        $this->inicio = $inicio;
    }

    /**
     * @return mixed
     */
    public function getDataDescoberta()
    {
        return $this->data_descoberta;
    }

    /**
     * @param mixed $data_descoberta
     */
    public function setDataDescoberta($data_descoberta)
    {
        $this->data_descoberta = $data_descoberta;
    }

    /**
     * @return mixed
     */
    public function getObservacao()
    {
        return $this->observacao;
    }

    /**
     * @param mixed $observacao
     */
    public function setObservacao($observacao)
    {
        $this->observacao = $observacao;
    }

    /**
     * @return mixed
     */
    public function getDataCriacao()
    {
        return $this->data_criacao;
    }

    /**
     * @param mixed $data_criacao
     */
    public function setDataCriacao($data_criacao)
    {
        $this->data_criacao = $data_criacao;
    }

    /**
     * @return mixed
     */
    public function getDataAtualizacao()
    {
        return $this->data_atualizacao;
    }

    /**
     * @param mixed $data_atualizacao
     */
    public function setDataAtualizacao($data_atualizacao)
    {
        $this->data_atualizacao = $data_atualizacao;
    }

    /**
     * @return mixed
     */
    public function getRemedioNatural()
    {
        return $this->remedio_natural;
    }

    /**
     * @param mixed $remedio_natural
     */
    public function setRemedioNatural(RemedioNatural $remedio_natural)
    {
        $this->remedio_natural[] = $remedio_natural;
    }

    /**
     * @return mixed
     */
    public function getRemedioSintetico()
    {
        return $this->remedio_sintetico;
    }

    /**
     * @param mixed $remedio_sintetico
     */
    public function setRemedioSintetico(RemedioSintetico $remedio_sintetico)
    {
        $this->remedio_sintetico[] = $remedio_sintetico;
    }

    /**
     * @return mixed
     */
    public function getMedico()
    {
        return $this->medico;
    }

    /**
     * @param mixed $medico
     */
    public function setMedico(Medico $medico)
    {
        $this->medico[] = $medico;
    }


}