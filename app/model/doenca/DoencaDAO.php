<?php

class DoencaDAO extends EntityModel{

    public function listar(){
        try{
            $sql = "SELECT * FROM doenca";

            $this->save = $this->start->prepare($sql);
            $this->save->execute();
            
            $arrayObj = $this->save->fetchAll(PDO::FETCH_OBJ);

            if(!empty($arrayObj)){
                foreach ($arrayObj as $obj){

                    $doenca = new Doenca();
                    $doenca->setId($obj->id);
                    $doenca->setNomePopular($obj->nome_popular);
                    $doenca->setNomeCientifico($obj->nome_cientifico);
                    $doenca->setSintomas($obj->sintomas);
                    $doenca->setPercentualRisco($obj->percentual_risco);
                    $doenca->setEspecialidade($obj->especialidade);
                    $doenca->setMetodoDiagnostico($obj->metodo_diagnostico);
                    $doenca->setPeriodo($obj->periodo);
                    $doenca->setDiasDuracao($obj->dias_duracao);
                    $doenca->setInicio($obj->inicio);
                    $doenca->setDataDescoberta($this->dataBrasil($obj->data_descoberta));
                    $doenca->setObservacao($obj->observacao);
                    $doenca->setDataCriacao($this->dataBrasil($obj->data_criacao));
                    $doenca->setDataAtualizacao($this->dataBrasil($obj->data_atualizacao));

                    $arrayObjDoencas[] = $doenca;
                    }
                    return $arrayObjDoencas;
                }else{
                return [];
            }

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }


    public function inserir($doenca){
        try{
            $sql = "INSERT INTO doenca 
                    (
                    nome_popular, 
                    nome_cientifico, 
                    sintomas, 
                    percentual_risco, 
                    especialidade,
                    metodo_diagnostico, 
                    periodo, 
                    dias_duracao, 
                    inicio, 
                    data_descoberta, 
                    observacao,
                    data_criacao, 
                    data_atualizacao
                    )
                    VALUE (?,?,?,?,?,?,?,?,?,?,?,?,?)";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $doenca->getNomePopular());
            $this->save->bindValue(2, $doenca->getNomeCientifico());
            $this->save->bindValue(3, $doenca->getSintomas());
            $this->save->bindValue(4, $doenca->getPercentualRisco());
            $this->save->bindValue(5, $doenca->getEspecialidade());
            $this->save->bindValue(6, $doenca->getMetodoDiagnostico());
            $this->save->bindValue(7, $doenca->getPeriodo());
            $this->save->bindValue(8, $doenca->getDiasDuracao());
            $this->save->bindValue(9, $doenca->getInicio());
            $this->save->bindValue(10, $doenca->getDataDescoberta());
            $this->save->bindValue(11, $doenca->getObservacao());
            $this->save->bindValue(12, $doenca->getDataCriacao());
            $this->save->bindValue(13, $doenca->getDataAtualizacao());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function visualizar($doenca){
        try{

            $sql = "SELECT * FROM doenca WHERE id = ?";

            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $doenca->getId());
            $this->save->execute();

            $obj = $this->save->fetch(PDO::FETCH_OBJ);

            if($obj){
                $doenca->setId($obj->id);
                $doenca->setNomePopular($obj->nome_popular);
                $doenca->setNomeCientifico($obj->nome_cientifico);
                $doenca->setSintomas($obj->sintomas);
                $doenca->setPercentualRisco($obj->percentual_risco);
                $doenca->setEspecialidade($obj->especialidade);
                $doenca->setMetodoDiagnostico($obj->metodo_diagnostico);
                $doenca->setPeriodo($obj->periodo);
                $doenca->setDiasDuracao($obj->dias_duracao);
                $doenca->setInicio($obj->inicio);
                $doenca->setDataDescoberta($this->dataBrasil($obj->data_descoberta));
                $doenca->setObservacao($obj->observacao);
                $doenca->setDataCriacao($this->dataBrasil($obj->data_criacao));
                $doenca->setDataAtualizacao($this->dataBrasil($obj->data_atualizacao));
            }

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function atualizar($doenca){
        try{
            $sql = "UPDATE doenca 
                    SET 
                    nome_popular = ?, 
                    nome_cientifico = ?, 
                    sintomas = ?, 
                    percentual_risco = ?, 
                    especialidade = ?,
                    metodo_diagnostico = ?, 
                    periodo = ?, 
                    dias_duracao = ?, 
                    inicio = ?, 
                    data_descoberta = ?, 
                    observacao = ?,
                    data_atualizacao = ?
                    WHERE id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $doenca->getNomePopular());
            $this->save->bindValue(2, $doenca->getNomeCientifico());
            $this->save->bindValue(3, $doenca->getSintomas());
            $this->save->bindValue(4, $doenca->getPercentualRisco());
            $this->save->bindValue(5, $doenca->getEspecialidade());
            $this->save->bindValue(6, $doenca->getMetodoDiagnostico());
            $this->save->bindValue(7, $doenca->getPeriodo());
            $this->save->bindValue(8, $doenca->getDiasDuracao());
            $this->save->bindValue(9, $doenca->getInicio());
            $this->save->bindValue(10, $doenca->getDataDescoberta());
            $this->save->bindValue(11, $doenca->getObservacao());
            $this->save->bindValue(12, $doenca->getDataAtualizacao());
            $this->save->bindValue(13, $doenca->getId());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }


    public function excluir($doenca){
        try{
            $sql = "DELETE FROM doenca WHERE id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $doenca->getId());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function excluirARN($doenca){
        try{
            $sql = "DELETE FROM doenca_has_remedio_natural WHERE doenca_id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $doenca->getId());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function excluirARS($doenca){
        try{
            $sql = "DELETE FROM doenca_has_remedio_sintetico WHERE doenca_id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $doenca->getId());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }


    public function validarMedico($medico){
        try{die;
            $sql = "SELECT id, nome FROM medico WHERE email = ? AND senha = ? limit 1";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);

            $this->save->bindValue(1, $medico->getEmail());
            $this->save->bindValue(2, $medico->getSenha());

            $this->save->execute();
            $this->start->commit();
            return $this->save->fetch(PDO::FETCH_ASSOC);
        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function listarARN($doenca){
        try{

            $sql = "SELECT remedio_natural.* FROM doenca_has_remedio_natural 
                    INNER JOIN remedio_natural on remedio_natural.id = doenca_has_remedio_natural.remedio_id
                    WHERE doenca_has_remedio_natural.doenca_id = ?";

            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $doenca->getId());
            $this->save->execute();
            $arrayObj = $this->save->fetchAll(PDO::FETCH_OBJ);

            if($arrayObj){
                foreach ($arrayObj as $obj) {
                    $remedioNatural = new RemedioNatural();
                    $remedioNatural->setId($obj->id);
                    $remedioNatural->setNomePopular($obj->nome_popular);
                    $remedioNatural->setNomeCientifico($obj->nome_cientifico);
                    $remedioNatural->setClasse($obj->classe);
                    $remedioNatural->setFamilia($obj->familia);
                    $remedioNatural->setGenero($obj->genero);
                    $remedioNatural->setReino($obj->reino);
                    $remedioNatural->setQuilograma($obj->quilograma);
                    $remedioNatural->setCalorias($obj->calorias);
                    $remedioNatural->setCorNatural($obj->cor_natural);
                    $remedioNatural->setDataDescoberta($this->dataBrasil($obj->data_descoberta));
                    $remedioNatural->setFinsMedicinais($obj->fins_medicinais);
                    $remedioNatural->setDataCriacao($this->dataBrasil($obj->data_criacao));
                    $remedioNatural->setDataAtualizacao($this->dataBrasil($obj->data_atualizacao));
                    $doenca->setRemedioNatural($remedioNatural);
                }
            }

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function listarARS($doenca){
        try{

            $sql = "SELECT remedio_sintetico.* FROM doenca_has_remedio_sintetico 
                    INNER JOIN remedio_sintetico on remedio_sintetico.id = doenca_has_remedio_sintetico.remedio_id
                    WHERE doenca_has_remedio_sintetico.doenca_id = ?";

            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $doenca->getId());
            $this->save->execute();
            $arrayObj = $this->save->fetchAll(PDO::FETCH_OBJ);

            if($arrayObj){
                foreach ($arrayObj as $obj) {
                    $remedioSintetico = new RemedioSintetico();
                    $remedioSintetico->setId($obj->id);
                    $remedioSintetico->setNomePopular($obj->nome_popular);
                    $remedioSintetico->setNomeCientifico($obj->nome_cientifico);
                    $remedioSintetico->setQuilograma($obj->quilograma);
                    $remedioSintetico->setCalorias($obj->calorias);
                    $remedioSintetico->setCorNatural($obj->cor_natural);
                    $remedioSintetico->setDataDescoberta($this->dataBrasil($obj->data_descoberta));
                    $remedioSintetico->setExcrecao($obj->excrecao);
                    $remedioSintetico->setAdministracao($obj->administracao);
                    $remedioSintetico->setCas($obj->cas);
                    $remedioSintetico->setMetabolismo($obj->metabolismo);
                    $remedioSintetico->setFinsMedicinais($obj->fins_medicinais);
                    $remedioSintetico->setDataCriacao($this->dataBrasil($obj->data_criacao));
                    $remedioSintetico->setDataAtualizacao($this->dataBrasil($obj->data_atualizacao));
                    $doenca->setRemedioSintetico($remedioSintetico);
                }
            }

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }


    public function visualizarARN($doenca){
        try{

            $sql = "SELECT * FROM doenca_has_remedio_natural WHERE doenca_id = ?";

            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $doenca->getId());
            $this->save->execute();
            $arrayObj = $this->save->fetchAll(PDO::FETCH_OBJ);

            if($arrayObj){
                foreach ($arrayObj as $obj) {
                    $remedioNatural = new RemedioNatural();
                    $remedioNatural->setId($obj->remedio_id);
                    $doenca->setRemedioNatural($remedioNatural);
                }
            }

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function desassocRN($doenca){
        try{
            $sql = "DELETE FROM doenca_has_remedio_natural WHERE doenca_id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $doenca->getId());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function assocRN($doenca){
        try{
            $sql = "INSERT INTO doenca_has_remedio_natural (doenca_id, remedio_id) VALUE (?,?)";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            foreach ($doenca->getRemedioNatural() as $remedio) {
                $this->save->bindValue(1, $doenca->getId());
                $this->save->bindValue(2, $remedio->getId());
                $this->save->execute();
            }
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }


    public function visualizarARS($doenca){
        try{

            $sql = "SELECT * FROM doenca_has_remedio_sintetico WHERE doenca_id = ?";

            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $doenca->getId());
            $this->save->execute();
            $arrayObj = $this->save->fetchAll(PDO::FETCH_OBJ);

            if($arrayObj){
                foreach ($arrayObj as $obj) {
                    $remedioSintetico = new RemedioSintetico();
                    $remedioSintetico->setId($obj->remedio_id);
                    $doenca->setRemedioSintetico($remedioSintetico);
                }
            }

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function desassocRS($doenca){
        try{
            $sql = "DELETE FROM doenca_has_remedio_sintetico WHERE doenca_id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $doenca->getId());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function assocRS($doenca){
        try{
            $sql = "INSERT INTO doenca_has_remedio_sintetico (doenca_id, remedio_id) VALUE (?,?)";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            foreach ($doenca->getRemedioSintetico() as $remedio) {
                $this->save->bindValue(1, $doenca->getId());
                $this->save->bindValue(2, $remedio->getId());
                $this->save->execute();
            }
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }






}
 ?>




