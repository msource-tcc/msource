<?php

class MedicoDAO extends EntityModel{

    public function listar(){
        try{

            $sql = "SELECT * FROM medico";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->execute();

            $objArray = $this->save->fetchAll(PDO::FETCH_OBJ);

            if(!empty($objArray)){
                foreach ($objArray as $obj) {

                    /** instancia **/
                    $medico = new Medico();
                    $medico->setId($obj->id);
                    $medico->setNome($obj->nome);
                    $medico->setEndereco($obj->endereco);
                    $medico->setNumero($obj->numero);
                    $medico->setComplemento($obj->complemento);
                    $medico->setBairro($obj->bairro);
                    $medico->setCidade($obj->cidade);
                    $medico->setUf($obj->uf);
                    $medico->setCep($obj->cep);
                    $medico->setEmail($obj->email);
                    $medico->setSenha($obj->senha);
                    $medico->setDataCriacao($this->dataBrasil($obj->data_criacao));
                    $medico->setDataAtualizacao($this->dataBrasil($obj->data_atualizacao));
                    $medico->setSexo($obj->sexo);
                    $medico->setDataNascimento($this->dataBrasil($obj->data_nascimento));
                    $medico->setNacionalidade($obj->nacionalidade);
                    $medico->setCrm($obj->crm);
                    $medico->setEspRemedioNat($obj->esp_remedio_nat);
                    $medico->setEspRemedioSint($obj->esp_remedio_sint);
                    $medico->setObservacao($obj->observacao);
                    $medicoArray[] = $medico;
                }
                return $medicoArray;
            }
            return [];

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function listarPorEspecializacaoMaior($doenca)
    {
        try{

            $sql = "SELECT * FROM medico WHERE esp_remedio_nat > 50.00 AND esp_remedio_sint > 50.00 ";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->execute();

            $objArray = $this->save->fetchAll(PDO::FETCH_OBJ);

            if(!empty($objArray)){
                foreach ($objArray as $obj) {

                    /** instancia **/
                    $medico = new Medico();
                    $medico->setId($obj->id);
                    $medico->setNome($obj->nome);
                    $medico->setEndereco($obj->endereco);
                    $medico->setNumero($obj->numero);
                    $medico->setComplemento($obj->complemento);
                    $medico->setBairro($obj->bairro);
                    $medico->setCidade($obj->cidade);
                    $medico->setUf($obj->uf);
                    $medico->setCep($obj->cep);
                    $medico->setEmail($obj->email);
                    $medico->setSenha($obj->senha);
                    $medico->setDataCriacao($this->dataBrasil($obj->data_criacao));
                    $medico->setDataAtualizacao($this->dataBrasil($obj->data_atualizacao));
                    $medico->setSexo($obj->sexo);
                    $medico->setDataNascimento($this->dataBrasil($obj->data_nascimento));
                    $medico->setNacionalidade($obj->nacionalidade);
                    $medico->setCrm($obj->crm);
                    $medico->setEspRemedioNat($obj->esp_remedio_nat);
                    $medico->setEspRemedioSint($obj->esp_remedio_sint);
                    $medico->setObservacao($obj->observacao);
                    $doenca->setMedico($medico);
                }
            }

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }


    public function inserir($medico){
        try{
            $sql = "INSERT INTO medico 
                    (
                    nome, 
                    endereco, 
                    numero, 
                    complemento, 
                    bairro,
                    cidade, 
                    uf, 
                    cep, 
                    nacionalidade, 
                    data_nascimento, 
                    sexo,
                    crm, 
                    esp_remedio_nat, 
                    esp_remedio_sint, 
                    observacao, 
                    email, 
                    senha, 
                    data_criacao, 
                    data_atualizacao
                    )
                    VALUE (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $medico->getNome());
            $this->save->bindValue(2, $medico->getEndereco());
            $this->save->bindValue(3, $medico->getNumero());
            $this->save->bindValue(4, $medico->getComplemento());
            $this->save->bindValue(5, $medico->getBairro());
            $this->save->bindValue(6, $medico->getCidade());
            $this->save->bindValue(7, $medico->getUf());
            $this->save->bindValue(8, $medico->getCep());
            $this->save->bindValue(9, $medico->getNacionalidade());
            $this->save->bindValue(10, $medico->getDataNascimento());
            $this->save->bindValue(11, $medico->getSexo());
            $this->save->bindValue(12, $medico->getCrm());
            $this->save->bindValue(13, $medico->getEspRemedioNat());
            $this->save->bindValue(14, $medico->getEspRemedioSint());
            $this->save->bindValue(15, $medico->getObservacao());
            $this->save->bindValue(16, $medico->getEmail());
            $this->save->bindValue(17, $medico->getSenha());
            $this->save->bindValue(18, $medico->getDataCriacao());
            $this->save->bindValue(19, $medico->getDataAtualizacao());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function visualizar($medico){
        try{

            $sql = "SELECT * FROM medico WHERE id = ?";

            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $medico->getId());
            $this->save->execute();

            $obj = $this->save->fetch(PDO::FETCH_OBJ);

            if($obj){
                $medico->setNome($obj->nome);
                $medico->setEndereco($obj->endereco);
                $medico->setNumero($obj->numero);
                $medico->setComplemento($obj->complemento);
                $medico->setBairro($obj->bairro);
                $medico->setCidade($obj->cidade);
                $medico->setUf($obj->uf);
                $medico->setCep($obj->cep);
                $medico->setEmail($obj->email);
                $medico->setSenha($obj->senha);
                $medico->setDataCriacao($this->dataBrasil($obj->data_criacao));
                $medico->setDataAtualizacao($this->dataBrasil($obj->data_atualizacao));
                $medico->setSexo($obj->sexo);
                $medico->setDataNascimento($this->dataBrasil($obj->data_nascimento));
                $medico->setNacionalidade($obj->nacionalidade);
                $medico->setCrm($obj->crm);
                $medico->setEspRemedioNat($obj->esp_remedio_nat);
                $medico->setEspRemedioSint($obj->esp_remedio_sint);
                $medico->setObservacao($obj->observacao);
            }

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function atualizar($medico){
        try{
            $sql = "UPDATE  medico 
                    SET 
                    nome = ?, 
                    endereco = ?, 
                    numero = ?, 
                    complemento = ?, 
                    bairro = ?,
                    cidade = ?, 
                    uf = ?, 
                    cep = ?, 
                    nacionalidade = ?, 
                    data_nascimento = ?, 
                    sexo = ?,
                    crm = ?, 
                    esp_remedio_nat = ?, 
                    esp_remedio_sint = ?, 
                    observacao = ?, 
                    email = ?, 
                    senha = ?,                     
                    data_atualizacao = ?
                    WHERE id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $medico->getNome());
            $this->save->bindValue(2, $medico->getEndereco());
            $this->save->bindValue(3, $medico->getNumero());
            $this->save->bindValue(4, $medico->getComplemento());
            $this->save->bindValue(5, $medico->getBairro());
            $this->save->bindValue(6, $medico->getCidade());
            $this->save->bindValue(7, $medico->getUf());
            $this->save->bindValue(8, $medico->getCep());
            $this->save->bindValue(9, $medico->getNacionalidade());
            $this->save->bindValue(10, $medico->getDataNascimento());
            $this->save->bindValue(11, $medico->getSexo());
            $this->save->bindValue(12, $medico->getCrm());
            $this->save->bindValue(13, $medico->getEspRemedioNat());
            $this->save->bindValue(14, $medico->getEspRemedioSint());
            $this->save->bindValue(15, $medico->getObservacao());
            $this->save->bindValue(16, $medico->getEmail());
            $this->save->bindValue(17, $medico->getSenha());
            $this->save->bindValue(18, $medico->getDataAtualizacao());
            $this->save->bindValue(19, $medico->getId());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }


    public function excluir($medico){
        try{
            $sql = "DELETE FROM medico WHERE id = ?";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);
            $this->save->bindValue(1, $medico->getId());
            $this->save->execute();
            $this->start->commit();

        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }

    public function validarMedico($medico){
        try{
            $sql = "SELECT id, nome FROM medico WHERE email = ? AND senha = ? limit 1";

            $this->start->beginTransaction();
            $this->save = $this->start->prepare($sql);

            $this->save->bindValue(1, $medico->getEmail());
            $this->save->bindValue(2, $medico->getSenha());

            $this->save->execute();
            $this->start->commit();
            return $this->save->fetch(PDO::FETCH_ASSOC);
        }catch(PDOException $ErrorPDO){
            die($ErrorPDO->getMessage());
        }
    }
}
 ?>
