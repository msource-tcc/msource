<?php
/**
 * Created by PhpStorm.
 * User: Paulo Teixeira
 * Date: 06/05/2018
 * Time: 00:45
 */
require_once ('app/model/abstract/Pessoa.php');

class Medico extends pessoa
{
    private $id;
    private $nome;
    private $endereco;
    private $numero;
    private $complemento;
    private $bairro;
    private $cidade;
    private $uf;
    private $cep;
    private $email;
    private $senha;
    private $hospital_id;
    private $sexo;
    private $data_nascimento;
    private $nacionalidade;
    private $observacao;
    private $crm;
    private $esp_remedio_nat;
    private $esp_remedio_sint;
    private $hospital;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * @param mixed $endereco
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
    }

    /**
     * @return mixed
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param mixed $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    /**
     * @return mixed
     */
    public function getComplemento()
    {
        return $this->complemento;
    }

    /**
     * @param mixed $complemento
     */
    public function setComplemento($complemento)
    {
        $this->complemento = $complemento;
    }

    /**
     * @return mixed
     */
    public function getBairro()
    {
        return $this->bairro;
    }

    /**
     * @param mixed $bairro
     */
    public function setBairro($bairro)
    {
        $this->bairro = $bairro;
    }

    /**
     * @return mixed
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * @param mixed $cidade
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;
    }

    /**
     * @return mixed
     */
    public function getUf()
    {
        return $this->uf;
    }

    /**
     * @param mixed $uf
     */
    public function setUf($uf)
    {
        $this->uf = $uf;
    }

    /**
     * @return mixed
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * @param mixed $cep
     */
    public function setCep($cep)
    {
        $this->cep = $cep;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getSenha()
    {
        return $this->senha;
    }

    /**
     * @param mixed $senha
     */
    public function setSenha($senha)
    {
        $this->senha = $senha;
    }

    /**
     * @return mixed
     */
    public function getHospitalId()
    {
        return $this->hospital_id;
    }

    /**
     * @param mixed $hospital_id
     */
    public function setHospitalId($hospital_id)
    {
        $this->hospital_id = $hospital_id;
    }

    /**
     * @return mixed
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * @param mixed $sexo
     */
    public function setSexo($sexo)
    {
        $this->sexo = $sexo;
    }

    /**
     * @return mixed
     */
    public function getDataNascimento()
    {
        return $this->data_nascimento;
    }

    /**
     * @param mixed $data_nascimento
     */
    public function setDataNascimento($data_nascimento)
    {
        $this->data_nascimento = $data_nascimento;
    }

    /**
     * @return mixed
     */
    public function getNacionalidade()
    {
        return $this->nacionalidade;
    }

    /**
     * @param mixed $nacionalidade
     */
    public function setNacionalidade($nacionalidade)
    {
        $this->nacionalidade = $nacionalidade;
    }

    /**
     * @return mixed
     */
    public function getObservacao()
    {
        return $this->observacao;
    }

    /**
     * @param mixed $observacao
     */
    public function setObservacao($observacao)
    {
        $this->observacao = $observacao;
    }

    /**
     * @return mixed
     */
    public function getCrm()
    {
        return $this->crm;
    }

    /**
     * @param mixed $crm
     */
    public function setCrm($crm)
    {
        $this->crm = $crm;
    }

    /**
     * @return mixed
     */
    public function getEspRemedioNat()
    {
        return $this->esp_remedio_nat;
    }

    /**
     * @param mixed $esp_remedio_nat
     */
    public function setEspRemedioNat($esp_remedio_nat)
    {
        $this->esp_remedio_nat = $esp_remedio_nat;
    }

    /**
     * @return mixed
     */
    public function getEspRemedioSint()
    {
        return $this->esp_remedio_sint;
    }

    /**
     * @param mixed $esp_remedio_sint
     */
    public function setEspRemedioSint($esp_remedio_sint)
    {
        $this->esp_remedio_sint = $esp_remedio_sint;
    }

    /**
     * @return mixed
     */
    public function getHospital()
    {
        return $this->hospital;
    }

    /**
     * @param mixed $hospital
     */
    public function setHospital(Hospital $hospital)
    {
        $this->hospital[] = $hospital;
    }





}