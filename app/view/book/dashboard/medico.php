<?php $this->loadDefault(SELF::HEADER); ?>
<div class="container top">
    <div class="row-opcao row d-flex justify-content-center">
        <div class="col-sm-2">
            <a href="medico/visualizar/<?php echo $this->acessoSessao->getId(); ?>" class="cx-opcao">
                <div class="bl-opcao">
                    <div class="superior d-flex justify-content-center align-items-end"><i class="fa fa-stethoscope" aria-hidden="true"></i></div>
                    <div class="inferior d-flex justify-content-center align-items-center"><span>Perfil médico</span></div>
                </div>
            </a>
        </div>

        <div class="col-sm-2">
            <a href="produto/listar" class="cx-opcao">
                <div class="bl-opcao">
                    <div class="superior d-flex justify-content-center align-items-end"><i class="fa fa-leaf" aria-hidden="true"></i></div>
                    <div class="inferior d-flex justify-content-center align-items-center"><span>Produto</span></div>
                </div>
            </a>
        </div>

        <div class="col-sm-2">
            <a href="causa/listar" class="cx-opcao">
                <div class="bl-opcao">
                    <div class="superior d-flex justify-content-center align-items-end"><i class="fa fa-heartbeat" aria-hidden="true"></i></div>
                    <div class="inferior d-flex justify-content-center align-items-center"><span>Causa</span></div>
                </div>
            </a>
        </div>
    </div>
</div>
<?php $this->loadDefault(SELF::FOOTER); ?>