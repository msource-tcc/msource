<?php $this->loadDefault(SELF::HEADER); ?>
<div class="container top">
    <div class="row-opcao row d-flex justify-content-center">
        <div class="col-sm-2">
            <a href="fornecedor/visualizar" class="cx-opcao">
                <div class="bl-opcao">
                    <div class="superior d-flex justify-content-center align-items-end"><i class="fa fa-truck" aria-hidden="true"></i></div>
                    <div class="inferior d-flex justify-content-center align-items-center"><span>Perfil</span></div>
                </div>
            </a>
        </div>
    </div>
</div>
<?php $this->loadDefault(SELF::FOOTER); ?>