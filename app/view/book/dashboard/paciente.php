<?php $this->loadDefault(SELF::HEADER); ?>
<div class="container top">
    <div class="row-opcao row d-flex justify-content-center">
        <div class="col-sm-2">
            <a href="paciente/visualizar" class="cx-opcao">
                <div class="bl-opcao">
                    <div class="superior d-flex justify-content-center align-items-end"><i class="fa fa-users" aria-hidden="true"></i></div>
                    <div class="inferior d-flex justify-content-center align-items-center"><span>Perfil</span></div>
                </div>
            </a>
        </div>

        <div class="col-sm-2">
            <a href="doenca/listar" class="cx-opcao">
                <div class="bl-opcao">
                    <div class="superior d-flex justify-content-center align-items-end"><i class="fa fa-heartbeat" aria-hidden="true"></i></div>
                    <div class="inferior d-flex justify-content-center align-items-center"><span>Doença</span></div>
                </div>
            </a>
        </div>

        <div class="col-sm-2">
            <a href="remedio-natural/listar" class="cx-opcao">
                <div class="bl-opcao">
                    <div class="superior d-flex justify-content-center align-items-end"><i class="fa fa-leaf" aria-hidden="true"></i></div>
                    <div class="inferior d-flex justify-content-center align-items-center"><span>Remédio Natural</span></div>
                </div>
            </a>
        </div>

        <div class="col-sm-2">
            <a href="remedio-sintetico/listar" class="cx-opcao">
                <div class="bl-opcao">
                    <div class="superior d-flex justify-content-center align-items-end"><i class="fa fa-flask" aria-hidden="true"></i></div>
                    <div class="inferior d-flex justify-content-center align-items-center"><span>Remédio Sintetico</span></div>
                </div>
            </a>
        </div>
    </div>
</div>
<?php $this->loadDefault(SELF::FOOTER); ?>



