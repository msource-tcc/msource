<!-- preparando formulario para pagina -->
<?php $this->loadDefault(SELF::HEADER); ?>
<div class="container top">
    <div class="row d-flex justify-content-center">
        <div class="col-sm-2">
            <a href="administrador/visualizar" class="cx-opcao">
                <div class="bl-opcao">
                    <div class="superior d-flex justify-content-center align-items-end"><i class="fa fa-user-circle-o" aria-hidden="true"></i></div>
                    <div class="inferior d-flex justify-content-center align-items-center"><span>Perfil</span></div>
                </div>
            </a>
        </div>
        <div class="col-sm-2">
            <a href="administrador/inserir" class="cx-opcao">
                <div class="bl-opcao">
                    <div class="superior d-flex justify-content-center align-items-end"><i class="fa fa-plus-circle" aria-hidden="true"></i></div>
                    <div class="inferior d-flex justify-content-center align-items-center"><span>Novo Adm</span></div>
                </div>
            </a>
        </div>
        <div class="col-sm-2">
            <a href="hospital/listar" class="cx-opcao">
                <div class="bl-opcao">
                    <div class="superior d-flex justify-content-center align-items-end"><i class="fa fa-hospital-o" aria-hidden="true"></i></div>
                    <div class="inferior d-flex justify-content-center align-items-center"><span>Hospital</span></div>
                </div>
            </a>
        </div>
        <div class="col-sm-2">
            <a href="medico/listar" class="cx-opcao">
                <div class="bl-opcao">
                    <div class="superior d-flex justify-content-center align-items-end"><i class="fa fa-stethoscope" aria-hidden="true"></i></div>
                    <div class="inferior d-flex justify-content-center align-items-center"><span>Médico</span></div>
                </div>
            </a>
        </div>
    </div>

    <div class="mtop2 row d-flex justify-content-center">
        <div class="col-sm-2">
            <a href="doenca/listar" class="cx-opcao">
                <div class="bl-opcao">
                    <div class="superior d-flex justify-content-center align-items-end"><i class="fa fa-heartbeat" aria-hidden="true"></i></div>
                    <div class="inferior d-flex justify-content-center align-items-center"><span>Doença</span></div>
                </div>
            </a>
        </div>
        <div class="col-sm-2">
            <a href="remedio-natural/listar" class="cx-opcao">
                <div class="bl-opcao">
                    <div class="superior d-flex justify-content-center align-items-end"><i class="fa fa-leaf" aria-hidden="true"></i></div>
                    <div class="inferior d-flex justify-content-center align-items-center"><span>Remédio Natural</span></div>
                </div>
            </a>
        </div>
        <div class="col-sm-2">
            <a href="remedio-sintetico/listar" class="cx-opcao">
                <div class="bl-opcao">
                    <div class="superior d-flex justify-content-center align-items-end"><i class="fa fa-flask" aria-hidden="true"></i></div>
                    <div class="inferior d-flex justify-content-center align-items-center"><span>Remédio Sintético</span></div>
                </div>
            </a>
        </div>

    </div>
</div>
<?php $this->loadDefault(SELF::FOOTER); ?>



