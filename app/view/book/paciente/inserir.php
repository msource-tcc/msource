<?php $this->loadDefault(SELF::HEADER); ?>
<div class="container top-default">
    <div class="row">
        <div class="col-sm-2 d-flex">
            <a href="login/paciente" class="btn btn-padrao mtop mbottom">Voltar</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-default">
                <div class="header d-flex flex-column justify-content-center align-items-center">
                    <i class="fa fa-users mright" aria-hidden="true"></i>
                    <span>Cadastro de paciente</span>
                </div>
                <form method="post">
                    <div class="form-group">
                        <label>Nome</label>
                        <input name="nome" type="text" class="form-control tam5" required>
                    </div>
                    <div class="form-group">
                        <label>Endereço</label>
                        <input name="endereco" type="text" class="form-control tam5" required>
                    </div>
                    <div class="form-group">
                        <label>Número</label>
                        <input name="numero" type="text" class="number form-control tam1" required>
                    </div>
                    <div class="form-group">
                        <label>Complemento</label>
                        <input name="complemento" type="text" class="form-control tam5" required>
                    </div>
                    <div class="form-group">
                        <label>Bairro</label>
                        <input name="bairro" type="text" class="form-control tam3" required>
                    </div>
                    <div class="form-group">
                        <label>Cidade</label>
                        <input name="cidade" type="text" class="form-control tam3" required>
                    </div>
                    <div class="form-group">
                        <label>UF</label>
                        <input name="uf" type="text" class="form-control tam05" maxlength="2" required>
                    </div>
                    <div class="form-group">
                        <label>CEP</label>
                        <input name="cep" type="text" class="form-control tam1 cep" required>
                    </div>

                    <div class="form-group">
                        <label>Peso</label>
                        <input name="peso" type="text" class="float form-control tam1" required>
                    </div>
                    <div class="form-group">
                        <label>Altura</label>
                        <input name="altura" type="text" class="float form-control tam1" required>
                    </div>
                    <div class="form-group">
                        <label>Nacionalidade</label>
                        <input name="nacionalidade" type="text" class="form-control tam2" required>
                    </div>
                    <div class="form-group">
                        <label>Data de Nascimento</label>
                        <input name="data_nascimento" type="text" class="date form-control tam1" required>
                    </div>
                    <div class="form-group tam1">
                        <label>Sexo</label>
                        <select name="sexo" class="form-control" required>
                            <option></option>
                            <option value="masculino">Masculino</option>
                            <option value="feminino">Feminino</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Nome do Pai</label>
                        <input name="nome_pai" type="text" class="form-control tam5" required>
                    </div>
                    <div class="form-group">
                        <label>Nome da Mãe</label>
                        <input name="nome_mae" type="text" class="form-control tam5" required>
                    </div>
                    <div class="form-group">
                        <label>Observação</label>
                        <textarea name="observacao" class="form-control"  rows="10"></textarea>
                    </div>

                    <div class="form-group">
                        <label>E-mail</label>
                        <input name="email" type="email" class="form-control tam3" required>
                    </div>
                    <div class="form-group">
                        <label>Senha</label>
                        <input name="senha" type="password" class="form-control tam1" required>
                    </div>
                    <div class="header d-flex flex-column justify-content-center align-items-center">
                        <i class="fa fa-tty mright" aria-hidden="true"></i>
                        <span>Contato do paciente</span>
                    </div>
                    <div class="form-group">
                        <div class="alert alert-primary" role="alert">
                            Contanto de no maximo 3.
                        </div>
                    </div>

                    <?php for($i=1; $i <=3; $i++){ ?>
                        <div class="bloco-contato">
                            <div class="form-group form-check">
                                <input name="contato[<?php echo $i ?>][contato-checkbox]" type="checkbox" class="form-check-input contato-checkbox" id="<?php echo $i ?>">
                                <label class="form-check-label">Contato <?php echo $i ?></label>
                            </div>
                            <div id="contato-<?php echo $i ?>" class="none">
                                <div class="form-group">
                                    <label>DDI</label>
                                    <input name="contato[<?php echo $i ?>][ddi]" type="text" class="number form-control tam05">
                                </div>
                                <div class="form-group">
                                    <label>Celular</label>
                                    <input name="contato[<?php echo $i ?>][celular]" type="text" class="form-control tam1">
                                </div>
                                <div class="form-group">
                                    <label>Whatsapp</label>
                                    <select name="contato[<?php echo $i ?>][whatsapp]" class="form-control tam1">
                                        <option></option>
                                        <option value="não">Não</option>
                                        <option value="sim">Sim</option>
                                        <option value="não informado">Não informado</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Telegram</label>
                                    <select name="contato[<?php echo $i ?>][telegram]" class="form-control tam1">
                                        <option></option>
                                        <option value="não">Não</option>
                                        <option value="sim">Sim</option>
                                        <option value="não informado">Não informado</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Contato de primeiro grau</label>
                                    <input name="contato[<?php echo $i ?>][contato_primeiro_grau]" type="text" class="form-control tam4">
                                </div>
                                <div class="form-group">
                                    <label>Contato de segundo grau</label>
                                    <input name="contato[<?php echo $i ?>][contato_segundo_grau]" type="text" class="form-control tam4">
                                </div>
                                <div class="form-group">
                                    <label>Titular Numero Celular?</label>
                                    <select name="contato[<?php echo $i ?>][titular]" class="form-control tam1">
                                        <option></option>
                                        <option value="sim">Sim</option>
                                        <option value="parentes">Parentes</option>
                                        <option value="amigos">Amigos</option>
                                        <option value="outros">Outros</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Frequencia Uso</label>
                                    <input name="contato[<?php echo $i ?>][frequencia_uso]" type="text" class="float form-control tam05">
                                </div>
                                <div class="form-group">
                                    <label>Pertence desde?</label>
                                    <input name="contato[<?php echo $i ?>][pertence_desde]" type="text" class="date form-control tam1">
                                </div>
                                <div class="form-group">
                                    <label>Observação</label>
                                    <textarea name="contato[<?php echo $i ?>][observacao]" class="form-control"  rows="10"></textarea>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <div class="form-group">
                        <button class="btn btn-padrao">Enviar</button>
                        <a href="login/usuario" class="btn btn-padrao">Voltar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->loadDefault(SELF::FOOTER); ?>



