<?php $this->loadDefault(SELF::HEADER); ?>
<div class="container">
    <div class="row">
        <div class="col-sm-12 d-flex">
            <a href="dashboard" class="btn btn-padrao mtop mbottom mright">Voltar</a>
            <a href="paciente/atualizar" class="btn btn-padrao mtop mbottom">Atualizar</a>
            <a href="paciente/excluir" class="btn btn-padrao mtop mbottom ml-auto">Excluir Conta</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="tabela-padrao">
                <table>
                    <tbody>
                        <tr><td colspan="2" class="table-header">Usuário</td></tr>
                        <tr><th>nome:</th><td><?php echo $this->paciente->getNome(); ?></td></tr>
                        <tr><th>Endereço:</th><td><?php echo $this->paciente->getEndereco(); ?></td></tr>
                        <tr><th>numero:</th><td><?php echo $this->paciente->getNumero(); ?></td></tr>
                        <tr><th>complemento:</th><td><?php echo $this->paciente->getComplemento(); ?></td></tr>
                        <tr><th>bairro:</th><td><?php echo $this->paciente->getBairro(); ?></td></tr>
                        <tr><th>cidade:</th><td><?php echo $this->paciente->getCidade(); ?></td></tr>
                        <tr><th>UF:</th><td><?php echo $this->paciente->getUf(); ?></td></tr>
                        <tr><th>CEP:</th><td><?php echo $this->paciente->getCep(); ?></td></tr>
                        <tr><th>E-mail:</th><td><?php echo $this->paciente->getEmail(); ?></td></tr>
                        <tr><th>Senha:</th><td><?php echo $this->paciente->getSenha(); ?></td></tr>
                        <tr><th>Peso:</th><td><?php echo $this->paciente->getPeso(); ?></td></tr>
                        <tr><th>Altura:</th><td><?php echo $this->paciente->getAltura(); ?></td></tr>
                        <tr><th>Nacionalidade:</th><td><?php echo $this->paciente->getNacionalidade(); ?></td></tr>
                        <tr><th>Data Nascimento:</th><td><?php echo $this->paciente->getDataNascimento(); ?></td></tr>
                        <tr><th>Sexo:</th><td><?php echo $this->paciente->getSexo(); ?></td></tr>
                        <tr><th>Nome Pai:</th><td><?php echo $this->paciente->getNomePai(); ?></td></tr>
                        <tr><th>Nome Mãe:</th><td><?php echo $this->paciente->getNomeMae(); ?></td></tr>
                        <tr><th>Observação:</th><td><?php echo $this->paciente->getObservacao(); ?></td></tr>
                        <tr><th>Data Criação:</th><td><?php echo $this->paciente->getDataCriacao(); ?></td></tr>
                        <tr><th>Data Atualização:</th><td><?php echo $this->paciente->getDataAtualizacao(); ?></td></tr>
                        <?php if(!empty($this->paciente->getContatoPaciente())){ ?>
                            <?php $count = 0; foreach ($this->paciente->getContatoPaciente() as $contato) { $count++; ?>
                                <?php if(!empty($contato->getId())){ ?>
                                    <tr><td colspan="2" class="table-header">Contato <?php echo $count ?></td></tr>
                                    <tr><th>DDI:</th><td><?php echo $contato->getDdi(); ?></td></tr>
                                    <tr><th>Celular:</th><td><?php echo $contato->getCelular(); ?></td></tr>
                                    <tr><th>Whatsapp:</th><td><?php echo $contato->getWhatsapp(); ?></td></tr>
                                    <tr><th>Telegram:</th><td><?php echo $contato->getTelegram(); ?></td></tr>
                                    <tr><th>Contato Primeiro Grau:</th><td><?php echo $contato->getContatoPrimeiroGrau(); ?></td></tr>
                                    <tr><th>Contato Segundo Grau:</th><td><?php echo $contato->getContatoSegundoGrau(); ?></td></tr>
                                    <tr><th>Titular:</th><td><?php echo $contato->getTitular(); ?></td></tr>
                                    <tr><th>Frequencia Uso:</th><td><?php echo $contato->getFrequenciaUso(); ?>%</td></tr>
                                    <tr><th>Pertence Desde:</th><td><?php echo $contato->getPertenceDesde(); ?></td></tr>
                                    <tr><th>Obervacao:</th><td><?php echo $contato->getObservacao(); ?></td></tr>
                                    <tr><th>Data Criação:</th><td><?php echo $contato->getDataCriacao(); ?></td></tr>
                                    <tr><th>Data Atualização:</th><td><?php echo $contato->getDataAtualizacao(); ?></td></tr>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php $this->loadDefault(SELF::FOOTER); ?>



