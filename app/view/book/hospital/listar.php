<?php $this->loadDefault(SELF::HEADER); ?>
<div class="container">
    <div class="row">
        <div class="col-sm-2 d-flex">
            <a href="dashboard" class="btn btn-padrao mtop mbottom mright">Voltar</a>
            <a href="hospital/inserir" class="btn btn-padrao mtop mbottom">Cadastrar</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?php if(empty($this->hospitalArray)){?>
                <div class="alert alert-warning" role="alert">
                    Nenhum registro.
                </div>
            <?php } else { ?>
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col" colspan="5"><span class="d-flex justify-content-center">Hospital</span></th>
                    </tr>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Endereço</th>
                        <th scope="col">Bairro</th>
                        <th scope="col">Cidade</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($this->hospitalArray as $hospital) { ?>
                            <tr>
                                <th scope="row"><?php echo $hospital->getId(); ?></th>
                                <td><a href="hospital/visualizar/<?php echo $hospital->getId(); ?>"><?php echo $hospital->getNome(); ?></a></td>
                                <td><?php echo $hospital->getEndereco(); ?></td>
                                <td><?php echo $hospital->getBairro(); ?></td>
                                <td><?php echo $hospital->getCidade(); ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php } ?>
        </div>
    </div>
</div>
<?php $this->loadDefault(SELF::FOOTER); ?>



