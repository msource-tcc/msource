<?php $this->loadDefault(SELF::HEADER); ?>
<div class="container">
    <div class="row mtop">
        <div class="col-sm-12">
            <div class="alert alert-success <?php echo (is_null($this->mensagem)) ? "none" : "" ?>" role="alert">
                <?php echo $this->mensagem; ?>
            </div>
        </div>
    </div>
    <div class="row mbottom">
        <div class="col-sm-2 d-flex">
            <a href="hospital/visualizar/<?php echo $this->hospital->getId(); ?>" class="btn btn-padrao">Voltar</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 ">
            <div class="form-default">
                <div class="header d-flex flex-column justify-content-center align-items-center">
                    <i class="fa fa-hospital-o mright" aria-hidden="true"></i>
                    <span>Atualização de hospital</span>
                </div>
                <form method="post">
                    <div class="form-group">
                        <label>Nome</label>
                        <input name="nome" type="text" class="form-control tam5" required value="<?php echo $this->hospital->getNome(); ?>">
                    </div>
                    <div class="form-group">
                        <label>Endereço</label>
                        <input name="endereco" type="text" class="form-control tam5" required value="<?php echo $this->hospital->getEndereco(); ?>">
                    </div>
                    <div class="form-group">
                        <label>Numero</label>
                        <input name="numero" type="text" class="form-control tam1" required value="<?php echo $this->hospital->getNumero(); ?>">
                    </div>
                    <div class="form-group">
                        <label>Complemento</label>
                        <input name="complemento" type="text" class="form-control tam5" required value="<?php echo $this->hospital->getComplemento(); ?>">
                    </div>
                    <div class="form-group">
                        <label>Bairro</label>
                        <input name="bairro" type="text" class="form-control tam5" required value="<?php echo $this->hospital->getBairro(); ?>">
                    </div>
                    <div class="form-group">
                        <label>Cidade</label>
                        <input name="cidade" type="text" class="form-control tam5" required value="<?php echo $this->hospital->getCidade(); ?>">
                    </div>
                    <div class="form-group">
                        <label>Uf</label>
                        <input name="uf" type="text" class="form-control tam05" maxlength="2" required value="<?php echo $this->hospital->getUf(); ?>">
                    </div>
                    <div class="form-group">
                        <label>CEP</label>
                        <input name="cep" type="text" class="cep form-control tam1" required value="<?php echo $this->hospital->getCep(); ?>">
                    </div>
                    <div class="form-group">
                        <label>desde</label>
                        <input name="desde" type="text" class="date form-control tam1" required value="<?php echo $this->hospital->getDesde(); ?>">
                    </div>
                    <div class="form-group">
                        <label>Avaliação</label>
                        <input name="avaliacao" type="text" class="percentual form-control tam05" required value="<?php echo $this->hospital->getAvaliacao(); ?>">
                    </div>
                    <div class="form-group">
                        <label>Observação</label>
                        <textarea name="observacao" class="form-control"  rows="10"><?php echo $this->hospital->getObservacao(); ?></textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-padrao">Salvar</button>
                        <a href="hospital/visualizar/<?php echo $this->hospital->getId(); ?>" class="btn btn-padrao">Voltar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->loadDefault(SELF::FOOTER); ?>



