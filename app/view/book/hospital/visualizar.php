<?php $this->loadDefault(SELF::HEADER); ?>
<div class="container">
    <div class="row">
        <div class="col-sm-12 d-flex">
            <a href="hospital/listar" class="btn btn-padrao mtop mbottom mright">Voltar</a>
            <a href="hospital/atualizar/<?php echo $this->hospital->getId();?>" class="btn btn-padrao mtop mbottom">Atualizar</a>
            <a href="hospital/excluir/<?php echo $this->hospital->getId();?>" class="btn btn-padrao mtop mbottom ml-auto">Excluir Hospital</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="tabela-padrao">
                <table>
                    <tbody>
                        <tr><td colspan="2" class="table-header">Hospital</td></tr>
                        <tr><th>Nome:</th><td><?php echo $this->hospital->getNome(); ?></td></tr>
                        <tr><th>Endereço:</th><td><?php echo $this->hospital->getEndereco(); ?></td></tr>
                        <tr><th>numero:</th><td><?php echo $this->hospital->getNumero(); ?></td></tr>
                        <tr><th>complemento:</th><td><?php echo $this->hospital->getComplemento(); ?></td></tr>
                        <tr><th>bairro:</th><td><?php echo $this->hospital->getBairro(); ?></td></tr>
                        <tr><th>cidade:</th><td><?php echo $this->hospital->getCidade(); ?></td></tr>
                        <tr><th>UF:</th><td><?php echo $this->hospital->getUf(); ?></td></tr>
                        <tr><th>CEP:</th><td><?php echo $this->hospital->getCep(); ?></td></tr>
                        <tr><th>Desde:</th><td><?php echo $this->hospital->getDesde(); ?></td></tr>
                        <tr><th>Avaliação:</th><td><?php echo $this->hospital->getAvaliacao(); ?></td></tr>
                        <tr><th>Observação:</th><td><?php echo $this->hospital->getObservacao(); ?></td></tr>
                        <tr><th>Data Criação:</th><td><?php echo $this->hospital->getDataCriacao(); ?></td></tr>
                        <tr><th>Data Atualização:</th><td><?php echo $this->hospital->getDataAtualizacao(); ?></td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<?php $this->loadDefault(SELF::FOOTER); ?>



