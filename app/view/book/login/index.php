<!-- preparando formulario para pagina -->
<?php $this->loadDefault(SELF::HEADER); ?>
    <div class="container top">
        <div class="row-opcao row d-flex justify-content-center">
            <div class="col-sm-2">
                <a href="login/paciente" class="cx-opcao">
                    <div class="bl-opcao">
                        <div class="superior d-flex justify-content-center align-items-end"><i class="fa fa-users" aria-hidden="true"></i></div>
                        <div class="inferior d-flex justify-content-center align-items-center"><span>Paciente</span></div>
                    </div>
                </a>
            </div>
            <div class="col-sm-2">
                <a href="login/fornecedor" class="cx-opcao">
                    <div class="bl-opcao">
                        <div class="superior d-flex justify-content-center align-items-end"><i class="fa fa-truck" aria-hidden="true"></i></div>
                        <div class="inferior d-flex justify-content-center align-items-center"><span>Fornecedor</span></div>
                    </div>
                </a>
            </div>
            <div class="col-sm-2">
                <a href="login/medico" class="cx-opcao">
                    <div class="bl-opcao">
                        <div class="superior d-flex justify-content-center align-items-end"><i class="fa fa-stethoscope" aria-hidden="true"></i></div>
                        <div class="inferior d-flex justify-content-center align-items-center"><span>Médico</span></div>
                    </div>
                </a>
            </div>
            <div class="col-sm-2">
                <a href="login/administrador" class="cx-opcao">
                    <div class="bl-opcao">
                        <div class="superior d-flex justify-content-center align-items-end"><i class="fa fa-user-circle-o" aria-hidden="true"></i></div>
                        <div class="inferior d-flex justify-content-center align-items-center"><span>Administrador</span></div>
                    </div>
                </a>
            </div>
        </div>
    </div>
<?php $this->loadDefault(SELF::FOOTER); ?>



