<!-- preparando formulario para pagina -->
<?php $this->loadDefault(SELF::HEADER); ?>
<div class="container top">
    <div class="row">
        <div class="col-sm-4 offset-4">
            <div class="form-login">
                <div class="header d-flex flex-column justify-content-center align-items-center">
                    <i class="fa fa-truck" aria-hidden="true"></i>
                    <span>Fornecedor</span>
                </div>
                <form class="form-group" method="post">
                    <div class="alert alert-danger <?php echo ($this->mensagem == null)? "none": ""; ?>" role="alert">
                        <?php echo $this->mensagem; ?>
                    </div>
                    <input required name="email" type="email" class="form-control mbottom" placeholder="E-mail">
                    <input required name="senha" type="password" class="form-control mbottom" placeholder="Senha">
                    <button type="submit" class="btn btn-padrao">Entrar</button>
                    <a href="" class="btn btn-padrao">Voltar</a>
                    <div class="link form-group mtop">
                        <a href="fornecedor/inserir" class="">Cadastrar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->loadDefault(SELF::FOOTER); ?>



