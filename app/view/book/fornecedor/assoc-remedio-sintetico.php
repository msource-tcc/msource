<?php $this->loadDefault(SELF::HEADER); ?>
<div class="container">
    <div class="row mtop">
        <div class="col-sm-12">
            <div class="alert alert-success <?php echo (is_null($this->mensagem)) ? "none" : "" ?>" role="alert">
                <?php echo $this->mensagem; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2 d-flex">
            <a href="fornecedor/visualizar" class="btn btn-padrao mtop mbottom">Voltar</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 ">
            <div class="form-default">
                <div class="header d-flex flex-column justify-content-center align-items-center">
                    <i class="fa fa-truck mright" aria-hidden="true"></i>
                    <span>Associação de fornecedor com seus remédios sinteticos</span>
                </div>
                <form method="post">
                    <div class="form-group">
                        <label>Fornecedor: <?php echo $this->fornecedor->getNome(); ?></label>
                    </div>
                    <div class="caixa-select-check">
                        <?php foreach ($this->arrayObjRemedioSintetico as $remedioSintetico) { ?>
                            <?php $check = "";
                            if($this->fornecedor->getRemedioSintetico()){
                                foreach ($this->fornecedor->getRemedioSintetico() as $remedioSinteticoAssoc) {
                                    if($remedioSintetico->getId() == $remedioSinteticoAssoc->getId()){ $check = "checked"; break; }
                                }
                            }?>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="remedio[]" value="<?php echo $remedioSintetico->getId(); ?>" <?php echo $check; ?> >
                            <label class="form-check-label" >
                                <?php echo $remedioSintetico->getNomePopular(); ?>
                            </label>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-padrao">Salvar</button>
                        <a href="doenca/listar" class="btn btn-padrao">Voltar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->loadDefault(SELF::FOOTER); ?>



