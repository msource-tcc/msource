<!-- preparando formulario para pagina -->
<?php $this->loadDefault(SELF::HEADER); ?>
<div class="container top">
    <div class="row">
        <div class="col-sm-12 ">
            <div class="form-default">
                <div class="header d-flex flex-column justify-content-center align-items-center">
                    <i class="fa fa-truck mright" aria-hidden="true"></i>
                    <span>Cadastro de fornecedor</span>
                </div>
                <form method="post">
                    <div class="form-group">
                        <label>Nome</label>
                        <input name="nome" type="text" class="form-control tam5" required>
                    </div>
                    <div class="form-group">
                        <label>Endereço</label>
                        <input name="endereco" type="text" class="form-control tam5" required>
                    </div>
                    <div class="form-group">
                        <label>Número</label>
                        <input name="numero" type="text" class="number form-control tam1" required>
                    </div>
                    <div class="form-group">
                        <label>Complemento</label>
                        <input name="complemento" type="text" class="form-control tam5" required>
                    </div>
                    <div class="form-group">
                        <label>Bairro</label>
                        <input name="bairro" type="text" class="form-control tam3" required>
                    </div>
                    <div class="form-group">
                        <label>Cidade</label>
                        <input name="cidade" type="text" class="form-control tam3" required>
                    </div>
                    <div class="form-group">
                        <label>UF</label>
                        <input name="uf" type="text" class="form-control tam05" maxlength="2" required>
                    </div>
                    <div class="form-group">
                        <label>CEP</label>
                        <input name="cep" type="text" class="cep form-control tam1" required>
                    </div>
                    <div class="form-group">
                        <label>Nome Fantasia</label>
                        <input name="nome_fantasia" type="text" class="form-control tam5" required>
                    </div>
                    <div class="form-group">
                        <label>Percentual Ramo (Ervas)</label>
                        <input name="percentual_ramo" type="text" class="float form-control tam1" required>
                    </div>
                    <div class="form-group">
                        <label>Atua Desde</label>
                        <input name="desde" type="text" class="date float form-control tam1" required>
                    </div>
                    <div class="form-group">
                        <label>Observação</label>
                        <textarea name="observacao" class="form-control"  rows="10"></textarea>
                    </div>
                    <div class="form-group">
                        <label>E-mail</label>
                        <input name="email" type="email" class="form-control tam3" required>
                    </div>
                    <div class="form-group">
                        <label>Senha</label>
                        <input name="senha" type="password" class="form-control tam1" required>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-padrao">Enviar</button>
                        <a href="login/fornecedor" class="btn btn-padrao">Voltar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->loadDefault(SELF::FOOTER); ?>



