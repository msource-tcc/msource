<!-- preparando formulario para pagina -->
<?php $this->loadDefault(SELF::HEADER); ?>
<div class="container">
    <div class="row">
        <div class="col-sm-12 d-flex">
            <a href="dashboard" class="btn btn-padrao mtop mbottom">voltar</a>
            <a href="fornecedor/atualizar" class="btn btn-padrao mtop mbottom mleft">Atualizar</a>
            <a href="fornecedor/assoc-remedio-natural" class="btn btn-padrao mtop mbottom mleft">Associar Remédio Natural</a>
            <a href="fornecedor/assoc-remedio-sintetico" class="btn btn-padrao mtop mbottom mleft">Associar Remédio Sintetico</a>
            <a href="fornecedor/excluir" class="btn btn-padrao mtop mbottom mleft ml-auto">Excluir Conta</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="tabela-padrao">
                <table>
                    <tbody>
                        <tr><td colspan="2" class="table-header">Perfil</td></tr>
                        <tr><th>nome:</th><td><?php echo $this->fornecedor->getNome(); ?></td></tr>
                        <tr><th>Endereço:</th><td><?php echo $this->fornecedor->getEndereco(); ?></td></tr>
                        <tr><th>numero:</th><td><?php echo $this->fornecedor->getNumero(); ?></td></tr>
                        <tr><th>complemento:</th><td><?php echo $this->fornecedor->getComplemento(); ?></td></tr>
                        <tr><th>bairro:</th><td><?php echo $this->fornecedor->getBairro(); ?></td></tr>
                        <tr><th>cidade:</th><td><?php echo $this->fornecedor->getCidade(); ?></td></tr>
                        <tr><th>UF:</th><td><?php echo $this->fornecedor->getUf(); ?></td></tr>
                        <tr><th>CEP:</th><td><?php echo $this->fornecedor->getCep(); ?></td></tr>
                        <tr><th>E-mail:</th><td><?php echo $this->fornecedor->getEmail(); ?></td></tr>
                        <tr><th>Senha:</th><td><?php echo $this->fornecedor->getSenha(); ?></td></tr>
                        <tr><th>Nome Fantasia:</th><td><?php echo $this->fornecedor->getNomeFantasia(); ?></td></tr>
                        <tr><th>Atua Desde:</th><td><?php echo $this->fornecedor->getDesde(); ?></td></tr>
                        <tr><th>Percentual Ramo:</th><td><?php echo $this->fornecedor->getPercentualRamo(); ?>%</td></tr>
                        <tr><th>Observação:</th><td><?php echo $this->fornecedor->getObservacao(); ?></td></tr>
                        <tr><th>Data Criação:</th><td><?php echo $this->fornecedor->getDataCriacao(); ?></td></tr>
                        <tr><th>Data Atualização:</th><td><?php echo $this->fornecedor->getDataAtualizacao(); ?></td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-sm-12 mtop">
            <?php if($this->fornecedor->getRemedioNatural()){ ?>
                <div class="cx-acordeon">
                    <div class="cartao">
                        <div class="cartao-header" id="1">
                            <h5 class="mb-0">
                                <span>Remédios Naturais</span>
                            </h5>
                        </div>
                        <div id="cartao-1" class="none">
                            <div class="cartao-body">
                                <?php foreach ($this->fornecedor->getRemedioNatural() as $remedioNatural) { ?>
                                    <div class="caixa-item">
                                        <div class="faixa-item" id="<?php echo $remedioNatural->getId(); ?>-cartao-1"><?php echo $remedioNatural->getNomePopular(); ?></div>
                                        <div class="bloco-item none" id="bloco-item-<?php echo $remedioNatural->getId(); ?>-cartao-1">
                                            <div class="tabela-padrao">
                                                <table>
                                                    <tbody>
                                                    <tr><th>Nome Popular:</th><td><?php echo $remedioNatural->getNomePopular(); ?></td></tr>
                                                    <tr><th>Nome Cientifico:</th><td><?php echo $remedioNatural->getNomeCientifico(); ?></td></tr>
                                                    <tr><th>Classe:</th><td><?php echo $remedioNatural->getClasse(); ?></td></tr>
                                                    <tr><th>Familia:</th><td><?php echo $remedioNatural->getFamilia(); ?></td></tr>
                                                    <tr><th>Gênero:</th><td><?php echo $remedioNatural->getGenero(); ?></td></tr>
                                                    <tr><th>Reino:</th><td><?php echo $remedioNatural->getReino(); ?></td></tr>
                                                    <tr><th>Quilograma:</th><td><?php echo $remedioNatural->getQuilograma(); ?></td></tr>
                                                    <tr><th>Calorias:</th><td><?php echo $remedioNatural->getCalorias(); ?></td></tr>
                                                    <tr><th>Cor Natural:</th><td><?php echo $remedioNatural->getCorNatural(); ?></td></tr>
                                                    <tr><th>Data Descoberta:</th><td><?php echo $remedioNatural->getDataDescoberta(); ?></td></tr>
                                                    <tr><th>Fins Medicinais:</th><td><?php echo $remedioNatural->getFinsMedicinais(); ?></td></tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>

        <div class="col-sm-12 mtop">
            <?php if($this->fornecedor->getRemedioSintetico()){ ?>
                <div class="cx-acordeon">
                    <div class="cartao">
                        <div class="cartao-header" id="2">
                            <h5 class="mb-0">
                                <span>Remédios Sintéticos</span>
                            </h5>
                        </div>
                        <div id="cartao-2" class="none">
                            <div class="cartao-body">
                                <?php foreach ($this->fornecedor->getRemedioSintetico() as $remedioSintetico) { ?>
                                    <div class="caixa-item">
                                        <div class="faixa-item" id="<?php echo $remedioSintetico->getId(); ?>-cartao-2"><?php echo $remedioSintetico->getNomePopular(); ?></div>
                                        <div class="bloco-item none" id="bloco-item-<?php echo $remedioSintetico->getId(); ?>-cartao-2">
                                            <div class="tabela-padrao">
                                                <table>
                                                    <tbody>
                                                    <tr><th>Nome Popular:</th><td><?php echo $remedioSintetico->getNomePopular(); ?></td></tr>
                                                    <tr><th>Nome Cientifico:</th><td><?php echo $remedioSintetico->getNomeCientifico(); ?></td></tr>
                                                    <tr><th>Quilograma:</th><td><?php echo $remedioSintetico->getQuilograma(); ?></td></tr>
                                                    <tr><th>Calorias:</th><td><?php echo $remedioSintetico->getCalorias(); ?></td></tr>
                                                    <tr><th>Excreção:</th><td><?php echo $remedioSintetico->getExcrecao(); ?></td></tr>
                                                    <tr><th>Administração:</th><td><?php echo $remedioSintetico->getAdministracao(); ?></td></tr>
                                                    <tr><th>CAS:</th><td><?php echo $remedioSintetico->getCas(); ?></td></tr>
                                                    <tr><th>Metabolismo:</th><td><?php echo $remedioSintetico->getMetabolismo(); ?></td></tr>
                                                    <tr><th>Data Descoberta:</th><td><?php echo $remedioSintetico->getDataDescoberta(); ?></td></tr>
                                                    <tr><th>Fins Medicinais:</th><td><?php echo $remedioSintetico->getFinsMedicinais(); ?></td></tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php $this->loadDefault(SELF::FOOTER); ?>



