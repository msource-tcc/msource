<?php $this->loadDefault(SELF::HEADER); ?>
<div class="container">
    <div class="row mtop">
        <div class="col-sm-12">
            <div class="alert alert-success <?php echo (is_null($this->mensagem)) ? "none" : "" ?>" role="alert">
                <?php echo $this->mensagem; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 d-flex">
            <a href="fornecedor/visualizar" class="btn btn-padrao mbottom">Voltar</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-default">
                <div class="header d-flex flex-column justify-content-center align-items-center">
                    <i class="fa fa-truck mright" aria-hidden="true"></i>
                    <span>Atualizar fornecedor</span>
                </div>
                <form method="post">
                    <div class="form-group">
                        <label>Nome</label>
                        <input name="nome" type="text" class="form-control tam5" required value="<?php echo $this->fornecedor->getNome() ?>">
                    </div>
                    <div class="form-group">
                        <label>Endereço</label>
                        <input name="endereco" type="text" class="form-control tam5" required value="<?php echo $this->fornecedor->getEndereco() ?>">
                    </div>
                    <div class="form-group">
                        <label>Número</label>
                        <input name="numero" type="text" class="number form-control tam1" required value="<?php echo $this->fornecedor->getNumero() ?>">
                    </div>
                    <div class="form-group">
                        <label>Complemento</label>
                        <input name="complemento" type="text" class="form-control tam5" required value="<?php echo $this->fornecedor->getComplemento() ?>">
                    </div>
                    <div class="form-group">
                        <label>Bairro</label>
                        <input name="bairro" type="text" class="form-control tam3" required value="<?php echo $this->fornecedor->getBairro() ?>">
                    </div>
                    <div class="form-group">
                        <label>Cidade</label>
                        <input name="cidade" type="text" class="form-control tam3" required value="<?php echo $this->fornecedor->getCidade() ?>">
                    </div>
                    <div class="form-group">
                        <label>UF</label>
                        <input name="uf" type="text" class="form-control tam05" maxlength="2" required value="<?php echo $this->fornecedor->getUf() ?>">
                    </div>
                    <div class="form-group">
                        <label>CEP</label>
                        <input name="cep" type="text" class="cep form-control tam1" required value="<?php echo $this->fornecedor->getCep() ?>">
                    </div>
                    <div class="form-group">
                        <label>Nome Fantasia</label>
                        <input name="nome_fantasia" type="text" class="form-control tam5" required value="<?php echo $this->fornecedor->getNomeFantasia() ?>">
                    </div>
                    <div class="form-group">
                        <label>Atua Desde</label>
                        <input name="desde" type="text" class="date form-control tam1" required value="<?php echo $this->fornecedor->getDesde() ?>">
                    </div>
                    <div class="form-group">
                        <label>Percentual Ramo</label>
                        <input name="percentual_ramo" type="text" class="percentual form-control tam1" required value="<?php echo $this->fornecedor->getPercentualRamo() ?>>"
                    </div>
                    <div class="form-group">
                        <label>Observação</label>
                        <textarea name="observacao" class="form-control"  rows="10"> <?php echo $this->fornecedor->getObservacao() ?></textarea>
                    </div>
                    <div class="form-group">
                        <label>E-mail</label>
                        <input name="email" type="email" class="form-control tam3" required value="<?php echo $this->fornecedor->getEmail() ?>">
                    </div>
                    <div class="form-group">
                        <label>Senha</label>
                        <input name="senha" type="password" class="form-control tam1" required value="<?php echo $this->fornecedor->getSenha() ?>">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-padrao">Enviar</button>
                        <a href="fornecedor/visualizar" class="btn btn-padrao">Voltar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->loadDefault(SELF::FOOTER); ?>



