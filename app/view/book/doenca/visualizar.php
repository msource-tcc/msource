<?php $this->loadDefault(SELF::HEADER); ?>
<div class="container">
    <div class="row">
        <div class="col-sm-12 d-flex">
            <a href="doenca/listar" class="btn btn-padrao mtop mbottom">Voltar</a>
            <?php if($this->tipoSessao != "paciente"){ ?>
            <a href="doenca/atualizar/<?php echo $this->doenca->getId();?>" class="btn btn-padrao mtop mbottom mleft">Atualizar</a>
            <a href="doenca/assoc-remedio-natural/<?php echo $this->doenca->getId();?>" class="btn btn-padrao mtop mbottom mleft">Associar Remédio Natural</a>
            <a href="doenca/assoc-remedio-sintetico/<?php echo $this->doenca->getId();?>" class="btn btn-padrao mtop mbottom mleft">Associar Remédio Sintetico</a>
            <a href="doenca/excluir/<?php echo $this->doenca->getId();?>" class="btn btn-padrao mtop mbottom ml-auto">Excluir Doença</a>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="tabela-padrao">
                <table>
                    <tbody>
                        <tr><td colspan="2" class="table-header">Doença</td></tr>
                        <tr><th>Nome Popular:</th><td><?php echo $this->doenca->getNomePopular(); ?></td></tr>
                        <tr><th>Nome Cientifico:</th><td><?php echo $this->doenca->getNomeCientifico(); ?></td></tr>
                        <tr><th>Sintomas:</th><td><?php echo $this->doenca->getSintomas(); ?></td></tr>
                        <tr><th>Percentual de Risco:</th><td><?php echo $this->doenca->getPercentualRisco(); ?>%</td></tr>
                        <tr><th>Especialidade:</th><td><?php echo $this->doenca->getEspecialidade(); ?></td></tr>
                        <tr><th>Metodo Diagnostico:</th><td><?php echo $this->doenca->getMetodoDiagnostico(); ?></td></tr>
                        <tr><th>Periodo:</th><td><?php echo $this->doenca->getPeriodo(); ?></td></tr>
                        <tr><th>Dias Durações:</th><td><?php echo $this->doenca->getDiasDuracao(); ?></td></tr>
                        <tr><th>Inicio:</th><td><?php echo $this->doenca->getInicio(); ?></td></tr>
                        <tr><th>Data Descoberta:</th><td><?php echo $this->doenca->getDataDescoberta(); ?></td></tr>
                        <tr><th>Obervação:</th><td><?php echo $this->doenca->getObservacao(); ?></td></tr>
                        <tr><th>Data Criação:</th><td><?php echo $this->doenca->getDataCriacao(); ?></td></tr>
                        <tr><th>Data Atualização:</th><td><?php echo $this->doenca->getDataAtualizacao(); ?></td></tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-12 mtop">
            <?php if($this->doenca->getMedico()){ ?>
                <div class="cx-acordeon">
                    <div class="cartao">
                        <div class="cartao-header" id="1">
                            <h5 class="mb-0">
                                <span>Médicos agregados com percentual de especialização acima de 50.00%</span>
                            </h5>
                        </div>
                        <div id="cartao-1" class="none">
                            <div class="cartao-body">
                                <?php foreach ($this->doenca->getMedico() as $medico) { ?>
                                    <div class="caixa-item">
                                        <div class="faixa-item" id="<?php echo $medico->getId(); ?>-cartao-1"><?php echo $medico->getNome(); ?></div>
                                        <div class="bloco-item none" id="bloco-item-<?php echo $medico->getId(); ?>-cartao-1">
                                            <div class="tabela-padrao">
                                                <table>
                                                    <tbody>
                                                    <tr><th>nome:</th><td><?php echo $medico->getNome(); ?></td></tr>
                                                    <tr><th>cidade:</th><td><?php echo $medico->getCidade(); ?></td></tr>
                                                    <tr><th>UF:</th><td><?php echo $medico->getUf(); ?></td></tr>
                                                    <tr><th>E-mail:</th><td><?php echo $medico->getEmail(); ?></td></tr>
                                                    <tr><th>Sexo:</th><td><?php echo $medico->getSexo(); ?></td></tr>
                                                    <tr><th>Esp. Remédio Natural:</th><td><?php echo $medico->getEspRemedioNat(); ?>%</td></tr>
                                                    <tr><th>Esp. Remédio Sintetico:</th><td><?php echo $medico->getEspRemedioSint(); ?>%</td></tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>

        <div class="col-sm-12 mtop">
            <?php if($this->doenca->getRemedioNatural()){ ?>
                <div class="cx-acordeon">
                    <div class="cartao">
                        <div class="cartao-header" id="2">
                            <h5 class="mb-0">
                                <span>Remédios Naturais</span>
                            </h5>
                        </div>
                        <div id="cartao-2" class="none">
                            <div class="cartao-body">
                                <?php foreach ($this->doenca->getRemedioNatural() as $remedioNatural) { ?>
                                    <div class="caixa-item">
                                        <div class="faixa-item" id="<?php echo $remedioNatural->getId(); ?>-cartao-2"><?php echo $remedioNatural->getNomePopular(); ?></div>
                                        <div class="bloco-item none" id="bloco-item-<?php echo $remedioNatural->getId(); ?>-cartao-2">
                                            <div class="tabela-padrao">
                                                <table>
                                                    <tbody>
                                                        <tr><th>Nome Popular:</th><td><?php echo $remedioNatural->getNomePopular(); ?></td></tr>
                                                        <tr><th>Nome Cientifico:</th><td><?php echo $remedioNatural->getNomeCientifico(); ?></td></tr>
                                                        <tr><th>Classe:</th><td><?php echo $remedioNatural->getClasse(); ?></td></tr>
                                                        <tr><th>Familia:</th><td><?php echo $remedioNatural->getFamilia(); ?></td></tr>
                                                        <tr><th>Gênero:</th><td><?php echo $remedioNatural->getGenero(); ?></td></tr>
                                                        <tr><th>Reino:</th><td><?php echo $remedioNatural->getReino(); ?></td></tr>
                                                        <tr><th>Quilograma:</th><td><?php echo $remedioNatural->getQuilograma(); ?></td></tr>
                                                        <tr><th>Calorias:</th><td><?php echo $remedioNatural->getCalorias(); ?></td></tr>
                                                        <tr><th>Cor Natural:</th><td><?php echo $remedioNatural->getCorNatural(); ?></td></tr>
                                                        <tr><th>Data Descoberta:</th><td><?php echo $remedioNatural->getDataDescoberta(); ?></td></tr>
                                                        <tr><th>Fins Medicinais:</th><td><?php echo $remedioNatural->getFinsMedicinais(); ?></td></tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>

        <div class="col-sm-12 mtop">
            <?php if($this->doenca->getRemedioSintetico()){ ?>
                <div class="cx-acordeon">
                    <div class="cartao">
                        <div class="cartao-header" id="3">
                            <h5 class="mb-0">
                                <span>Remédios Sintéticos</span>
                            </h5>
                        </div>
                        <div id="cartao-3" class="none">
                            <div class="cartao-body">
                                <?php foreach ($this->doenca->getRemedioSintetico() as $remedioSintetico) { ?>
                                    <div class="caixa-item">
                                        <div class="faixa-item" id="<?php echo $remedioSintetico->getId(); ?>-cartao-3"><?php echo $remedioSintetico->getNomePopular(); ?></div>
                                        <div class="bloco-item none" id="bloco-item-<?php echo $remedioSintetico->getId(); ?>-cartao-3">
                                            <div class="tabela-padrao">
                                                <table>
                                                    <tbody>
                                                    <tr><th>Nome Popular:</th><td><?php echo $remedioSintetico->getNomePopular(); ?></td></tr>
                                                    <tr><th>Nome Cientifico:</th><td><?php echo $remedioSintetico->getNomeCientifico(); ?></td></tr>
                                                    <tr><th>Quilograma:</th><td><?php echo $remedioSintetico->getQuilograma(); ?></td></tr>
                                                    <tr><th>Calorias:</th><td><?php echo $remedioSintetico->getCalorias(); ?></td></tr>
                                                    <tr><th>Excreção:</th><td><?php echo $remedioSintetico->getExcrecao(); ?></td></tr>
                                                    <tr><th>Administração:</th><td><?php echo $remedioSintetico->getAdministracao(); ?></td></tr>
                                                    <tr><th>CAS:</th><td><?php echo $remedioSintetico->getCas(); ?></td></tr>
                                                    <tr><th>Metabolismo:</th><td><?php echo $remedioSintetico->getMetabolismo(); ?></td></tr>
                                                    <tr><th>Data Descoberta:</th><td><?php echo $remedioSintetico->getDataDescoberta(); ?></td></tr>
                                                    <tr><th>Fins Medicinais:</th><td><?php echo $remedioSintetico->getFinsMedicinais(); ?></td></tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php $this->loadDefault(SELF::FOOTER); ?>



