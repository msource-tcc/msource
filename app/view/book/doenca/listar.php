<?php $this->loadDefault(SELF::HEADER); ?>
<div class="container">
    <div class="row">
        <div class="col-sm-2 d-flex">
            <a href="dashboard" class="btn btn-padrao mtop mbottom mright">Voltar</a>
            <?php if($this->tipoSessao != "paciente"){ ?>
            <a href="doenca/inserir" class="btn btn-padrao mtop mbottom">Cadastrar</a>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?php if(empty($this->arrayObjDoencas)){?>
                <div class="alert alert-warning" role="alert">
                    Nenhum registro.
                </div>
            <?php } else { ?>
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col" colspan="5"><span class="d-flex justify-content-center">Doença</span></th>
                    </tr>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nome Popular</th>
                        <th scope="col">Nome Cientifico</th>
                        <th scope="col">Especialização</th>
                        <th scope="col">Dias Duração</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($this->arrayObjDoencas as $doenca) { ?>
                            <tr>
                                <th scope="row"><?php echo $doenca->getId(); ?></th>
                                <td><a href="doenca/visualizar/<?php echo $doenca->getId(); ?>"><?php echo $doenca->getNomePopular(); ?></a></td>
                                <td><?php echo $doenca->getNomeCientifico(); ?></td>
                                <td><?php echo $doenca->getEspecialidade(); ?></td>
                                <td><?php echo $doenca->getDiasDuracao(); ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php } ?>
        </div>
    </div>
</div>
<?php $this->loadDefault(SELF::FOOTER); ?>



