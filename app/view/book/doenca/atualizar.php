<?php $this->loadDefault(SELF::HEADER); ?>
<div class="container">
    <div class="row mtop">
        <div class="col-sm-12">
            <div class="alert alert-success <?php echo (is_null($this->mensagem)) ? "none" : "" ?>" role="alert">
                <?php echo $this->mensagem; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2 d-flex">
            <a href="doenca/visualizar/<?php echo $this->doenca->getId(); ?>" class="btn btn-padrao mtop mbottom">Voltar</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 ">
            <div class="form-default">
                <div class="header d-flex flex-column justify-content-center align-items-center">
                    <i class="fa fa-heartbeat mright" aria-hidden="true"></i>
                    <span>Atualização de doença</span>
                </div>
                <form method="post">

                    <div class="form-group">
                        <label>Nome Popular</label>
                        <input name="nome_popular" type="text" class="form-control tam5" required value="<?php echo $this->doenca->getNomePopular(); ?>">
                    </div>
                    <div class="form-group">
                        <label>Nome Cientifico</label>
                        <input name="nome_cientifico" type="text" class="form-control tam5" required value="<?php echo $this->doenca->getNomeCientifico(); ?>">
                    </div>
                    <div class="form-group">
                        <label>Sintomas</label>
                        <input name="sintomas" type="text" class="form-control tam5" required value="<?php echo $this->doenca->getSintomas(); ?>">
                    </div>
                    <div class="form-group">
                        <label>Percentual Risco</label>
                        <input name="percentual_risco" type="text" class="percentual form-control tam05" required value="<?php echo $this->doenca->getPercentualRisco(); ?>">
                    </div>
                    <div class="form-group">
                        <label>Especialidade</label>
                        <input name="especialidade" type="text" class="form-control tam5" required value="<?php echo $this->doenca->getEspecialidade(); ?>">
                    </div>
                    <div class="form-group">
                        <label>Metodo Diagnostico</label>
                        <input name="metodo_diagnostico" type="text" class="form-control tam5" required value="<?php echo $this->doenca->getMetodoDiagnostico(); ?>">
                    </div>
                    <div class="form-group">
                        <label>Periodo</label>
                        <input name="periodo" type="text" class="form-control tam5" required value="<?php echo $this->doenca->getPeriodo(); ?>">
                    </div>
                    <div class="form-group">
                        <label>Dias Duração</label>
                        <input name="dias_duracao" type="text" class="number form-control tam05" required value="<?php echo $this->doenca->getDiasDuracao(); ?>">
                    </div>
                    <div class="form-group">
                        <label>Inicio</label>
                        <input name="inicio" type="text" class="form-control tam5" required value="<?php echo $this->doenca->getInicio(); ?>">
                    </div>
                    <div class="form-group">
                        <label>Data Descoberta</label>
                        <input name="data_descoberta" type="text" class="date form-control tam1" required value="<?php echo $this->doenca->getDataDescoberta(); ?>">
                    </div>
                    <div class="form-group">
                        <label>Observação</label>
                        <textarea name="observacao" class="form-control"  rows="10"><?php echo $this->doenca->getObservacao(); ?></textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-padrao">Salvar</button>
                        <a href="doenca/visualizar/<?php echo $this->doenca->getId(); ?>" class="btn btn-padrao">Voltar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->loadDefault(SELF::FOOTER); ?>



