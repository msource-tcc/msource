<!-- preparando formulario para pagina -->
<?php $this->loadDefault(SELF::HEADER); ?>
<div class="sub-header">
    <div class="container h100">
        <div class="row h100">
            <div class="col-sm-6 d-flex align-items-center">
                <span>Olá, <?php echo $this->acessoSessao->getNome(); ?> | Médico</span>
            </div>
            <div class="col-sm-6 d-flex align-items-center justify-content-end">
                <a href="dashboard" class="btn btn-padrao-inline mright">Voltar</a>
                <a href="logoff" class="btn btn-padrao-inline">Sair</a>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-sm-2 d-flex">
            <a href="produto/inserir" class="btn btn-padrao mtop mbottom">Cadastrar</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?php if(empty($this->medico->getProduto())){?>
                <div class="alert alert-warning" role="alert">
                    Não existe produto cadastrado com seu perfil.
                </div>
            <?php } else { ?>
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col" colspan="5"><span class="d-flex justify-content-center">Produto por perfil</span></th>
                    </tr>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nome Popular</th>
                        <th scope="col">Nome Cientifico</th>
                        <th scope="col">Quilogramas</th>
                        <th scope="col">Calorias</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($this->medico->getProduto() as $produto) { ?>
                            <tr>
                                <th scope="row"><?php echo $produto->getId(); ?></th>
                                <td><a href="produto/visualizar/<?php echo $produto->getId(); ?>"><?php echo $produto->getNomePopular(); ?></a></td>
                                <td><?php echo $produto->getNomeCientifico(); ?></td>
                                <td><?php echo $produto->getQuilograma(); ?></td>
                                <td><?php echo $produto->getCalorias(); ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php } ?>
        </div>
    </div>
</div>
<?php $this->loadDefault(SELF::FOOTER); ?>



