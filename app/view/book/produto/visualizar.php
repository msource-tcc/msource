<!-- preparando formulario para pagina -->
<?php $this->loadDefault(SELF::HEADER); ?>
<div class="sub-header">
    <div class="container h100">
        <div class="row h100">
            <div class="col-sm-6 d-flex align-items-center">
                <span>Olá, <?php echo $this->acessoSessao->getNome(); ?> | Médico</span>
            </div>
            <div class="col-sm-6 d-flex align-items-center justify-content-end">
                <a href="dashboard" class="btn btn-padrao-inline mright">Voltar</a>
                <a href="logoff" class="btn btn-padrao-inline">Sair</a>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-sm-2 d-flex">
            <a href="produto/atualizar/<?php echo $this->produto->getId(); ?>" class="btn btn-padrao mtop mbottom">Atualizar</a>
            <a href="produto/excluir/<?php echo $this->produto->getId(); ?>" class="btn btn-padrao mtop mbottom mleft">Excluir</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="tabela-padrao">
                <table>
                    <tbody>
                        <tr><td colspan="2" class="table-header">Produto</td></tr>
                        <tr><th>Nome Popular:</th><td><?php echo $this->produto->getNomePopular(); ?></td></tr>
                        <tr><th>Nome Cientifico:</th><td><?php echo $this->produto->getNomeCientifico(); ?></td></tr>
                        <tr><th>Classe:</th><td><?php echo $this->produto->getClasse(); ?></td></tr>
                        <tr><th>Familia:</th><td><?php echo $this->produto->getFamilia(); ?></td></tr>
                        <tr><th>Gênero:</th><td><?php echo $this->produto->getGenero(); ?></td></tr>
                        <tr><th>Reino:</th><td><?php echo $this->produto->getReino(); ?></td></tr>
                        <tr><th>Quilograma:</th><td><?php echo $this->produto->getQuilograma(); ?></td></tr>
                        <tr><th>Calorias:</th><td><?php echo $this->produto->getCalorias(); ?></td></tr>
                        <tr><th>Cor Natural:</th><td><?php echo $this->produto->getCorNatural(); ?></td></tr>
                        <tr><th>DataDescoberta:</th><td><?php echo $this->produto->getDataDescoberta(); ?></td></tr>
                        <tr><th>FinsMedicinais:</th><td><?php echo $this->produto->getFinsMedicinais(); ?></td></tr>
                        <tr><th>Data Criação:</th><td><?php echo $this->produto->getDataCriacao(); ?></td></tr>
                        <tr><th>Data Atualização:</th><td><?php echo $this->produto->getDataAtualizacao(); ?></td></tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<?php $this->loadDefault(SELF::FOOTER); ?>



