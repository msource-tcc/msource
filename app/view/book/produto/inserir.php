<!-- preparando formulario para pagina -->
<!--HEADER-->
<?php $this->loadDefault(SELF::HEADER); ?>
<div class="sub-header">
    <div class="container h100">
        <div class="row h100">
            <div class="col-sm-6 d-flex align-items-center">
                <span>Olá, <?php echo $this->acessoSessao->getNome(); ?> | Médico</span>
            </div>
            <div class="col-sm-6 d-flex align-items-center justify-content-end">
                <a href="produto/listar" class="btn btn-padrao-inline mright">Voltar</a>
                <a href="logoff" class="btn btn-padrao-inline">Sair</a>
            </div>
        </div>
    </div>
</div>
<!--HEADER-->
<div class="container">
    <div class="row mtop">
        <div class="col-sm-12">
            <div class="alert alert-success <?php echo (is_null($this->mensagem)) ? "none" : "" ?>" role="alert">
                <?php echo $this->mensagem; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 ">
            <div class="form-default">
                <div class="header d-flex flex-column justify-content-center align-items-center">
                    <i class="fa fa-leaf mright" aria-hidden="true"></i>
                    <span>Cadastro de produto</span>
                </div>
                <form method="post">
                    <div class="form-group">
                        <label>Nome Popular</label>
                        <input name="nome_popular" type="text" class="form-control tam5" required>
                    </div>
                    <div class="form-group">
                        <label>Nome Cientifico</label>
                        <input name="nome_cientifico" type="text" class="form-control tam5" required>
                    </div>
                    <div class="form-group">
                        <label>Classe</label>
                        <input name="classe" type="text" class="form-control tam3" required>
                    </div>
                    <div class="form-group">
                        <label>Familia</label>
                        <input name="familia" type="text" class="form-control tam3" required>
                    </div>
                    <div class="form-group">
                        <label>Gênero</label>
                        <input name="genero" type="text" class="form-control tam3" required>
                    </div>
                    <div class="form-group">
                        <label>Reino</label>
                        <input name="reino" type="text" class="form-control tam3" required>
                    </div>
                    <div class="form-group">
                        <label>Quilogramas</label>
                        <input name="quilograma" type="text" class="quilo form-control tam05" required>
                    </div>
                    <div class="form-group">
                        <label>Calorias</label>
                        <input name="calorias" type="text" class="form-control tam05" required>
                    </div>
                    <div class="form-group">
                        <label>Cor Natural</label>
                        <input name="cor_natural" type="text" class="form-control tam3" required>
                    </div>
                    <div class="form-group">
                        <label>Data Descoberta</label>
                        <input name="data_descoberta" type="text" class="date form-control tam1" required>
                    </div>
                    <div class="form-group">
                        <label>Fins Medicinais</label>
                        <textarea name="fins_medicinais" class="form-control"  rows="10"></textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-padrao">Salvar</button>
                        <a href="produto/listar" class="btn btn-padrao">Voltar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->loadDefault(SELF::FOOTER); ?>



