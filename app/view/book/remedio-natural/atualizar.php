<?php $this->loadDefault(SELF::HEADER); ?>
<div class="container">
    <div class="row mtop">
        <div class="col-sm-12">
            <div class="alert alert-success <?php echo (is_null($this->mensagem)) ? "none" : "" ?>" role="alert">
                <?php echo $this->mensagem; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2 d-flex">
            <a href="remedio-natural/visualizar/<?php echo $this->remedioNatural->getId(); ?>" class="btn btn-padrao mtop mbottom">Voltar</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 ">
            <div class="form-default">
                <div class="header d-flex flex-column justify-content-center align-items-center">
                    <i class="fa fa-leaf mright" aria-hidden="true"></i>
                    <span>Atualizar remédio natural</span>
                </div>
                <form method="post">
                    <div class="form-group">
                        <label>Nome Popular</label>
                        <input name="nome_popular" type="text" class="form-control tam5" required value="<?php echo $this->remedioNatural->getNomePopular(); ?>">
                    </div>
                    <div class="form-group">
                        <label>Nome Cientifico</label>
                        <input name="nome_cientifico" type="text" class="form-control tam5" required value="<?php echo $this->remedioNatural->getNomeCientifico(); ?>">
                    </div>
                    <div class="form-group">
                        <label>Classe</label>
                        <input name="classe" type="text" class="form-control tam2" required value="<?php echo $this->remedioNatural->getClasse(); ?>">
                    </div>
                    <div class="form-group">
                        <label>Familia</label>
                        <input name="familia" type="text" class="form-control tam2" required value="<?php echo $this->remedioNatural->getFamilia(); ?>">
                    </div>
                    <div class="form-group">
                        <label>Genero</label>
                        <input name="genero" type="text" class="form-control tam2" required value="<?php echo $this->remedioNatural->getGenero(); ?>">
                    </div>
                    <div class="form-group">
                        <label>Reino</label>
                        <input name="reino" type="text" class="form-control tam2" required value="<?php echo $this->remedioNatural->getReino(); ?>">
                    </div>
                    <div class="form-group">
                        <label>Quilograma</label>
                        <input name="quilograma" type="text" class="float form-control tam05" required value="<?php echo $this->remedioNatural->getQuilograma(); ?>">
                    </div>
                    <div class="form-group">
                        <label>Calorias</label>
                        <input name="calorias" type="text" class="float form-control tam05" required value="<?php echo $this->remedioNatural->getCalorias(); ?>">
                    </div>
                    <div class="form-group">
                        <label>Cor Natural</label>
                        <input name="cor_natural" type="text" class="form-control tam3" required value="<?php echo $this->remedioNatural->getCorNatural(); ?>">
                    </div>
                    <div class="form-group">
                        <label>Data Descoberta</label>
                        <input name="data_descoberta" type="text" class="date form-control tam1" required value="<?php echo $this->remedioNatural->getDataDescoberta(); ?>">
                    </div>
                    <div class="form-group">
                        <label>Fins Medicinais</label>
                        <textarea name="fins_medicinais" class="form-control"  rows="10"><?php echo $this->remedioNatural->getFinsMedicinais(); ?></textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-padrao">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->loadDefault(SELF::FOOTER); ?>



