<?php $this->loadDefault(SELF::HEADER); ?>
<div class="container">
    <div class="row">
        <div class="col-sm-12 d-flex">
            <a href="remedio-natural/listar" class="btn btn-padrao mtop mbottom mright">Voltar</a>
            <?php if($this->tipoSessao != "paciente"){ ?>
            <a href="remedio-natural/atualizar/<?php echo $this->remedioNatural->getId();?>" class="btn btn-padrao mtop mbottom">Atualizar</a>
            <a href="remedio-natural/excluir/<?php echo $this->remedioNatural->getId();?>" class="btn btn-padrao mtop mbottom ml-auto">Excluir Remedio Natural</a>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="tabela-padrao">
                <table>
                    <tbody>
                        <tr><td colspan="2" class="table-header">Remédio Natural</td></tr>
                        <tr><th>Nome Popular:</th><td><?php echo $this->remedioNatural->getNomePopular(); ?></td></tr>
                        <tr><th>Nome Cientifico:</th><td><?php echo $this->remedioNatural->getNomeCientifico(); ?></td></tr>
                        <tr><th>Classe:</th><td><?php echo $this->remedioNatural->getClasse(); ?></td></tr>
                        <tr><th>Familia:</th><td><?php echo $this->remedioNatural->getFamilia(); ?></td></tr>
                        <tr><th>Gênero:</th><td><?php echo $this->remedioNatural->getGenero(); ?></td></tr>
                        <tr><th>Reino:</th><td><?php echo $this->remedioNatural->getReino(); ?></td></tr>
                        <tr><th>Quilograma:</th><td><?php echo $this->remedioNatural->getQuilograma(); ?></td></tr>
                        <tr><th>Calorias:</th><td><?php echo $this->remedioNatural->getCalorias(); ?></td></tr>
                        <tr><th>Cor Natural:</th><td><?php echo $this->remedioNatural->getCorNatural(); ?></td></tr>
                        <tr><th>Data Descoberta:</th><td><?php echo $this->remedioNatural->getDataDescoberta(); ?></td></tr>
                        <tr><th>Fins Medicinais:</th><td><?php echo $this->remedioNatural->getFinsMedicinais(); ?></td></tr>
                        <tr><th>Data Criação:</th><td><?php echo $this->remedioNatural->getDataCriacao(); ?></td></tr>
                        <tr><th>Data Atualização:</th><td><?php echo $this->remedioNatural->getDataAtualizacao(); ?></td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-12 mtop">
            <?php if($this->remedioNatural->getDoenca()){ ?>
                <div class="cx-acordeon">
                    <div class="cartao">
                        <div class="cartao-header" id="1">
                            <h5 class="mb-0">
                                <span>Doença</span>
                            </h5>
                        </div>
                        <div id="cartao-1" class="none">
                            <div class="cartao-body">
                                <?php foreach ($this->remedioNatural->getDoenca() as $doenca) { ?>
                                    <div class="caixa-item">
                                        <div class="faixa-item" id="<?php echo $doenca->getId(); ?>-cartao-1"><?php echo $doenca->getNomePopular(); ?></div>
                                        <div class="bloco-item none" id="bloco-item-<?php echo $doenca->getId(); ?>-cartao-1">
                                            <div class="tabela-padrao">
                                                <table>
                                                    <tbody>
                                                    <tr><th>Nome Popular:</th><td><?php echo $doenca->getNomePopular(); ?></td></tr>
                                                    <tr><th>Nome Cientifico:</th><td><?php echo $doenca->getNomeCientifico(); ?></td></tr>
                                                    <tr><th>Sintomas:</th><td><?php echo $doenca->getSintomas(); ?></td></tr>
                                                    <tr><th>Percentual de Risco:</th><td><?php echo $doenca->getPercentualRisco(); ?></td></tr>
                                                    <tr><th>Especialidade:</th><td><?php echo $doenca->getEspecialidade(); ?></td></tr>
                                                    <tr><th>Metodo Diagnostico:</th><td><?php echo $doenca->getMetodoDiagnostico(); ?></td></tr>
                                                    <tr><th>Periodo:</th><td><?php echo $doenca->getPeriodo(); ?></td></tr>
                                                    <tr><th>Dias Durações:</th><td><?php echo $doenca->getDiasDuracao(); ?></td></tr>
                                                    <tr><th>Inicio:</th><td><?php echo $doenca->getInicio(); ?></td></tr>
                                                    <tr><th>Data Descoberta:</th><td><?php echo $doenca->getDataDescoberta(); ?></td></tr>
                                                    <tr><th>Obervação:</th><td><?php echo $doenca->getObservacao(); ?></td></tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-12 mtop">
            <?php if($this->remedioNatural->getFornecedor()){ ?>
                <div class="cx-acordeon">
                    <div class="cartao">
                        <div class="cartao-header" id="2">
                            <h5 class="mb-0">
                                <span>Fornecedor</span>
                            </h5>
                        </div>
                        <div id="cartao-2" class="none">
                            <div class="cartao-body">
                                <?php foreach ($this->remedioNatural->getFornecedor() as $fornecedor) { ?>
                                    <div class="caixa-item">
                                        <div class="faixa-item" id="<?php echo $fornecedor->getId(); ?>-cartao-2"><?php echo $fornecedor->getNome(); ?></div>
                                        <div class="bloco-item none" id="bloco-item-<?php echo $fornecedor->getId(); ?>-cartao-2">
                                            <div class="tabela-padrao">
                                                <table>
                                                    <tbody>
                                                    <tr><th>nome:</th><td><?php echo $fornecedor->getNome(); ?></td></tr>
                                                    <tr><th>Endereço:</th><td><?php echo $fornecedor->getEndereco(); ?></td></tr>
                                                    <tr><th>numero:</th><td><?php echo $fornecedor->getNumero(); ?></td></tr>
                                                    <tr><th>complemento:</th><td><?php echo $fornecedor->getComplemento(); ?></td></tr>
                                                    <tr><th>bairro:</th><td><?php echo $fornecedor->getBairro(); ?></td></tr>
                                                    <tr><th>cidade:</th><td><?php echo $fornecedor->getCidade(); ?></td></tr>
                                                    <tr><th>UF:</th><td><?php echo $fornecedor->getUf(); ?></td></tr>
                                                    <tr><th>CEP:</th><td><?php echo $fornecedor->getCep(); ?></td></tr>
                                                    <tr><th>E-mail:</th><td><?php echo $fornecedor->getEmail(); ?></td></tr>
                                                    <tr><th>Nome Fantasia:</th><td><?php echo $fornecedor->getNomeFantasia(); ?></td></tr>
                                                    <tr><th>Percentual Ramo:</th><td><?php echo $fornecedor->getPercentualRamo(); ?>%</td></tr>
                                                    <tr><th>Observação:</th><td><?php echo $fornecedor->getObservacao(); ?></td></tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-12 mtop">
            <?php if($this->remedioNatural->getMedico()){ ?>
                <div class="cx-acordeon">
                    <div class="cartao">
                        <div class="cartao-header" id="3">
                            <h5 class="mb-0">
                                <span>Médico Especialista</span>
                            </h5>
                        </div>
                        <div id="cartao-3" class="none">
                            <div class="cartao-body">
                                <?php foreach ($this->remedioNatural->getMedico() as $medico) { ?>
                                    <div class="caixa-item">
                                        <div class="faixa-item" id="<?php echo $medico->getId(); ?>-cartao-3"><?php echo $medico->getNome(); ?></div>
                                        <div class="bloco-item none" id="bloco-item-<?php echo $medico->getId(); ?>-cartao-3">
                                            <div class="tabela-padrao">
                                                <table>
                                                    <tbody>
                                                    <tr><th>nome:</th><td><?php echo $medico->getNome(); ?></td></tr>
                                                    <tr><th>cidade:</th><td><?php echo $medico->getCidade(); ?></td></tr>
                                                    <tr><th>UF:</th><td><?php echo $medico->getUf(); ?></td></tr>
                                                    <tr><th>E-mail:</th><td><?php echo $medico->getEmail(); ?></td></tr>
                                                    <tr><th>Sexo:</th><td><?php echo $medico->getSexo(); ?></td></tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

</div>
<?php $this->loadDefault(SELF::FOOTER); ?>



