<!-- preparando formulario para pagina -->
<?php $this->loadDefault(SELF::HEADER); ?>
<div class="container top">
    <div class="row mtop">
        <div class="col-sm-12">
            <div class="alert alert-success <?php echo (is_null($this->mensagem)) ? "none" : "" ?>" role="alert">
                <?php echo $this->mensagem; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2 d-flex">
            <a href="medico/listar" class="btn btn-padrao mtop mbottom">Voltar</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 ">
            <div class="form-default">
                <div class="header d-flex flex-column justify-content-center align-items-center">
                    <i class="fa fa-stethoscope mright" aria-hidden="true"></i>
                    <span>Cadastro de médico</span>
                </div>
                <form method="post">
                    <div class="form-group">
                        <label>Nome</label>
                        <input name="nome" type="text" class="form-control tam5" required>
                    </div>
                    <div class="form-group">
                        <label>Endereço</label>
                        <input name="endereco" type="text" class="form-control tam5" required>
                    </div>
                    <div class="form-group">
                        <label>Número</label>
                        <input name="numero" type="text" class="number form-control tam1" required>
                    </div>
                    <div class="form-group">
                        <label>Complemento</label>
                        <input name="complemento" type="text" class="form-control tam5" required>
                    </div>
                    <div class="form-group">
                        <label>Bairro</label>
                        <input name="bairro" type="text" class="form-control tam3" required>
                    </div>
                    <div class="form-group">
                        <label>Cidade</label>
                        <input name="cidade" type="text" class="form-control tam3" required>
                    </div>
                    <div class="form-group">
                        <label>UF</label>
                        <input name="uf" type="text" class="form-control tam05" maxlength="2" required>
                    </div>
                    <div class="form-group">
                        <label>CEP</label>
                        <input name="cep" type="text" class="form-control tam1 cep" required>
                    </div>
                    <div class="form-group tam1">
                        <label>Sexo</label>
                        <select name="sexo" class="form-control" required>
                            <option></option>
                            <option value="masculino">Masculino</option>
                            <option value="feminino">Feminino</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Data de Nascimento</label>
                        <input name="data_nascimento" type="text" class="date form-control tam1" required>
                    </div>
                    <div class="form-group">
                        <label>Nacionalidade</label>
                        <input name="nacionalidade" type="text" class="form-control tam2" required>
                    </div>
                    <div class="form-group">
                        <label>Esp. Remedios Naturais</label>
                        <input name="esp_remedio_nat" type="text" class="percentual form-control tam1" required>
                    </div>
                    <div class="form-group">
                        <label>Esp. Remedios Sinteticos</label>
                        <input name="esp_remedio_sint" type="text" class="percentual form-control tam1" required>
                    </div>
                    <div class="form-group">
                        <label>CRM</label>
                        <input name="crm" type="text" class="form-control tam1" required>
                    </div>
                    <div class="form-group">
                        <label>Observação</label>
                        <textarea name="observacao" class="form-control"  rows="10"></textarea>
                    </div>
                    <div class="form-group">
                        <label>E-mail</label>
                        <input name="email" type="email" class="form-control tam3" required>
                    </div>
                    <div class="form-group">
                        <label>Senha</label>
                        <input name="senha" type="password" class="form-control tam1" required>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-padrao">Enviar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->loadDefault(SELF::FOOTER); ?>



