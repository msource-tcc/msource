<?php $this->loadDefault(SELF::HEADER); ?>
<div class="container">
    <div class="row">
        <div class="col-sm-2 d-flex">
            <a href="dashboard" class="btn btn-padrao mtop mbottom mright">Voltar</a>
            <a href="medico/inserir" class="btn btn-padrao mtop mbottom">Cadastrar</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?php if(empty($this->medicoArray)){?>
                <div class="alert alert-warning" role="alert">
                    Nenhum registro.
                </div>
            <?php } else { ?>
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col" colspan="5"><span class="d-flex justify-content-center">Médico</span></th>
                    </tr>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nome</th>
                        <th scope="col">CRM</th>
                        <th scope="col">Esp. Remedios Sinteticos</th>
                        <th scope="col">Esp. Remedios Naturais</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($this->medicoArray as $medico) { ?>
                            <tr>
                                <th scope="row"><?php echo $medico->getId(); ?></th>
                                <td><a href="medico/visualizar/<?php echo $medico->getId(); ?>"><?php echo $medico->getNome(); ?></a></td>
                                <td><?php echo $medico->getCrm(); ?></td>
                                <td><?php echo $medico->getEspRemedioSint(); ?></td>
                                <td><?php echo $medico->getEspRemedioNat(); ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php } ?>
        </div>
    </div>
</div>
<?php $this->loadDefault(SELF::FOOTER); ?>



