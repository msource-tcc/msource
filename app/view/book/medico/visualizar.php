<!-- preparando formulario para pagina -->
<?php $this->loadDefault(SELF::HEADER); ?>ss
<div class="container">
    <div class="row">
        <div class="col-sm-12 d-flex">
            <a href="medico/listar" class="btn btn-padrao mtop mbottom mright">Voltar</a>
            <a href="medico/atualizar/<?php echo $this->medico->getId();?>" class="btn btn-padrao mtop mbottom">Atualizar</a>
            <a href="medico/excluir/<?php echo $this->medico->getId();?>" class="btn btn-padrao mtop mbottom ml-auto">Excluir Médico</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="tabela-padrao">
                <table>
                    <tbody>
                        <tr><td colspan="2" class="table-header">Perfil Médico</td></tr>
                        <tr><th>nome:</th><td><?php echo $this->medico->getNome(); ?></td></tr>
                        <tr><th>Endereço:</th><td><?php echo $this->medico->getEndereco(); ?></td></tr>
                        <tr><th>numero:</th><td><?php echo $this->medico->getNumero(); ?></td></tr>
                        <tr><th>complemento:</th><td><?php echo $this->medico->getComplemento(); ?></td></tr>
                        <tr><th>bairro:</th><td><?php echo $this->medico->getBairro(); ?></td></tr>
                        <tr><th>cidade:</th><td><?php echo $this->medico->getCidade(); ?></td></tr>
                        <tr><th>UF:</th><td><?php echo $this->medico->getUf(); ?></td></tr>
                        <tr><th>CEP:</th><td><?php echo $this->medico->getCep(); ?></td></tr>
                        <tr><th>E-mail:</th><td><?php echo $this->medico->getEmail(); ?></td></tr>
                        <tr><th>Senha:</th><td><?php echo $this->medico->getSenha(); ?></td></tr>
                        <tr><th>Sexo:</th><td><?php echo $this->medico->getSexo(); ?></td></tr>
                        <tr><th>Data Nascimento:</th><td><?php echo $this->medico->getDataNascimento(); ?></td></tr>
                        <tr><th>Nacionalidade:</th><td><?php echo $this->medico->getNacionalidade(); ?></td></tr>
                        <tr><th>CRM:</th><td><?php echo $this->medico->getCrm(); ?></td></tr>
                        <tr><th>Esp. Remedios Naturais:</th><td><?php echo $this->medico->getEspRemedioNat(); ?>%</td></tr>
                        <tr><th>Esp. Remedios Sinteticos:</th><td><?php echo $this->medico->getEspRemedioSint(); ?>%</td></tr>
                        <tr><th>Observação:</th><td><?php echo $this->medico->getObservacao(); ?></td></tr>
                        <tr><th>Data Criação:</th><td><?php echo $this->medico->getDataCriacao(); ?></td></tr>
                        <tr><th>Data Atualização:</th><td><?php echo $this->medico->getDataAtualizacao(); ?></td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<?php $this->loadDefault(SELF::FOOTER); ?>



