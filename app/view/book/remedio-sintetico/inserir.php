<?php $this->loadDefault(SELF::HEADER); ?>
<div class="container">
    <div class="row mtop">
        <div class="col-sm-12">
            <div class="alert alert-success <?php echo (is_null($this->mensagem)) ? "none" : "" ?>" role="alert">
                <?php echo $this->mensagem; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2 d-flex">
            <a href="remedio-sintetico/listar" class="btn btn-padrao mtop mbottom">Voltar</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 ">
            <div class="form-default">
                <div class="header d-flex flex-column justify-content-center align-items-center">
                    <i class="fa fa-flask mright" aria-hidden="true"></i>
                    <span>Cadastro de remédio sintetico</span>
                </div>
                <form method="post">
                    <div class="form-group">
                        <label>Nome Popular</label>
                        <input name="nome_popular" type="text" class="form-control tam5" required>
                    </div>
                    <div class="form-group">
                        <label>Nome Cientifico</label>
                        <input name="nome_cientifico" type="text" class="form-control tam5" required>
                    </div>
                    <div class="form-group">
                        <label>Quilograma</label>
                        <input name="quilograma" type="text" class="float form-control tam05" required>
                    </div>
                    <div class="form-group">
                        <label>Calorias</label>
                        <input name="calorias" type="text" class="number form-control tam05" required>
                    </div>
                    <div class="form-group">
                        <label>Cor Natural</label>
                        <input name="cor_natural" type="text" class="form-control tam3" required>
                    </div>
                    <div class="form-group">
                        <label>Data Descoberta</label>
                        <input name="data_descoberta" type="text" class="date form-control tam1" required>
                    </div>
                    <div class="form-group">
                        <label>Excreção</label>
                        <input name="excrecao" type="text" class="form-control tam5" required>
                    </div>
                    <div class="form-group">
                        <label>Administração</label>
                        <input name="administracao" type="text" class="form-control tam5" required>
                    </div>
                    <div class="form-group">
                        <label>CAS</label>
                        <input name="cas" type="text" class="form-control tam5" required>
                    </div>
                    <div class="form-group">
                        <label>Metabolismo</label>
                        <input name="metabolismo" type="text" class="form-control tam5" required>
                    </div>
                    <div class="form-group">
                        <label>Fins Medicinais</label>
                        <textarea name="fins_medicinais" class="form-control"  rows="10"></textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-padrao">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->loadDefault(SELF::FOOTER); ?>



