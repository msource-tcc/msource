<?php $this->loadDefault(SELF::HEADER); ?>
<div class="container">
    <div class="row">
        <div class="col-sm-2 d-flex">
            <a href="dashboard" class="btn btn-padrao mtop mbottom mright">Voltar</a>
            <?php if($this->tipoSessao != "paciente"){ ?>
            <a href="remedio-sintetico/inserir" class="btn btn-padrao mtop mbottom">Cadastrar</a>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?php if(empty($this->remedioSinteticoArray)){?>
                <div class="alert alert-warning" role="alert">
                    Nenhum registro.
                </div>
            <?php } else { ?>
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col" colspan="5"><span class="d-flex justify-content-center">Remédio Sintetico</span></th>
                    </tr>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nome Popular</th>
                        <th scope="col">Nome Cientifico</th>
                        <th scope="col">Calorias</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($this->remedioSinteticoArray as $remedioSintetico) { ?>
                            <tr>
                                <th scope="row"><?php echo $remedioSintetico->getId(); ?></th>
                                <td><a href="remedio-sintetico/visualizar/<?php echo $remedioSintetico->getId(); ?>"><?php echo $remedioSintetico->getNomePopular(); ?></a></td>
                                <td><?php echo $remedioSintetico->getNomeCientifico(); ?></td>
                                <td><?php echo $remedioSintetico->getCalorias(); ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php } ?>
        </div>
    </div>
</div>
<?php $this->loadDefault(SELF::FOOTER); ?>



