<!-- preparando formulario para pagina -->
<?php $this->loadDefault(SELF::HEADER); ?>
<div class="container">
    <div class="row">
        <div class="col-sm-12 d-flex">
            <a href="administrador/atualizar" class="btn btn-padrao mtop mbottom">Atualizar</a>
            <a href="dashboard" class="btn btn-padrao mtop mbottom mleft">Voltar</a>
            <a href="administrador/excluir" class="btn btn-padrao mtop mbottom ml-auto">Excluir Conta</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="tabela-padrao">
                <table>
                    <tbody>
                        <tr><td colspan="2" class="table-header">Administrador</td></tr>
                        <tr><th>nome:</th><td><?php echo $this->administrador->getNome(); ?></td></tr>
                        <tr><th>Endereço:</th><td><?php echo $this->administrador->getEndereco(); ?></td></tr>
                        <tr><th>numero:</th><td><?php echo $this->administrador->getNumero(); ?></td></tr>
                        <tr><th>complemento:</th><td><?php echo $this->administrador->getComplemento(); ?></td></tr>
                        <tr><th>bairro:</th><td><?php echo $this->administrador->getBairro(); ?></td></tr>
                        <tr><th>cidade:</th><td><?php echo $this->administrador->getCidade(); ?></td></tr>
                        <tr><th>UF:</th><td><?php echo $this->administrador->getUf(); ?></td></tr>
                        <tr><th>CEP:</th><td><?php echo $this->administrador->getCep(); ?></td></tr>
                        <tr><th>E-mail:</th><td><?php echo $this->administrador->getEmail(); ?></td></tr>
                        <tr><th>Senha:</th><td><?php echo $this->administrador->getSenha(); ?></td></tr>
                        <tr><th>Peso:</th><td><?php echo $this->administrador->getPeso(); ?></td></tr>
                        <tr><th>Nacionalidade:</th><td><?php echo $this->administrador->getNacionalidade(); ?></td></tr>
                        <tr><th>Data Nascimento:</th><td><?php echo $this->administrador->getDataNascimento(); ?></td></tr>
                        <tr><th>Sexo:</th><td><?php echo $this->administrador->getSexo(); ?></td></tr>
                        <tr><th>Observação:</th><td><?php echo $this->administrador->getObservacao(); ?></td></tr>
                        <tr><th>Data Criação:</th><td><?php echo $this->administrador->getDataCriacao(); ?></td></tr>
                        <tr><th>Data Atualização:</th><td><?php echo $this->administrador->getDataAtualizacao(); ?></td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php $this->loadDefault(SELF::FOOTER); ?>



