<?php $this->loadDefault(SELF::HEADER); ?>
<div class="container">
    <div class="row mtop">
        <div class="col-sm-12">
            <div class="alert alert-success <?php echo (is_null($this->mensagem)) ? "none" : "" ?>" role="alert">
                <?php echo $this->mensagem; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2 d-flex">
            <a href="administrador/visualizar" class="btn btn-padrao mtop mbottom">Voltar</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-default">
                <div class="header d-flex flex-column justify-content-center align-items-center">
                    <i class="fa fa-user-circle-o mright" aria-hidden="true"></i>
                    <span>Atualizar administrador</span>
                </div>
                <form method="post">
                    <div class="form-group">
                        <label>Nome</label>
                        <input name="nome" type="text" class="form-control tam5" required value="<?php echo $this->administrador->getNome() ?>">
                    </div>
                    <div class="form-group">
                        <label>Endereço</label>
                        <input name="endereco" type="text" class="form-control tam5" required value="<?php echo $this->administrador->getEndereco() ?>">
                    </div>
                    <div class="form-group">
                        <label>Número</label>
                        <input name="numero" type="text" class="number form-control tam1" required value="<?php echo $this->administrador->getNumero() ?>">
                    </div>
                    <div class="form-group">
                        <label>Complemento</label>
                        <input name="complemento" type="text" class="form-control tam5" required value="<?php echo $this->administrador->getComplemento() ?>">
                    </div>
                    <div class="form-group">
                        <label>Bairro</label>
                        <input name="bairro" type="text" class="form-control tam3" required value="<?php echo $this->administrador->getBairro() ?>">
                    </div>
                    <div class="form-group">
                        <label>Cidade</label>
                        <input name="cidade" type="text" class="form-control tam3" required value="<?php echo $this->administrador->getCidade() ?>">
                    </div>
                    <div class="form-group">
                        <label>UF</label>
                        <input name="uf" type="text" class="form-control tam05" maxlength="2" required value="<?php echo $this->administrador->getUf() ?>">
                    </div>
                    <div class="form-group">
                        <label>CEP</label>
                        <input name="cep" type="text" class="form-control tam1 cep" required value="<?php echo $this->administrador->getCep() ?>">
                    </div>
                    <div class="form-group">
                        <label>Peso</label>
                        <input name="peso" type="text" class="float form-control tam1" required value="<?php echo $this->administrador->getPeso() ?>">
                    </div>
                    <div class="form-group">
                        <label>Nacionalidade</label>
                        <input name="nacionalidade" type="text" class="form-control tam2" required value="<?php echo $this->administrador->getNacionalidade() ?>">
                    </div>
                    <div class="form-group">
                        <label>Data de Nascimento</label>
                        <input name="data_nascimento" type="text" class="date form-control tam1" required value="<?php echo $this->administrador->getDataNascimento() ?>">
                    </div>
                    <div class="form-group tam1">
                        <label>Sexo</label>
                        <select name="sexo" class="form-control" required>
                            <option></option>
                            <option <?php echo ($this->administrador->getSexo() == "masculino") ? "selected" : "" ?> value="masculino">Masculino</option>
                            <option <?php echo ($this->administrador->getSexo() == "Feminino") ? "selected" : "" ?> value="feminino">Feminino</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Observação</label>
                        <textarea name="observacao" class="form-control"  rows="10"> <?php echo $this->administrador->getObservacao() ?></textarea>
                    </div>

                    <div class="form-group">
                        <label>E-mail</label>
                        <input name="email" type="email" class="form-control tam3" required value="<?php echo $this->administrador->getEmail() ?>">
                    </div>
                    <div class="form-group">
                        <label>Senha</label>
                        <input name="senha" type="password" class="form-control tam1" required value="<?php echo $this->administrador->getSenha() ?>">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-padrao">Enviar</button>
                        <a href="administrador/visualizar" class="btn btn-padrao">Voltar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->loadDefault(SELF::FOOTER); ?>



