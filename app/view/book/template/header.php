<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?php echo self::TITULO_ABA; ?></title>
    <base href="<?php echo BASE; ?>">
    <link rel="stylesheet" href="<?php echo self::BOOTSTRAP;?>">
    <link rel="stylesheet" href="<?php echo self::CSS;?>">
    <link rel="stylesheet" href="<?php echo self::FONTAWESOME;?>">
    <script type="text/javascript" src="<?php echo self::JQUERY ?>"></script>
    <script type="text/javascript" src="<?php echo self::MASK ?>"></script>
    <script type="text/javascript" src="<?php echo self::MAIN ?>"></script>
  </head>
  <body>
  <!-- barra horizontal -->
  <header>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div id="titulo" class="d-flex align-items-center">
                    <span><?php echo self::TITULO; ?> </span>
                </div>
            </div>
        </div>
    </div>
  </header>
  <section>
      <?php if(isset($this->acessoSessao) && isset($this->tipoSessao)){ ?>
      <div class="sub-header">
          <div class="container h100">
              <div class="row h100">
                  <div class="col-sm-6 d-flex align-items-center">
                      <span>Olá, <?php echo $this->acessoSessao->getNome() . " | " . ucfirst($this->tipoSessao); ?></span>
                  </div>
                  <div class="col-sm-6 d-flex align-items-center justify-content-end">
                      <a href="logoff" class="btn btn-padrao-inline">Sair</a>
                  </div>
              </div>
          </div>
      </div>
    <?php } ?>