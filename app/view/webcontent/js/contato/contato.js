$('document').ready(function(){

    //ACAO DELETAR
    $(".acao-deletar").click(function () {
        var retorno = confirm("DESEJA EXCLUIR ESTE CONTATO ?");
        var id_cliente = $(this).attr('id_cliente');

        if(retorno){
            $.ajax({
                type: 'POST',
                url: 'contato/excluir/'+id_cliente,
                dataType: 'json',
                success: function (response) {
                    if (response.status) {
                        location.reload();
                    }
                }
            });
        }
    });
//====================================================================


});