
$('document').ready(function(){

    //BUSCAR DUPLICIDADE DE DADOS - DEFAULT
    function buscar_duplicidade(dadosJson, atribuicao){
        $.ajax({
            type: 'POST',
            url: 'cliente/buscar_duplicidade',
            dataType: 'json',
            data: dadosJson,
            success: function (response) {
                if (response.status) {
                    $(".msg-faixa-falha"+atribuicao.classe+"").css('display', 'table');
                    $(".msg-faixa-falha"+atribuicao.classe+" span").text(atribuicao.mensagem);
                    bloquearElemento(atribuicao.elemento);
                }
            }
        });
    }

    function bloquearElemento(atribuicao) {
        $(atribuicao.elemento).attr('disabled', true);
        $(atribuicao.elemento).css('cursor', 'not-allowed');
    }

    function  liberarElemento(atribuicao) {
        $(atribuicao.elemento).attr('disabled', false);
        $(atribuicao.elemento).css('cursor', 'pointer');
    }

    //=======================================================================================================

    //BUSCAR DUPLICIDADE CPF
    $('#pessoa_cpf').blur( function () {
      var cpf = $('#pessoa_cpf').val();
      var dadosJson = {'tipo':'cpf', 'dados':cpf};
      var atribuicao = {'classe':'.cpf', 'titulo':'CPF', 'mensagem':'Este CPF já encontra-se cadastrado.', 'elemento':{'elemento' : '.btn-simples.cadastrar'}};
        buscar_duplicidade(dadosJson, atribuicao);
    });
    $('#pessoa_cpf').focus(function(){
        liberarElemento({'elemento' : '.btn-simples.cadastrar'});
        $('.msg-faixa-falha').fadeOut(1000);
    });

    //BUSCAR DUPLICIDADE RG
    $('#pessoa_rg').blur( function () {
        var rg = $('#pessoa_rg').val();
        var dadosJson = {'tipo':'rg', 'dados':rg};
        var atribuicao = {'classe':'.rg', 'titulo':'RG', 'mensagem':'Este RG já encontra-se cadastrado.', 'elemento':{'elemento' : '.btn-simples.cadastrar'}};
        buscar_duplicidade(dadosJson, atribuicao);
    });
    $('#pessoa_rg').focus(function(){
        liberarElemento({'elemento' : '.btn-simples.cadastrar'});
        $('.msg-faixa-falha').fadeOut(1000);
    });

    //BUSCAR DUPLICIDADE EMAIL
    $('#pessoa_email').blur( function () {
        var email = $('#pessoa_email').val();
        var dadosJson = {'tipo':'email', 'dados':email};
        var atribuicao = {'classe':'.email', 'titulo':'E-MAIL', 'mensagem':'Este E-MAIL já encontra-se cadastrado.', 'elemento':{'elemento' : '.btn-padrao.cadastrar'}};
        buscar_duplicidade(dadosJson, atribuicao);
    });
    $('#pessoa_email').focus(function(){
        liberarElemento({'elemento' : '.btn-padrao.cadastrar'});
        $('.msg-faixa-falha').fadeOut(1000);
    });


    //BUSCAR DUPLICIDADE CNPJ
    $('#pessoa_cnpj').blur( function () {
        var cnpj = $('#pessoa_cnpj').val();
        var dadosJson = {'tipo':'cnpj', 'dados':cnpj};
        var atribuicao = {'classe':'.cnpj', 'titulo':'CNPJ', 'mensagem':'Este CNPJ já encontra-se cadastrado.', 'elemento':{'elemento' : '.btn-simples.cadastrar'}};
        buscar_duplicidade(dadosJson, atribuicao);
    });
    $('#pessoa_cnpj').focus(function(){
        liberarElemento({'elemento' : '.btn-simples.cadastrar'});
        $('.msg-faixa-falha').fadeOut(1000);
    });

    //=======================================================================================================

    $('.select-natureza').change(function () {
        //botao wpp esta zerando tbm, fazer voltar ao estado
        //SETANDO OS VALORES

        if($(this).val()=="pj"){
            $('.pessoa_juridica').show();
            $('.pessoa_fisica').hide();
        }else if($(this).val()=="pf"){
            $('.pessoa_juridica').hide();
            $('.pessoa_fisica').show();
        }

        //REQUERINDO SIM E NÃO NOS CAMPOS CORRETOS
        if($(this).val()=="pj"){
            //PJ - SIM
            $('.pj-req .requerido').html('*');
            $('.pj-req input').attr('required', true);
            //PF - NÃO
            $('.pf-req .requerido').html('*');
            $('.pf-req input').attr('required', false);

        }else if($(this).val()=="pf"){
            //PJ - NAO
            $('.pj-req .requerido').html('*');
            $('.pj-req input').attr('required', false);
            //PF - SIM
            $('.pf-req .requerido').html('*');
            $('.pf-req input').attr('required', true);
        }
    });
//=======================================================================================================

    $(".acao-deletar").click(function () {
        var retorno = confirm("DESEJA EXCLUIR ESTE CLIENTE ?");
        var id_cliente = $(this).attr('id_cliente');

        if(retorno){
            $.ajax({
                type: 'POST',
                url: 'cliente/excluir/'+id_cliente,
                dataType: 'json',
                success: function (response) {
                    if (response.status) {
                        location.reload();
                    }
                }
            });
        }

    });
});