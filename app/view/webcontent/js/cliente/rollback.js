
$('document').ready(function(){

    jQuery(function($) {

        var url = window.location.pathname;
        var array_url = url.split("/", 3);
        array_url = array_url.splice(1);
        var controller = array_url[1];

        $.ajax({
            type: 'POST',
            url  : controller+'/rollback',
            dataType: "json",
            success :  function(retorno){
                $.each(retorno, function (titulo, objeto) {
                    $.each(objeto, function (atributo, valor) {
                        if($("[name*='"+titulo+"["+atributo+"]']").is('input') || $("[name*='"+titulo+"["+atributo+"]']").is('textarea')){
                            $("[name*='"+titulo+"["+atributo+"]']").val(valor);
                        }else if($("[name*='"+titulo+"["+atributo+"]']").is('select')){
                            $("[name*='"+titulo+"["+atributo+"]'] option[value='"+valor+"']").attr("selected", true);
                            //EXIBINDO ELEMENTOS QUE NAO PODE SER EXIBIDO SOMENTE COM SELECTED
                            if(atributo == "natureza"){
                                natureza(valor);
                            }
                        }
                    });
                });
            }
        });
        return "false";
    });
});

//======================================================================================================================

//EXIBINDO ELEMENTOS DA NATUREZA
function natureza(valor) {
    if(valor=="pj"){
        $('.pessoa_juridica').show();
        $('.pessoa_fisica').hide();
    }else if(valor=="pf"){
        $('.pessoa_juridica').hide();
        $('.pessoa_fisica').show();
    }
    //REQUERINDO SIM E NÃO NOS CAMPOS CORRETOS
    if(valor=="pj"){
        //PJ - SIM
        $('.pj-req .requerido').html('*');
        $('.pj-req input').attr('required', true);
        //PF - NÃO
        $('.pf-req .requerido').html('*');
        $('.pf-req input').attr('required', false);

    }else if(valor=="pf"){
        //PJ - NAO
        $('.pj-req .requerido').html('*');
        $('.pj-req input').attr('required', false);
        //PF - SIM
        $('.pf-req .requerido').html('*');
        $('.pf-req input').attr('required', true);
    }
}