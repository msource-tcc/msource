$(document).ready(function () {
    $(".float").mask('0ZZ.0Z', {translation: {'Z': {pattern: /[0-9]/, optional: true}}});
    $(".quilo").mask('0ZZ.0ZZ', {translation: {'Z': {pattern: /[0-9]/, optional: true}}});
    $(".percentual").mask('ABB.C', {translation: {'A': {pattern: /[0-1]/, optional: true}, 'B': {pattern: /[0-9]/, optional: true}, 'C': {pattern: /[0-9]/, optional: true}}});
    $(".number").mask('000000000');
    $(".cep").mask('00000-000');
    $(".celular").mask('(00) 00000-0000');
    $(".date").mask('00/00/0000');

    $(".contato-checkbox").click(function () {
       var id = $(this).attr('id');
        $("#contato-"+id).slideToggle('fast');
    });

    $(".cartao-header").click(function () {
        var id = $(this).attr('id');
        $("#cartao-"+id).slideToggle('fast');
    });

    $(".faixa-item").click(function () {
        var id = $(this).attr('id');
        $("#bloco-item-"+id).slideToggle('fast');
    });
});