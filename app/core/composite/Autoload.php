<?php

spl_autoload_register(function($class_name){

	$file = $class_name . ".php";
	$dir_dao = (stristr($class_name, "DAO"))? strtolower(str_replace('DAO', "", $class_name)) : "";

	$array_dependencias = [
		'Routing' 				=> 'config', 
		'Config' 				=> 'config', 
		'GlobalConstant'		=> 'app/common/public_class', 
		'Component' 			=> 'app/common/public_trait',
		'Standard' 				=> 'app/core/composite',
		'Controller' 			=> 'app/controller', 
		'View' 					=> 'app/view', 
		'Entity'				=> 'app/model/entity', 
		'Model' 				=> "app/model/tomodel",
		'DAO' 					=> "app/model/{$dir_dao}",
		'Class'					=>'app/common/public_class',
		'Trait'					=>'app/common/public_trait',
		'Connection' 			=> 'app/core/communicate',
		'Debug'					=>'app/common/public_trait',
	];

    $array_modelos = [
        'Administrador'         =>'app/model/administrador',
        'Hospital'              =>'app/model/hospital',
        'Paciente'				=>'app/model/paciente',
        'ContatoPaciente'	    =>'app/model/contatopaciente',
        'Doenca'	            =>'app/model/doenca',
        'Fornecedor'			=>'app/model/fornecedor',
        'Medico'			    =>'app/model/medico',
        'RemedioNatural'	    =>'app/model/remedionatural',
        'RemedioSintetico'	    =>'app/model/remediosintetico',
    ];


	//dependencias
	foreach ($array_dependencias as $class =>$path) {
		if(stristr($class_name, $class)){
			require_once("$path/$file");
			break;		
		}		
	}

	//modelos
    foreach ($array_modelos as $class =>$path) {
        if($class_name == $class){
            require_once("$path/$file");
            break;
        }
    }

} );
