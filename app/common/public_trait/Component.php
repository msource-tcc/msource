<?php
abstract class Component extends GlobalConstant {
    use Standard;
    use Debug;

    public $mensagem = null;
    public $tipoSessao;

    //VALIDANDO SESSAO LOGADO
    public function init(){
        if(!isset($_SESSION)){
            session_start();
        }
    }

    //VALIDANDO TIPO DE LOGIN - DEMAIS
    public function isLogged($tipo = null){
        if(
            isset($_SESSION['login']['logged']) &&
            ($_SESSION['login']['tipo'] == "paciente" || $_SESSION['login']['tipo'] == "fornecedor" || $_SESSION['login']['tipo'] == "medico" || $_SESSION['login']['tipo'] == "administrador")
        ){
            if(!is_null($tipo) &&  $_SESSION['login']['tipo'] != $tipo){
                $this->redirect("dashboard");
            }
        }else{
            $this->redirect("");
        }
    }
    //VALIDANDO TIPO DE LOGIN - DASHBOARD
    public function sessaoLogin($tipo = null){
        if(
            isset($_SESSION['login']['logged']) &&
            ($_SESSION['login']['tipo'] == "paciente" || $_SESSION['login']['tipo'] == "fornecedor" || $_SESSION['login']['tipo'] == "medico" || $_SESSION['login']['tipo'] == "administrador")
        ){
            return $_SESSION['login'];
        }else{
            return false;
        }
    }
}