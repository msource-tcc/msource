<?php

abstract class GlobalConstant {

    //ajustes
    const TITULO_ABA = 'MSOURCE';
    const TITULO = 'MSOURCE';
    const CONTROLLER_MAIN = 'dashboard';   
    const ACTION_MAIN = 'index';     
    
    //arquivos
    const HEADER = "app/view/book/template/header.php";
    const FOOTER = "app/view/book/template/footer.php";

    //folha de estilo
    const CSS = 'app/view/webcontent/css/main.css';
    const BOOTSTRAP  ='app/view/webcontent/lib/bootstrap-4.1.0/css/bootstrap.min.css';
    const FONTAWESOME  ='app/view/webcontent/lib/font-awesome/css/font-awesome.min.css';

    //javascritp
    const JS ='app/view/webcontent/js/';
    const JQUERY ='app/view/webcontent/js/default/jquery.min.js';
    const MASK ='app/view/webcontent/js/default/jquery.mask.js';
    const MAIN ='app/view/webcontent/js/default/main.js';

    //caminhos
    const IMG ='app/view/webcontent/img/';   
    const XML = 'app/common/public_xml/';   
    const BOOK = 'app/view/book/';     
    const DEFAULT_VIEW = 'app/view/book/default/';

}

?>