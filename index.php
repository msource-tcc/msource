<?php
require_once('app/core/composite/Autoload.php');

//diretorio BASE
define("HOST", "{$_SERVER['HTTP_HOST']}");       
define("BASE", "http://{$_SERVER['HTTP_HOST']}/msource/");

//criando array de urls.  padrão será: array(0=>array('url'=>'', 'controller'=>'', 'action'=>''));
$navegation = simplexml_load_file('config/navegation.xml');
foreach ($navegation as $rudder => $attributes) {
   $array_attributes_xml = (array)$attributes;   
   $array_navegation[] =  $array_attributes_xml['@attributes']; 
}

//instanciando classe de roteamento e controlador
new Routing($array_navegation);
